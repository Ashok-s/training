package com.kpr.training.jdbc.exception;

public enum ErrorCode {

	POSTAL_CODE_ZERO("ERR401", "postal code should not be zero")
	,READING_ADDRESS_FAILS("ERR402", "Failed to read address")
	,ADDRESS_CREATION_FAILS("ERR403", "Address was not created")
	,ADDRESS_UPDATION_FAILS("ERR404", "Failed to update Address")
	,ADDRESS_DELETION_FAILS("ERR405", "Failed to delete Address")
	,PERSON_CREATION_FAILS("ERR406", "Person was not created")
	,PERSON_UPDATION_FAILS("ERR407", "Failed to update Person")
	,PERSON_DELETION_FAILS("ERR408", "Failed to delete Person")
	,CONNECTION_FAILS("ERR409", "Failed to initiate connection")
	,EMAIL_NOT_UNIQUE("ERR410", "Email should be unique")
	,CONNECTION_FAILS_TO_CLOSE("ERR411", "Failed to close the connection")
	,FAILED_TO_COMMIT("ERR412", "Failed to commit")
	,FAILED_TO_ROLLBACK("ERR413", "Failed to rollback")
	,FAILED_TO_CHECK_EMAIL("ERR414", "Failed to check email")
	,READING_PERSON_FAILS("ERR415", "Failed to read person")
	,NAME_NOT_UNIQUE("ERR416", "Name should be unique")
	,READING_ADDRESSID_FAILS("ERR417", "Failed to read address id")
	,FAILED_TO_CHECK_NAME("ERR418", "Failed to check name")
	,WRONG_DATE_FORMAT("ERR419", "Date should be in dd-mm-yyyy format")
	,FAILED_TO_GET_ADDRESS_SIZE("ERR420", "Failed to get the size of address")
	,FAILED_TO_GET_PERSON_SIZE("ERR421", "Failed to get the size of person");

    private final String code;
    private final String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
