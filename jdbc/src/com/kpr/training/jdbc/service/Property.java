package com.kpr.training.jdbc.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class Property {

    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.setProperty("URL", "jdbc:mysql://localhost:3306/?user=root?autoReconnect=true&useSSL=false");
        properties.setProperty("USER_NAME", "root");
        properties.setProperty("PASSWORD", "Ashok@2001");
        properties.setProperty("SCHEMA", "jdbc");
        properties.setProperty("PORT", "3306");
        
        OutputStream output = new FileOutputStream("db.properties");
        properties.store(output, "Key and Value");
        System.out.println("Properties stored successful");

        properties.list(System.out);
    }
}
