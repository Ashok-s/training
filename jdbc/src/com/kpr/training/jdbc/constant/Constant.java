package com.kpr.training.jdbc.constant;

public class Constant {

    public static final String URL          = "URL";
    public static final String USER_NAME    = "USER_NAME";
    public static final String PASSWORD     = "PASSWORD";
    public static final String ID           = "id";
    public static final String STREET       = "street";
    public static final String CITY         = "city";
    public static final String PINCODE      = "pincode";
    public static final String FIRST_NAME   = "firstName";
    public static final String Last_NAME    = "lastName";
    public static final String EMAIL        = "email";
    public static final String ADDRESS_ID   = "addressId";
    public static final String BIRTH_DATE   = "birthDate";
    public static final String CREATED_DATE = "createdDate";
    
}
