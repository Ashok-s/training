package com.kpr.training.jdbc.constant;

public class QueryStatement {

    public static final String CREATE_ADDRESS_QUERY   = new StringBuilder("INSERT INTO `jdbc`.address (street, city, postal_code) ")
                                                                  .append("VALUES (?, ?, ?)")
                                                                  .toString();

    public static final String READ_ADDRESS_QUERY     = new StringBuilder("SELECT address.id ")
                                                                  .append(",address.street ")
                                                                  .append(",address.city ")
                                                                  .append(",address.postal_code ")
                                                                  .append("FROM `jdbc`.address ")
                                                                  .append("WHERE `id` = ?")
                                                                  .toString();

    public static final String READ_ALL_ADDRESS_QUERY = new StringBuilder("SELECT address.id ")
                                                                  .append(",address.street ")
                                                                  .append(",address.city ")
                                                                  .append(",address.postal_code ")
                                                                  .append("FROM `jdbc`.address")
                                                                  .toString();
    
    public static final String UPDATE_ADDRESS_QUERY = new StringBuilder("UPDATE `jdbc`.address ")
                                                                .append("SET street = ? ")
                                                                .append(",city = ? ")
                                                                .append(",postal_code = ? ")
                                                                .append("WHERE id = ?")
                                                                .toString();
    
    public static final String DELETE_ADDRESS_QUERY = new StringBuilder("DELETE FROM `jdbc`.address")
                                                                .append(" WHERE id=?")
                                                                .toString();

    public static final String CREATE_PERSON_QUERY = new StringBuilder("INSERT INTO `jdbc`.person (first_name, last_name, email, birth_date, address_id)")
                                                               .append("VALUES (?, ?, ?, ?, ?)")
                                                               .toString();

    public static final String READ_PERSON_QUERY = new StringBuilder("SELECT person.id ")
                                                             .append(",person.first_name ")
                                                             .append(",person.last_name ")
                                                             .append(",person.email ")
                                                             .append(",person.address_id ")
                                                             .append(",person.birth_date ")
                                                             .append(",person.created_date ")
                                                             .append("FROM `jdbc`.person ")
                                                             .append("WHERE `id` = ?")
                                                             .toString();

    public static final String READ_ALL_PERSON_QUERY = new StringBuilder("SELECT person.id ")
                                                                 .append(",person.first_name ")
                                                                 .append(",person.last_name ")
                                                                 .append(",person.email ")
                                                                 .append(",person.address_id ")
                                                                 .append(",person.birth_date ")
                                                                 .append(",person.created_date ")
                                                                 .append("FROM `jdbc`.person ")
                                                                 .toString();

    public static final String UPDATE_PERSON_QUERY = new StringBuilder("UPDATE `jdbc`.person ")
                                                               .append("SET first_name = ? ")
                                                               .append(",last_name = ? ")
                                                               .append(",email = ? ")
                                                               .append(",address_id = ? ")
                                                               .append(",birth_date = ? ")
                                                               .append("WHERE id = ?")
                                                               .toString();
    
    public static final String DELETE_PERSON_QUERY = new StringBuilder("DELETE FROM `jdbc`.person")
                                                               .append(" WHERE id=?")
                                                               .toString();

    public static final String EMAIL_UNIQUE = new StringBuilder("SELECT person.id ")
                                                        .append("FROM `jdbc`.person ")
                                                        .append("WHERE person.email = ?")
                                                        .toString();
    
    public static final String GET_ADDRESSID = new StringBuilder("SELECT person.address_id ")
                                                         .append("FROM `jdbc`.person ")
                                                         .append("WHERE person.id = ?")
                                                         .toString();
    
    public static final String NUMBER_OF_ADDRESS_RECORDS = new StringBuilder("SELECT COUNT(*) ")
                                                                     .append("FROM `jdbc`.address ")
                                                                     .toString();
    
    public static final String PERSON_TABLE_SIZE = new StringBuilder("SELECT COUNT(*) ")
                                                             .append("FROM `jdbc`.person ")
                                                             .toString();
    
    public static final String SEARCH_ADDRESS_QUERY = new StringBuilder("SELECT address.id ")
                                                                .append(",address.street ")
                                                                .append(",address.city ")
                                                                .append(",address.postal_code ")
                                                                .append("FROM `jdbc`.address ")
                                                                .append("WHERE CONCAT_WS(street, city, postal_code) ")
                                                                .append("Like ?" )
                                                                .toString();
    
    public static final String NAME_UNIQUE = new StringBuilder("SELECT person.id ")
                                                       .append(", person.first_name ")
                                                       .append(", person.last_name ")
                                                       .append("FROM `jdbc`.person ")
                                                       .append("WHERE person.first_name = ? ")
                                                       .append("AND person.last_name = ?")
                                                       .toString();
                        
    public static final String GET_ADDRESS_ID = new StringBuilder("SELECT address.id ")
                                                          .append("FROM `jdbc`.address ")
                                                          .append("WHERE address.street = ? ")
                                                          .append("AND address.city = ? ")
                                                          .append("AND address.postal_code = ? ")
                                                          .toString();
}
