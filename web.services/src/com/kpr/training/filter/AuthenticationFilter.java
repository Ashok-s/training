package com.kpr.training.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.service.ConnectionService;
import com.kpr.training.jdbc.service.PersonService;
import com.kpr.training.json.handler.JsonUtil;
import com.kpr.training.servlet.AuthenticationServlet;

public class AuthenticationFilter implements Filter{
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) {
        
            AuthenticationServlet authenticationServlet = new AuthenticationServlet();
            authenticationServlet.authenticate(request, response);
        
        try {
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        } 
        
    }
}
