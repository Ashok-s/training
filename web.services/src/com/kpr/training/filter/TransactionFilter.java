package com.kpr.training.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.kpr.training.jdbc.service.ConnectionService;

public class TransactionFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) {
        
        ConnectionService.init();
        try {
            filterChain.doFilter(req, res);
        } catch (Exception e) {
            e.printStackTrace();
        } 
        
    }

}
