package com.kpr.training.jdbc.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class Property {

    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.setProperty("url", "jdbc:mysql://localhost:3306/?user=root?autoReconnect=true&useSSL=false");
        properties.setProperty("username", "root");
        properties.setProperty("password", "Ashok@2001");
        properties.setProperty("schema", "jdbc");
        properties.setProperty("port", "3306");
        properties.setProperty("maximumPoolSize", "3");
        properties.setProperty("driverClassName", "com.mysql.cj.jdbc.Driver");
        
        OutputStream output = new FileOutputStream("db.properties");
        properties.store(output, "Key and Value");
        System.out.println("Properties stored successful");

        properties.list(System.out);
    }
}
