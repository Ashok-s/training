/*
Requirement:
    To perform the CRUD operation of the Person.
 
Entity:
    1.Person
    2.PersonService
    3.AppException
    4.ErrorCode
 
Function declaration:
    public long create(Person person, Address address)
    public Person read(long id, boolean addressFlag)
    public ArrayList<Person> readAll()
    public void update(long id, Person person, Address address)
    public void delete(long id)

Jobs To Be Done:
    1. Create a Person.
    2. Read a record in the Person.
    3. Read all the record in the Person.
    4. Update a Person.
    5. Delete a Person.
*/

package com.kpr.training.jdbc.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

public class PersonService {

    private Connection con;
    private AddressService addressService;
    private ResultSet resultSet;
    private Person person;
    private long addressId;
    private ArrayList<Person> persons;
    
    {
        addressService = new AddressService();
        persons = new ArrayList<>();
    }
    
    public long create(Person person) {

        con = ConnectionService.get();
        checkPerson(person, con);
        
        if (person.getAddress() != null) {
            addressId = addressService.create(person.getAddress());
        } else {
            addressId = 0;
        }
        
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.CREATE_PERSON_QUERY,
                PreparedStatement.RETURN_GENERATED_KEYS)) {

            setRecord(person, ps);           
            if (ps.executeUpdate() == 0 || !(resultSet = ps.getGeneratedKeys()).next()) {
                throw new AppException(ErrorCode.PERSON_CREATION_FAILS);
            }
            
            return resultSet.getLong(Constant.GENERATED_ID);
        } catch (Exception e) {
            ConnectionService.rollback();
            throw new AppException(ErrorCode.PERSON_CREATION_FAILS, e);
        }
 
    }

    public Person read(long id, boolean addressFlag) {

        try (PreparedStatement ps = ConnectionService.get()
                 .prepareStatement(QueryStatement.READ_PERSON_QUERY)) {

            ps.setLong(1, id);
            
            if ((resultSet = ps.executeQuery()).next()) {
                person = getRecord(resultSet);
                if(addressFlag) {
                    person.setAddress(addressService.read(resultSet.getLong(Constant.ADDRESS_ID)));
                }
            }
            return person;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
        }     
    }

    public ArrayList<Person> readAll() {

        try (ResultSet resultSet = ConnectionService.get()
             .prepareStatement(QueryStatement.READ_ALL_PERSON_QUERY).executeQuery()) {

            while (resultSet.next()) {
                Person person = getRecord(resultSet);
                person.setAddress(addressService.read(resultSet.getLong("address_id")));
                persons.add(person);
            }
            return persons;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
        }
    }

    public void update(Person person) {
        
        con = ConnectionService.get();
        checkPerson(person, con);
    
        if (person.getAddress() == null && getAddressId(person.getId()) == 0) {
            addressId = 0;
        } else if (person.getAddress() != null && getAddressId(person.getId()) == 0) {
            addressId = addressService.create(person.getAddress());
        } else if ((person.getAddress() != null && getAddressId(person.getId()) != 0)) {
            person.getAddress().setId(getAddressId(person.getId()));
            addressService.update(person.getAddress());
            addressId = getAddressId(person.getId());
        } else if ((person.getAddress() == null && getAddressId(person.getId()) != 0)) {
            addressId = getAddressId(person.getId());
        }       
        
        try (PreparedStatement ps = ConnectionService.get()
               .prepareStatement(QueryStatement.UPDATE_PERSON_QUERY)) {
            
            setRecord(person, ps);
            ps.setLong(6, person.getId());
            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.PERSON_UPDATION_FAILS);
            }
            
        } catch (Exception e) {
            ConnectionService.rollback();
            throw new AppException(ErrorCode.PERSON_UPDATION_FAILS, e);
        }
        
    }

    public void delete(long id) {
        
        try (PreparedStatement ps = ConnectionService.get()
               .prepareStatement(QueryStatement.DELETE_PERSON_QUERY)) {

            ps.setLong(1, id); 
            
            addressId = getAddressId(id);
            
            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.PERSON_DELETION_FAILS);
            }
            
            if (addressId != 0) {
                addressService.delete(addressId);
            }
            
        } catch (Exception e) {
            ConnectionService.rollback();
            throw new AppException(ErrorCode.PERSON_DELETION_FAILS, e);
        }
    }


    public void checkPerson(Person person, Connection con) {
        
        int count = 0;
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.NAME_UNIQUE)) {
            
            ps.setString(1, person.getFirstName());
            ps.setString(2, person.getLastName());
            resultSet = ps.executeQuery();
            
            String name = person.getFirstName() + person.getLastName();
            while (resultSet.next()) {
                if (name.equalsIgnoreCase
                        (resultSet.getString("first_name") + resultSet.getString("last_name")) 
                   && resultSet.getLong("id") != person.getId()) {
                    count = 1;
                    break;
                } else {
                    continue;
                }
            }
     
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_NAME, e);
        }
        
        if (count > 0) {
            throw new AppException(ErrorCode.NAME_NOT_UNIQUE);
        }
        
        long personId = 0;
        try (PreparedStatement ps = con.prepareStatement(QueryStatement.EMAIL_UNIQUE)) {
    
            ps.setString(1, person.getEmail());
             
            if ((resultSet = ps.executeQuery()).next()) {
                personId = resultSet.getLong(Constant.ID);
            }         
             
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_EMAIL, e);
        }   
        long id = person.getId();
        if ((id == 0 && personId > 0) || (personId > 0 && personId != id)) {
            throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
        }
    }   
        
    public long getAddressId(long id) {
        
        try (PreparedStatement ps = ConnectionService.get()
            .prepareStatement(QueryStatement.GET_ADDRESSID)) {
            
            ps.setLong(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                addressId = resultSet.getLong(Constant.ADDRESS_ID);
            }
            return addressId;  
            
        } catch (Exception e) {
            throw new AppException(ErrorCode.GETTING_ADDRESSID_FAILS, e);
        }
     
    }
    
    public Person getRecord(ResultSet resultSet) {
        
        try {
            Person person = new Person(resultSet.getString(Constant.FIRST_NAME)
                               ,resultSet.getString(Constant.LAST_NAME)
                               ,resultSet.getString(Constant.EMAIL),
                     new java.util.Date(resultSet.getDate(Constant.BIRTH_DATE).getTime()));
            person.setCreatedDate(resultSet.getTimestamp(Constant.CREATED_DATE));
            person.setId(resultSet.getLong("id"));
            return person;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
        }
      
    } 
    
    public java.util.Date dateValidator(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date utilDate = null;
        boolean valid = false;
        
        try {
            LocalDate.parse(date,
                    DateTimeFormatter.ofPattern("dd-MM-uuuu")
                            .withResolverStyle(ResolverStyle.STRICT));
            
            valid = true;
        } catch (Exception e) {
            valid = false;
            throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
        }
        
        if (valid == true) {
            try {
                utilDate = formatter.parse(date);
            } catch (Exception e) {
                throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
            }
        }
        return utilDate;
    }
        
    public void setRecord(Person person, PreparedStatement ps) {   
            
        try {
                
            ps.setString(1, person.getFirstName());
            ps.setString(2, person.getLastName());
            ps.setString(3, person.getEmail());
            ps.setDate(4, new java.sql.Date(person.getBirthDate().getTime()));
            ps.setLong(5, addressId);
        } catch (Exception e) {
            throw new AppException(ErrorCode.SET_ADDRESS_FAILS);
        }  
   }
    
    public long getAddressId(Connection con, Address address) {

        try (PreparedStatement ps = con.prepareStatement(QueryStatement.GET_ADDRESS_ID)) {

            if (address == null) {
                addressId = 0;
            }
            addressService.setRecord(address, ps);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                addressId = resultSet.getLong(Constant.ID);
            }

            return addressId;

        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_CREATION_FAILS, e);

        }
    }
    
    public ArrayList<Person> readCsvFile(String fileName) {
        
            BufferedReader fileReader = null;
    
            try {
    
                String line = "";
                fileReader = new BufferedReader(new FileReader(fileName));
                fileReader.readLine();
    
                while ((line = fileReader.readLine()) != null) {
                    String[] tokens = line.split(",");
                    if (tokens.length > 0) {
                        Address address = null;
                        Person person = new Person(tokens[0], tokens[1], tokens[2],
                                dateValidator(tokens[3]));
                        if (tokens[4] != "NULL" && tokens[5] != "NULL"
                                && Integer.parseInt(tokens[6]) != 0) {
                            address = new Address(tokens[4], tokens[5], Integer.parseInt(tokens[6]));
                        }
    
                        person.setAddress(address);
                        persons.add(person);
                    }
                }
            } catch (Exception e) {
                throw new AppException(ErrorCode.CSV_FILEREADING_FAILS, e);
            } finally {
                try {
                    fileReader.close();
                } catch (Exception e) {
                    throw new AppException(ErrorCode.CLOSING_CSVFILE_FAILS, e);
                }
            }
            
            return persons;
    
        }
}
