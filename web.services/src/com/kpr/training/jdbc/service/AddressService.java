/*
Requirement:
	To perform the CRUD operation of the Address.
 
Entity:
	1.Address
    2.AddressService
    3.AppException
    4.ErrorCode
 
Function declaration:
	public long create(Connection con, Address address) {}
    public Address read(Connection con, long id) {}
    public ArrayList<Address> readAll(Connection con) {}
    public void update(Connection con, long id, Address address) {}
    public void delete(Connection con, long id) {}
Jobs To Be Done:
    1. Create a Address.
    2. Read a record in the Address.
    3. Read all the record in the addresses.
    4. Update an Address.
    5. Delete an Address.
*/

package com.kpr.training.jdbc.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;

public class AddressService {

    private Address address;
    private ResultSet resultSet;
    private ArrayList<Address> addresses;
    
    {
        addresses = new ArrayList<>();
    }

    public long create(Address address) {

        validateAddress(address);

        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.CREATE_ADDRESS_QUERY, PreparedStatement.RETURN_GENERATED_KEYS)) {

            setRecord(address, ps);
            if (ps.executeUpdate() == 0 || !(resultSet = ps.getGeneratedKeys()).next()) {
                throw new AppException(ErrorCode.PERSON_CREATION_FAILS);
            }
            
            return resultSet.getLong(Constant.GENERATED_ID);
        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_CREATION_FAILS, e); // doubt
        }
        
    }

    public Address read(long id) {

        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.READ_ADDRESS_QUERY)) {

            ps.setLong(1, id);
            if (!(resultSet = ps.executeQuery()).next()) {
                return null;
            }
            return getRecord(resultSet);
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILS, e);
        }
    }

    public ArrayList<Address> readAll() {

        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.READ_ALL_ADDRESS_QUERY)) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                addresses.add(getRecord(resultSet));
            }
            return addresses; 

        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILS, e);
        }
    }

    public void update(Address address) {

        validateAddress(address);

        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.UPDATE_ADDRESS_QUERY)) {

            setRecord(address, ps);
            ps.setLong(4, address.getId());

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILS);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILS, e);
        }

    }

    public void delete(long id) {

        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.DELETE_ADDRESS_QUERY)) {

            ps.setLong(1, id);
            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.ADDRESS_DELETION_FAILS);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_DELETION_FAILS, e);
        }
    }

    public void validateAddress(Address address) {

        String street = address.getStreet();
        String city = address.getCity();
        if (street == null || street.equals(" ")) {
            throw new AppException(ErrorCode.STREET_NAME_ERROR);
        }

        if (city == null || city.equals(" ")) {
            throw new AppException(ErrorCode.CITY_NAME_ERROR);
        }

        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.POSTAL_CODE_ZERO);
        }
    }

    public void setRecord(Address address, PreparedStatement ps) {

        try {
            ps.setString(1, address.getStreet());
            ps.setString(2, address.getCity());
            ps.setInt(3, address.getPostalCode());
        } catch (Exception e) {
            throw new AppException(ErrorCode.SET_ADDRESS_FAILS);
        }
    }

    public Address getRecord(ResultSet resultSet) {

        try {
            address = new Address(resultSet.getString(Constant.STREET), resultSet.getString(
                    Constant.CITY), resultSet.getInt(Constant.POSTAL_CODE));
            address.setId(resultSet.getLong(Constant.ID));
            return address;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILS, e);
        }
    }

    public ArrayList<Address> search(String string) {

        if(string.isEmpty()) {
            return null;
        }

        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.SEARCH_ADDRESS_QUERY)) {

            ps.setString(1, "%" + string + "%");
 
            resultSet = ps.executeQuery();
            while (resultSet.next()) {
                address = getRecord(resultSet);
                addresses.add(address);
            }

            return addresses;
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_SEARCH_FAILS);
        }

    }
}
