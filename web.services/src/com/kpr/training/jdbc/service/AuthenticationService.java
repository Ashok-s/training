package com.kpr.training.jdbc.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Person;

public class AuthenticationService {
    
    public String authenticate(Person person) {
        
//        long id = person.getId();
//        String user = person.getUser();
//        String password = person.getPassword();
        
        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.AUTHENTICATE_QUERY)) {

            ps.setString(1, person.getUser());
            ps.setString(2, person.getPassword());
            ResultSet resultSet = ps.executeQuery();
            if (!(resultSet.next())) {
                throw new AppException(ErrorCode.PERSON_CREATION_FAILS);
            }
            
            return resultSet.getString(Constant.ROLL);
        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_CREATION_FAILS, e); // doubt
        }
        
        
    }

}
