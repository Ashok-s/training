package com.kpr.training.jdbc.constant;

public class QueryStatement {

    public static final String CREATE_ADDRESS_QUERY   
                          = new StringBuilder("INSERT INTO address (street, city, postal_code) ")
                                      .append("VALUES (?, ?, ?)                                ")
                                      .toString();

    public static final String READ_ADDRESS_QUERY     
                          = new StringBuilder("SELECT address.id          ")
                                      .append("      ,address.street      ")
                                      .append("      ,address.city        ")
                                      .append("      ,address.postal_code ")
                                      .append("  FROM address        ")
                                      .append(" WHERE `id` = ?            ")
                                      .toString();
    
    public static final String READ_ALL_ADDRESS_QUERY 
                          = new StringBuilder("SELECT address.id          ")
                                      .append("      ,address.street      ")
                                      .append("      ,address.city        ")
                                      .append("      ,address.postal_code ")
                                      .append("  FROM address        ")
                                      .toString();
    
    public static final String UPDATE_ADDRESS_QUERY 
                          = new StringBuilder("UPDATE address         ")
                                      .append("   SET street = ?      ")
                                      .append("      ,city = ?        ")
                                      .append("      ,postal_code = ? ")
                                      .append(" WHERE id = ?          ")
                                      .toString();
    
    public static final String DELETE_ADDRESS_QUERY
                          = new StringBuilder("DELETE FROM address")
                                      .append(" WHERE id=?        ")
                                      .toString();

    public static final String CREATE_PERSON_QUERY
    = new StringBuilder("INSERT INTO person (first_name, last_name, email, birth_date, address_id)")
                .append("VALUES (?, ?, ?, ?, ?)                                                   ")
                .toString();

    public static final String READ_PERSON_QUERY
                          = new StringBuilder("SELECT person.id           ")
                                      .append("      ,person.first_name   ")
                                      .append("      ,person.last_name    ")
                                      .append("      ,person.email        ")
                                      .append("      ,person.address_id   ")
                                      .append("      ,person.birth_date   ")
                                      .append("      ,person.created_date ")
                                      .append("  FROM person              ")
                                      .append(" WHERE id = ?              ")
                                      .toString();

    public static final String READ_ALL_PERSON_QUERY
                          = new StringBuilder("SELECT person.id           ")
                                      .append("      ,person.first_name   ")
                                      .append("      ,person.last_name    ")
                                      .append("      ,person.email        ")
                                      .append("      ,person.address_id   ")
                                      .append("      ,person.birth_date   ")
                                      .append("      ,person.created_date ")
                                      .append("  FROM person              ")
                                      .toString();

    public static final String UPDATE_PERSON_QUERY 
                          = new StringBuilder("UPDATE person         ")
                                      .append("   SET first_name = ? ")
                                      .append("      ,last_name = ?  ")
                                      .append("      ,email = ?      ")
                                      .append("      ,birth_date = ? ")
                                      .append("      ,address_id = ? ")
                                      .append(" WHERE id = ?         ")
                                      .toString();
    
    public static final String DELETE_PERSON_QUERY
                          = new StringBuilder("DELETE FROM person ")
                                      .append(" WHERE id=?        ")
                                      .toString();

    public static final String EMAIL_UNIQUE
                          = new StringBuilder("SELECT person.id        ")
                                      .append("  FROM person           ")
                                      .append(" WHERE person.email = ? ")
                                      .toString();
    
    public static final String GET_ADDRESSID 
                          = new StringBuilder("SELECT person.address_id ")
                                      .append("  FROM person              ")
                                      .append(" WHERE person.id = ?      ")
                                      .toString();
    
    public static final String NUMBER_OF_ADDRESS_RECORDS 
                          = new StringBuilder("SELECT COUNT(id) ")
                                      .append("  FROM address   ")
                                      .toString();
    
    public static final String NUMBER_OF_PERSON_RECORDS
                          = new StringBuilder("SELECT COUNT(id) ")
                                       .append("  FROM person   ")
                                       .toString();
    
    public static final String SEARCH_ADDRESS_QUERY 
                          = new StringBuilder("SELECT address.id                           ")
                                      .append("      ,address.street                       ")
                                      .append("      ,address.city                         ")
                                      .append("      ,address.postal_code                  ")
                                      .append("  FROM address                              ")
                                      .append(" WHERE CONCAT_WS(street, city, postal_code) ")
                                      .append("  LIKE ?                                    ")
                                      .toString();

    public static final String NAME_UNIQUE
                          = new StringBuilder("SELECT person.id             ")
                                      .append("      ,person.first_name     ")
                                      .append("      ,person.last_name      ")
                                      .append("  FROM person                ")
                                      .append(" WHERE person.first_name = ? ")
                                      .append("   AND person.last_name = ?  ")
                                      .toString();

    public static final String GET_ADDRESS_ID
                          = new StringBuilder("SELECT address.id              ")
                                      .append("  FROM address                 ")
                                      .append(" WHERE address.street = ?      ")
                                      .append("   AND address.city = ?        ")
                                      .append("   AND address.postal_code = ? ")
                                      .toString();
    
    public static final String AUTHENTICATE_QUERY
                          = new StringBuilder("SELECT person.roll          ")
                                      .append("  FROM person               ")
                                      .append(" WHERE person.user_name = ? ")
                                      .append("   AND person.password = ?  ")
                                      .toString();
}
