package com.kpr.training.jdbc.exception;

public enum ErrorCode {

	POSTAL_CODE_ZERO("ERR401", "postal code should not be zero"),
	READING_ADDRESS_FAILS("ERR402", "Failed to read address"),
	ADDRESS_CREATION_FAILS("ERR403", "Address was not created"),
	ADDRESS_UPDATION_FAILS("ERR404", "Failed to update Address"),
	ADDRESS_DELETION_FAILS("ERR405", "Failed to delete Address"),
	PERSON_CREATION_FAILS("ERR406", "Person was not created"),
	PERSON_UPDATION_FAILS("ERR407", "Failed to update Person"),
	PERSON_DELETION_FAILS("ERR408", "Failed to delete Person"),
	CONNECTION_FAILS("ERR409", "Failed to initiate connection"),
	EMAIL_NOT_UNIQUE("ERR410", "Email should be unique"),
	CONNECTION_FAILS_TO_CLOSE("ERR411", "Failed to close the connection"),
	FAILED_TO_COMMIT("ERR412", "Failed to commit"),
	FAILED_TO_ROLLBACK("ERR413", "Failed to rollback"),
	FAILED_TO_CHECK_EMAIL("ERR414", "Failed to check email"),
	READING_PERSON_FAILS("ERR415", "Failed to read person"),
	NAME_NOT_UNIQUE("ERR416", "Name should be unique"),
	READING_ADDRESSID_FAILS("ERR417", "Failed to read address id"),
	FAILED_TO_CHECK_NAME("ERR418", "Failed to check name"),
	WRONG_DATE_FORMAT("ERR419", "Date should be in dd-mm-yyyy format"),
	FAILED_TO_GET_ADDRESS_SIZE("ERR420", "Failed to get the size of address"),
	FAILED_TO_GET_PERSON_SIZE("ERR421", "Failed to get the size of person"),
	ADDRESS_SEARCH_FAILS("ERR422", "Failed to search Adderss"),
	GETTING_ADDRESSID_FAILS("ERR423", "Failed to get address id"),
	CSV_FILEREADING_FAILS("ERR424", "Error in CsvFileReader !!!"),
	CLOSING_CSVFILE_FAILS("ERR425", "Error in closing csv file"),
	SET_ADDRESS_FAILS("ERR426", "Error in setting fields in querry"),
	STREET_NAME_ERROR("ERR427", "street name should be valid and mandatory"),
	CITY_NAME_ERROR("ERR427", "city name should be valid and mandatory");

    private final String code;
    private final String message;
    
    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
