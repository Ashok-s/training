package com.kpr.training.servlet;

import java.io.BufferedReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.service.AddressService;
import com.kpr.training.jdbc.service.ConnectionService;
import com.kpr.training.json.handler.JsonUtil;

public class AddressServlet extends HttpServlet {
    
    static JsonUtil jsonUtil = new JsonUtil();
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer addressJson = new StringBuffer();
        String line = null;
       
        AddressService addressService = new AddressService();
        Address address = new Address();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                addressJson.append(line); 
            address = (Address) jsonUtil.getJavaObject(addressJson.toString(), address);
            long addressId = addressService.create(address);
            ConnectionService.commit();
            PrintWriter writer = response.getWriter();
            writer.append("Your Address Id = :" + addressId);
        } catch (Exception e) {
            e.printStackTrace();;
        } 
    }
    
    public void doPut(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer addressJson = new StringBuffer();
        String line = null;
       
        AddressService addressService = new AddressService();
        Address address = new Address();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                addressJson.append(line); 
            Address address1 = (Address) jsonUtil.getJavaObject(addressJson.toString(), address);
            addressService.update(address1);
            ConnectionService.commit();
            PrintWriter writer = response.getWriter();
            writer.append("Updated Successfully");
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer addressJson = new StringBuffer();
        String line = null;
       
        AddressService addressService = new AddressService();
        Address address = new Address();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                addressJson.append(line); 
            Address address1 = (Address) jsonUtil.getJavaObject(addressJson.toString(), address);
            ConnectionService.init();
            Address address2 = addressService.read(address1.getId());
            String address3 = jsonUtil.getJsonString(address2);
            PrintWriter writer = response.getWriter();
            writer.append(address3);
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    public void doDelete(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer addressJson = new StringBuffer();
        String line = null;
       
        AddressService addressService = new AddressService();
        Address address = new Address();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                addressJson.append(line); 
            Address address1 = (Address) jsonUtil.getJavaObject(addressJson.toString(), address);
            addressService.delete(address1.getId());
            ConnectionService.commit();
            PrintWriter writer = response.getWriter();
            writer.append("Address deleted successfully");
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }

//        
//        public static void main(String[] args) {
//            
//            String addresJson = "{\r\n" + 
//                    "                \"street\" : \"Milky street\",\r\n" + 
//                    "                \"city\" : \"Galaxy\",\r\n" + 
//                    "                \"postalCode\" : \"6265451\"\r\n" + 
//                    "            }";
//            
//            JsonUtil jsonUtil = new JsonUtil();
//            AddressService addressService = new AddressService();
//            Address address1 = new Address();
//            Address address2 = (Address) jsonUtil.getJavaObject(addresJson, address1);
//            AddressServlet add = new AddressServlet();
//          //  add.doPost(request, response);
//            System.out.println(address2.getPostalCode());
//            System.out.println(address2);
//           // System.out.println(jsonUtil.getJsonString(address));
//            ConnectionService.init();
//            System.out.println(addressService.create(new Address(address2.getStreet(), address2.getCity(), address2.getPostalCode())));
//        }
//        
    
}
    
    
    
   

   

    
