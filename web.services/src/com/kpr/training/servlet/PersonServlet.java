/*
---------wbs-----------
Requirement
Implement doPost() for PersonServlet

Entity
PersonServlet

Method Signature
void doPost(HttpServletRequest req, HttpServletResponse res)

Jobs to be done
 1. Get person json from HttpServletRequest req body and store it in field personJson of type string.
 2. Parse personJson into person of type Person.
 3. Prepare personService of type PersonService
 4. Invoke create method using personService and pass person as parameter, store the generated id in personId of type long
 5. Parse personId into Json string and set it in httpservleresponse res body.
*/
package com.kpr.training.servlet;

import java.io.BufferedReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.service.AddressService;
import com.kpr.training.jdbc.service.ConnectionService;
import com.kpr.training.jdbc.service.PersonService;
import com.kpr.training.json.handler.JsonUtil;

public class PersonServlet extends HttpServlet {

static JsonUtil jsonUtil = new JsonUtil();
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer personJson = new StringBuffer();
        String line = null;
       
        PersonService personService = new PersonService();
        Person person = new Person();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                personJson.append(line); 
            person = (Person) jsonUtil.getJavaObject(personJson.toString(), person);
            long personId= personService.create(person);
            ConnectionService.commit();
            PrintWriter writer = response.getWriter();
            writer.append("Your Address Id = :" + personId);
        } catch (Exception e) {
            e.printStackTrace();;
        } 
    }
    
    public void doPut(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer personJson = new StringBuffer();
        String line = null;
       
        PersonService personService = new PersonService();
        Person person = new Person();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                personJson.append(line); 
            Person person1 = (Person) jsonUtil.getJavaObject(personJson.toString(), person);
            personService.update(person1);
            ConnectionService.commit();
            PrintWriter writer = response.getWriter();
            writer.append("Updated Successfully");
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer personJson = new StringBuffer();
        String line = null;
       
        PersonService personService = new PersonService();
        Person person = new Person();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                personJson.append(line); 
            Person person1 = (Person) jsonUtil.getJavaObject(personJson.toString(), person);
            ConnectionService.init();
            Person person2 = personService.read(person1.getId(), true);
            String address3 = jsonUtil.getJsonString(person2);
            PrintWriter writer = response.getWriter();
            writer.append(address3);
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    public void doDelete(HttpServletRequest request, HttpServletResponse response) {

        StringBuffer personJson = new StringBuffer();
        String line = null;
       
        PersonService personService = new PersonService();
        Person person = new Person();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                personJson.append(line); 
            Person person1 = (Person) jsonUtil.getJavaObject(personJson.toString(), person);
            personService.delete(person1.getId());
            ConnectionService.commit();
            PrintWriter writer = response.getWriter();
            writer.append("Address deleted successfully");
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
}
