package com.kpr.training.servlet;

import java.io.BufferedReader;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.service.AuthenticationService;
import com.kpr.training.jdbc.service.PersonService;
import com.kpr.training.json.handler.JsonUtil;

public class AuthenticationServlet {

    static JsonUtil jsonUtil = new JsonUtil();
    
    public void authenticate(ServletRequest request, ServletResponse respnse) {
        
        StringBuffer personJson = new StringBuffer();
        String line = null;
       
        PersonService personService = new PersonService();
        Person person = new Person();

        try (BufferedReader reader = request.getReader()) {

            while ((line = reader.readLine()) != null)
                personJson.append(line); 
            person = (Person) jsonUtil.getJavaObject(personJson.toString(), person);
            AuthenticationService authenticationService = new AuthenticationService();
            String roll = authenticationService.authenticate(person);
        } catch (Exception e) {
            e.printStackTrace();;
        } 
        
    }
}
