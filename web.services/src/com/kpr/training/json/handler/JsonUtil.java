package com.kpr.training.json.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {
  
    public Object getJavaObject(String jsonString, Object javaObject)  {  
        return (new GsonBuilder().setDateFormat("yyyy-MM-dd").create()).fromJson(jsonString, javaObject.getClass()); 
    } 
    
    public String getJsonString(Object javaObject) {
        return (new Gson()).toJson(javaObject);
    }
    
}
