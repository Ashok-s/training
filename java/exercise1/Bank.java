package com.banking.training.bank;

import java.util.Scanner;

public class Bank {
    public String name;
    public int id;
    public String address;
    
    public Bank(int bankId) {
        id = bankId;
    }
    
    {
        address = "Sulur, Coimbatore, TamilNadu";
    }
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int id = scanner.nextInt();
        Bank bank = new Bank(id);
        System.out.println(name);
        System.out.println(bank.id);
        System.out.println(bank.address);
    }
}
