package com.banking.training.atm;

import com.banking.training.person.Person;
import java.util.Scanner;

public class Atm {

    public static void main(String[] args) {
        Atm atm = new Atm();
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int acNo = scanner.nextInt();
        int aadhaarNo = scanner.nextInt();
        int amount = scanner.nextInt();
        atm.printDetail(int acNo, int aadhaarNo);     // shows error due to private access specifier
        com.banking.training.person.Person person = new com.banking.training.person.Person();
        person.printName(name);
        person.printBalance(amount);    // shows error due to protected access specifier
    }
}
