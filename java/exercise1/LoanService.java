package com.banking.training.service;

public class LoanService implements Service {

    public void provide() {
        System.out.println("provide the Loan");
    }

    public void initiate() {
        System.out.println("provide the cash");
    }

    public void delete() {
        System.out.println("delete the history");
    }
}
