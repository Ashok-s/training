package com.banking.training.bank;

public abstract class Loan {

    public void request(int amount) {
        System.out.println("requesting for loan for " + amount + " lakh");
    }

    protected abstract String provide();
}


class Employee extends Loan {

    protected String provide() {
        return ("provide 10 lakh");
    }
}

class Customer extends Loan{

    public String provide(){
        return ("provide 5 lakh");
    }
}
