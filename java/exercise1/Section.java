package com.banking.training.section;

public class Section {
    public String name;
    public int noOfEmployee;
    public int counterNo;
      
    public Section() {
        this.name = "major section";
        this.noOfEmployee = 24;
        this.counterNo = 1;
    }
    public void printName() {
        System.out.println(this.name);
    }
    
    private void printNoOfEmployee() {
        System.out.println(this.noOfEmployee);
    }
    
    protected void printCounterNo() {
        System.out.println(this.counterNo);
    }
    public class Cashier {
        public String name;
        public int age;
        public long mobileNo;
        
        public Cashier(){
        this.name = "Ashok";
        this.age = 19;
        this.mobileNo = 9988776655L;
        }
        
        public void printName() {
            System.out.println(this.name);
        }
    
        private void printAge() {
            System.out.println(this.age);
        }
    
        protected void printMobileNo() {
            System.out.println(this.mobileNo);
        }
    }
}