package com.banking.training.service;

public interface Service {

    public void provide();
    public void initiate();
    public void delete();
    
}