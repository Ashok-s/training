package com.banking.training.service;

public class MainService {

    public static void main(String[] args) {
        AtmService atm = new AtmService();
        atm.provide();
        atm.initiate();
        atm.delete();
        LoanService loan = new LoanService();
        loan.provide();
        loan.initiate();
        loan.delete();
    }
}
