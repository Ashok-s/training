package com.banking.training.person;

import java.util.Scanner;

public class Customer extends Person {

    public static void main(String[] args){
        Customer customer = new Customer();
        Scanner scanner = new Scanner(System.in);
        int amount = scanner.nextInt();
        int acNo = scanner.nextInt();
        int aadhaarNo = scanner.nextInt();
        Person person = new Person();
        customer.printBalance(amount);               //the method balance in class Person has protected access specifier.
        customer.printDetail(acNo, aadhaarNo);      //shows error because the method detail has private access specifier.
        
    }
}
