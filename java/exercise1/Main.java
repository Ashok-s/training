package com.banking.training.section;

public class Main {

    public static void main(String[] args) {
        Section section = new Section();
        Section.Cashier cashier = section.new Cashier();
        section.printName();
        section.printNoOfEmployee();    // shows error due to private access specifier.
        section.printCounterNo();
        cashier.printName();
        cashier.printAge();             // shows error due to private access specifier.
        cashier.printMobileNo();
    }
}
