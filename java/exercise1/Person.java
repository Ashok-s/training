package com.banking.training.person;

import java.util.Scanner;

public class Person {

    public static void printName(String name) {
        System.out.println("person name" + " " + name);
    }

    public static void printAddress() {
        System.out.println("person address Coimbatore, TamilNadu");
    }
    
    private static void printDetail(int acNo, int aadhaarNo) {
        System.out.println("Account Number" + " " + acNo);
        System.out.println("Aadhaar Number" + " " + aadhaarNo);
    }
    
    protected static void printBalance(int amount) {
        System.out.println("person balance " + amount);
    }

    public static void main(String[] args) {
        Person person = new Person();
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int acNo = scanner.nextInt();
        int aadhaarNo = scanner.nextInt();
        int amount = scanner.nextInt();
        person.printName(name);
        person.printAddress();
        person.printDetail(acNo, aadhaarNo);
        person.printBalance(amount);
    }
}
