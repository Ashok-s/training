package com.banking.training.service;

public class AtmService implements Service {

    public void provide() {
        System.out.println("provide the atm service");
    }

    public void initiate() {
        System.out.println("provide the atm card");
    }

    public void delete() {
        System.out.println("delete the history");
    }
}
