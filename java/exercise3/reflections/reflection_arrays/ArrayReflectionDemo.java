/*
Requirement:
    To code for creating java array using reflect array class and add elements to it.
    
Entity:
    ArrayReflectionDemo
    
Function Declaration:
    public static void main(String[] args);
    
Jobs to be done:
    1. Get the size for which the array is to be created and store it in integer variable as 
       sizeOfArray
    2. Create a variable intArray of type integer that stores the array created.
    3. Add the elements to it 
    4. Print the array.
    5. For each element in the array intArray
           5.1) get all elements and print it.
    
Pseudo code:
class ArrayReflectionDemo {

    public static void main(String[] args) {
        int sizeOfArray = 3;
        int[] intArray = (int[]) Array.newInstance(int.class, sizeOfArray);
        Array.set(intArray, 0, 25);
        Array.set(intArray, 1, 76);
        Array.set(intArray, 2, 33);
        System.out.println(intArray);
    }
}
*/

package com.kpriet.training.java.reflection_arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayReflectionDemo {
    
    public static void main(String[] args) {
        int sizeOfArray = 3;
        int[] intArray = (int[]) Array.newInstance(int.class, sizeOfArray);
        
        Array.setInt(intArray, 0, 25);
        Array.setInt(intArray, 1, 76);
        Array.setInt(intArray, 2, 33);
        
        System.out.println("The integer array is: " + Arrays.toString(intArray));
        
        for (int iteration = 0; iteration < sizeOfArray; iteration++) {
            System.out.println("The elements at index " + iteration + " is: " 
                                                        + Array.getInt(intArray, iteration));
        }
    }
}
