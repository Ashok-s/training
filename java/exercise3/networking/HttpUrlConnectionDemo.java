/*
Requirement:
    To create a program for httpUrlConnection.
    
Entity:
    HttpUrlConnectionDemo
    
Function Declaration:
    public static void main(String[] args); 
    
Jobs to be done:
    1. Create an instance for URL.
    2. Create instance for HttpUrlConnection that stores an object of UrlConnection class.
    3. print the response code for the URL
    
Pseudo code:
class HttpUrlConnectionDemo {
    
    public static void main(String[] args) throws Exception {   
            URL urlName = new URL("http://www.google.com");    
            HttpURLConnection connection=(HttpURLConnection)urlName.openConnection();  
            System.out.println(connection.getResponseCode());
    }
}
 */

package com.kpriet.training.java.networking;

import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUrlConnectionDemo {

	public static void main(String[] args) throws Exception {
		URL urlName = new URL("http://www.google.com");
		HttpURLConnection connection = (HttpURLConnection) urlName.openConnection();
		System.out.println(connection.getResponseCode());
	}
}


