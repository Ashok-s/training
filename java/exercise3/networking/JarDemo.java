/*
 * Requirement:
 *     To create an jar file inside eclipse
 *    
 * Entity:
 *    JarDemo
 *    
 * Method Signature:
 *    public static void main(String[] args);
 *    
 * Jobs to be done:
 *    1) Create a path instance for a file which has to be created.
 *    2) By using createFile method ,file will be created.
 *    
 * Pseudo code:
 * class JarDemo {
 * 
 * 	   public static void main(String[] args) throws IOException {
 *         Path sourceFile = Paths.get(
 *               "C:\\Users\\ashok\\eclipse-workspace\\javaee-demo\\src\\com\\kpriet\\training\\java\\networking\\JarCreated");
 *         Files.createFile(sourceFile);
 *         System.out.println("Jar file created successfully ");
 *     }
 * }
 */

package com.kpriet.training.java.networking;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JarDemo {

    public static void main(String[] args) throws IOException {
        Path sourceFile = Paths.get(
                "C:\\Users\\ashok\\eclipse-workspace\\javaee-demo\\src\\com\\kpriet\\training\\java\\networking\\JarCreated");
        Files.createFile(sourceFile);
        System.out.println("Jar file created successfully ");
    }
}
