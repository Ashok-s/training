/*
Requirement:
    To determine the IP address and host name of the local computer.
    
Entity:.
    IpAdressAndHostNameDemo
    
Function Declaration:
    public static void main(String[] args);
    
Jobs to be done:
    1. An object is created for InetAddress as inetAddress that stores the address of the local host.
    2. Print the IP address in the textual presentation.
    3. Print the local host name for that IP address.
    
Pseudo code:
class IpAdressAnd HostNameDemo {

    public static void main(String[] args) {
        InetAddress inetAddress = InetAddress.getLocalHost();
        System.out.println("IP Address " + inetAddress.getHostAddress());
        System.out.println("Host Name " + inetAddress.getHostName());
    }
}
 */

package com.kpriet.training.java.networking;

import java.net.InetAddress;

public class IPAdressAndHostNameDemo {
    
    public static void main(String[] args) throws Exception {
        InetAddress inetAddress = InetAddress.getLocalHost();
        System.out.println("IP Address: " + inetAddress.getHostAddress());
        System.out.println("HostName: " + inetAddress.getHostName());
    }
}
