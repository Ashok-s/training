/*
 * Requirements : 
 * 		Will the following class compile? If not, why?
 *  	public final class Algorithm {
 *          public static <T> T max(T x, T y) {
 *              return x > y ? x : y;
 *          }
 *      }
 *
 * Entities :
 * 		Algorithm.
 * 
 * Function Declaration :
 * 		public static <T> T max(T x, T y);
 * 
 * Jobs To Be Done:
 * 		1)Find whether the code compiles or not.
 */

/*
 * The code does not compile, because the greater than (>) operator applies only to primitive numeric types.
 */

package com.kpriet.training.java.generics;

public class Algorithm {

	public static <T> T max(T x, T y) {
        return (x > y) ? x : y;
    }
}
