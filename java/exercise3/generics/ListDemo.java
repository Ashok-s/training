/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for list.
 *
 * Entities :
 *    ListDemo 
 *    
 * Function Declaration :
 *    	public static void main(String[] args);
 *    
 * Jobs To Be Done:
 *     1.Create a new list and add elements to the list.
 *     2.Print the element of the list using index, iterator method and for loop.
 *     
 * Pseudo code:
 *    class ListDemo {
 *    
 *        public static void main(String[] args) {
 *	          ArrayList<String> names = new ArrayList<String>();
 *	          String firstName = "Ashok";
 *	          names.add(firstName);
 *	          names.add("Arya");
 *	          names.add("Surya");
 *	          names.add("Ajith");
 *	          names.add("Ajith");
 *	          System.out.println(names.get(1));
 *	          System.out.println("Iterating String list using while loop");
 *	          Iterator<String> stringIterator = names.iterator();
 *	          while (stringIterator.hasNext()) {
 *	              System.out.println(stringIterator.next());
 *	          }
 *	
 *	          System.out.println("Iterating String list using for loop");
 *	          for (String name : names) {
 *	              System.out.println(name);
 *	          }
 *	
 *	          ArrayList<Integer> ages = new ArrayList<>();
 *	          Scanner scanner = new Scanner(System.in);
 *	          for (String name : names) {
 *	              ages.add(scanner.nextInt());
 *	          }
 *	
 *	          scanner.close();
 *	          System.out.println("age at index 3 is : " + ages.get(3));
 *	          System.out.println("Iterating Integer list using while loop");
 *	          Iterator<Integer> intIterator = ages.iterator();
 *	          while (intIterator.hasNext()) {
 *	              System.out.println(intIterator.next());
 *	          }
 *	
 *	          System.out.println("Iterating Integer list using for loop");
 *	          for (int age : ages) {
 *	              System.out.println(age);
 *	          }
 *	      }
 *    }     
 */

package com.kpriet.training.java.generics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ListDemo {

    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<String>();
        String firstName = "Ashok";
        names.add(firstName);
        names.add("Arya");
        names.add("Surya");
        names.add("Ajith");
        names.add("Ajith");
        System.out.println(names.get(1));
        System.out.println("Iterating String list using while loop");
        Iterator<String> stringIterator = names.iterator();
        while (stringIterator.hasNext()) {
            System.out.println(stringIterator.next());
        }

        System.out.println("Iterating String list using for loop");
        for (String name : names) {
            System.out.println(name);
        }

        ArrayList<Integer> ages = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        for (String name : names) {
            ages.add(scanner.nextInt());
        }

        scanner.close();
        System.out.println("age at index 3 is : " + ages.get(3));
        System.out.println("Iterating Integer list using while loop");
        Iterator<Integer> intIterator = ages.iterator();
        while (intIterator.hasNext()) {
            System.out.println(intIterator.next());
        }

        System.out.println("Iterating Integer list using for loop");
        for (int age : ages) {
            System.out.println(age);
        }
    }
}
