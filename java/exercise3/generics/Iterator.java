package com.kpriet.training.java.generics;

/*Requirements:
 * 		To Find why except iterator() method in iterable interface, are not neccessary to define in the implemented class?
 * 
 * Entity:
 * 		Iterable
 * 
 * Function Declaration:
 * 		Iterator<T>    iterator();
 *      Spliterator<T> spliterator();
 *       void forEach(Consumer<? super T> action);
 * 
 * Jobs To Be Done:
 * 		1.The Iterable interface has 3 methods in it.
 *      2.Only one method is declared, and other two are default implementation.
 */
/*Solution:
 *  The Java Iterable interface has three methods of which only one needs to be implemented. The other two have default implementations.
 */