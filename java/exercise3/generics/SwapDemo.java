/*
Requirement:
    To write a generic Method to swap the elements.

Entity:
    SwapDemo

Function Declaration:
    public static <T> void swap(T[] numbers, int index1, int index2);
    public static void main(String[] args);

Jobs to be done:
    1.Get the list and the indexes to be swaped in that list
    2.Invoke the method swap with list and indexes as arguments.
    3.Define a method swap() of type T and swap the elements using temporary variables.
    4.Print the resultant list after swaping the elements.
    
Pseudo code:
class SwapDemo {

     public static <T> void swap(T[] numbers, int index1, int index2) {
		T temp = numbers[index1];
		numbers[index1] = numbers[index2];
		numbers[index2] = temp;
		System.out.println(Arrays.toString(numbers));
	}

	public static void main(String[] args) {
		System.out.println("enter the indexes to be swaped");
		Scanner scanner = new Scanner(System.in);
		int index1 = scanner.nextInt();
		int index2 = scanner.nextInt();
		scanner.close();
		Integer[] numbers = {100, 200, 300, 400, 500, 600, 700};
		System.out.println("List before swapping");
		System.out.println(Arrays.toString(numbers));
		System.out.println("List after swapping");
		swap(numbers, index1, index2);
	}	
}

 */

package com.kpriet.training.java.generics;

import java.util.Scanner;
import java.util.Arrays;

public class SwapDemo {
	
	public static <T> void swap(T[] numbers, int index1, int index2) {
		T temp = numbers[index1];
		numbers[index1] = numbers[index2];
		numbers[index2] = temp;
		System.out.println(Arrays.toString(numbers));
	}

	public static void main(String[] args) {
		System.out.println("enter the indexes to be swaped");
		Scanner scanner = new Scanner(System.in);
		int index1 = scanner.nextInt();
		int index2 = scanner.nextInt();
		scanner.close();
		Integer[] numbers = {100, 200, 300, 400, 500, 600, 700};
		System.out.println("List before swapping");
		System.out.println(Arrays.toString(numbers));
		System.out.println("List after swapping");
		swap(numbers, index1, index2);
	}
}
