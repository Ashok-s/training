/*
 * Requirements : 
 * 		What will be the output of the following program?

		public class UseGenerics {
    		public static void main(String args[]){  
        		MyGen<Integer> m = new MyGen<Integer>();  
        		m.set("merit");
        		System.out.println(m.get());
    		}
		}
		class MyGen<T> {
    		T var;
    		void  set(T var) {
        		this.var = var;
    		}
    		T get() {
        		return var;
    		}
		}

 * Entities :
 * 		UseGenerics.
 * 
 * Function Declaration :
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Explain what's wrong with this program.
 */
package com.kpriet.training.java.generics;

public class UseGenerics {
	
	public static void main(String args[]) {
		MyGen<Integer> integer  = new MyGen<Integer>();
		integer.set("merit");
		System.out.println(integer.get());
	}
}

class MyGen<T> {
	T var;

	void set(T var) {
		this.var = var;
	}
	T get() {
		return var;
	}
}
/*
 * It gives a compile time error because while creating the reference the generic type is given as Integer,
 * but String is passed as argument in set method.
 */ 
