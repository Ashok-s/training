/*
 * Requirements : 
 * 		Write a generic method to count the number of elements in a collection that have a specific
 * property (for example, odd integers, prime numbers, palindromes).
 *
 * Entities :
 * 		CountSpecificProperty.
 * 
 * Function Declaration :
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Create a new list and add the elements to that list.
 * 		2.Invoke the method count() with list as argument
 * 		3.Define the method count as
 *  		3.1)initialize a variable countNumber to zero.
 *  		3.2)For each of the element in the list 
 *  			3.2.1)check whether the element is divided by two.
 *  			3.2.2)If so increase the value of countNumber by 1
 *    		3.3)Return the value of countNumber.
 *    	4.Print the value of countNumber.
 *    
 * Pseudo code:
 * 	  class CountSpecificProperty {
 * 
 * 	  	  public static int count(ArrayList<Integer> list) {
 *			  int countNumber = 0;
 *			  for(int element : list) {
 *				  if(element % 2 != 0) {
 *					  countNumber++;
 *				  }
 *			  }
 *			  return countNumber;
 *		  }
 *	
 *		  public static void main(String[] args) {
 *			  ArrayList<Integer> list = new ArrayList<>();
 *			  list.add(1);
 *			  list.add(2);
 *			  list.add(3);
 *			  list.add(4);
 *			  list.add(5);
 *			  list.add(6);
 *			  list.add(7);
 *			  list.add(8);
 *			  list.add(9);
 *			  list.add(10);
 *			  list.add(11);
 *			  System.out.println("Number of odd integers : " + count(list));
 *	      }
 *    }    
 */
package com.kpriet.training.java.generics;

import java.util.ArrayList;

public class CountSpecificProperty {
	
	public static int count(ArrayList<Integer> list) {
		int countNumber = 0;
		for(int element : list) {
			if(element % 2 != 0) {
				countNumber++;
			}
		}
		return countNumber;
	}

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(7);
		list.add(8);
		list.add(9);
		list.add(10);
		list.add(11);
		System.out.println("Number of odd integers : " + count(list));

	}

}
