/*
 * Requirements : 
 *    To write a program to demonstrate generics - class objects as type literals.
 *
 * Entities :
 *    interface Animal
 *    
 * Function Declaration :
 *  	public void sound();
 *
 * Jobs To Be Done:
 *     1)Create an interface Animal and declare the method sound().
 *     
 * Pseudo code:
 *     interface Animal {
 *     
 *         public void sound();
 *     }
 */

package com.kpriet.training.java.generics;

interface Animal {

    public void sound();
}
