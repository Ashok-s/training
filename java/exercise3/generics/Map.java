/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for map.
 *
 * Entities :
 *    public class Map 
 *    
 * Function Declaration :
 *     public static <K, E> void printMap(HashMap<K, E> map);
 *     public static void main(String[] args);
 *    
 * Jobs To Be Done:
 *     1.Create two maps with specific type and add the key value pair to the map
 *     2.Invoke the method printMap with maps as argument
 *     3.Define the method with type 
 *     	   3.1)Till the end of the map print the key value pair of the map.
 *     
 * Pseudo code:
 *    class Map {
 *    
 *        public static <K, E> void printMap(HashMap<K, E> map) {
 *	          for (K key : map.keySet()) {
 *	              E value = map.get(key);
 *	              System.out.println(key + " : " + value);
 *	          }
 *	      }
 *	
 *	      public static void main(String[] args) {
 *	          HashMap<Integer, String> firstMap = new HashMap<Integer, String>();
 *	          Scanner scanner = new Scanner(System.in);
 *	          int range = scanner.nextInt();
 *	          for (int element = 0; element<range; element++) {
 *	              firstMap.put(scanner.nextInt(), scanner.nextLine());
 *	          }
 *	          printMap(firstMap);
 *	          scanner.close();
 *	          HashMap<String, Integer> secondMap = new HashMap<>();
 *	          for (int element = 0; element<range; element++) {
 *	              secondMap.put(scanner.nextLine(), scanner.nextInt());
 *	          }
 *	          printMap(secondMap);
 *	      }
 *    }
 */

package com.kpriet.training.java.generics;

import java.util.HashMap;
import java.util.Scanner;

public class Map {

    public static <K, E> void printMap(HashMap<K, E> map) {
        for (K key : map.keySet()) {
            E value = map.get(key);
            System.out.println(key + " : " + value);
        }
    }

    public static void main(String[] args) {
        HashMap<Integer, String> firstMap = new HashMap<Integer, String>();
        Scanner scanner = new Scanner(System.in);
        int range = scanner.nextInt();
        for (int element = 0; element<range; element++) {
            firstMap.put(scanner.nextInt(), scanner.nextLine());
        }
        printMap(firstMap);
        scanner.close();
        HashMap<String, Integer> secondMap = new HashMap<>();
        for (int element = 0; element<range; element++) {
            secondMap.put(scanner.nextLine(), scanner.nextInt());
        }
        printMap(secondMap);
    }
}
