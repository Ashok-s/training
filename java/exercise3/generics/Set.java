/*
 * Requirements : 
 *    To write a program to demonstrate generics - for loop for set.
 *
 * Entities :
 *    Set
 *    
 * Function Declaration :
 *    public static void main(String[] args);
 *    
 * Jobs To Be Done:
 *    1.Create a set and add elements to the set
 *    2.Print the element of the set using index, iterator method and for loop.
 *    
 * Pseudo code:
 *    class Set {
 * 
 *        public static void main(String[] args) {
 *	          HashSet<String> names = new HashSet<String>();
 *	          String firstName = "Ashok";
 *	          names.add(firstName);
 *	          names.add("Arya");
 *	          names.add("Surya");
 *	          names.add("Ajith");
 *	          names.add("Ajith");
 *	          System.out.println("Iterating String set using while loop");
 *	          Iterator<String> iterator = names.iterator();
 *	          while (iterator.hasNext()) {
 *	              System.out.println(iterator.next());
 *	          }
 *	
 *    	      System.out.println("Iterating String set using for loop");
 *	          for (String name : names) {
 *	              System.out.println(name);
 *	          }
 *	
 *	          HashSet<Integer> ages = new HashSet<>();
 *	          Scanner scanner = new Scanner(System.in);
 *	          for (String name : names) {
 *	              ages.add(scanner.nextInt());
 *	          }
 *	
 *	          scanner.close();
 *	          System.out.println("Iterating Integer set using while loop");
 *	          Iterator<Integer> intIterator = ages.iterator();
 *	          while (intIterator.hasNext()) {
 *	              System.out.println(intIterator.next());
 *	          }
 *	
 *	          System.out.println("Iterating Integer set using for loop");
 *	          for (int age : ages) {
 *	              System.out.println(age);
 *	          }
 *	      }
 *    }    
 */

package com.kpriet.training.java.generics;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Set {

    public static void main(String[] args) {
        HashSet<String> names = new HashSet<String>();
        String firstName = "Ashok";
        names.add(firstName);
        names.add("Arya");
        names.add("Surya");
        names.add("Ajith");
        names.add("Ajith");
        System.out.println("Iterating String set using while loop");
        Iterator<String> iterator = names.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("Iterating String set using for loop");
        for (String name : names) {
            System.out.println(name);
        }

        HashSet<Integer> ages = new HashSet<>();
        Scanner scanner = new Scanner(System.in);
        for (String name : names) {
            ages.add(scanner.nextInt());
        }

        scanner.close();
        System.out.println("Iterating Integer set using while loop");
        Iterator<Integer> intIterator = ages.iterator();
        while (intIterator.hasNext()) {
            System.out.println(intIterator.next());
        }

        System.out.println("Iterating Integer set using for loop");
        for (int age : ages) {
            System.out.println(age);
        }
    }
}
