/*
Requirement:
  	To write a program to print difference of two numbers using lambda expression
and the single method interface

Entity:
	public class NumberDifference
	interface Subractor

Function Declaration:
	public static void main(String[] args)
	public int Difference(int number1, int number2)

Jobs to be done:
	1.Get the numbers to be subtracted.
	2.Create the lambda expression for performing subtraction.
	3.Invoke the method and print the result.
	
Pseudo code:
interface Subtractor {
	public int difference(int number1, int number2);
}

class NumberDifference {

	public static void main(String[] args) 
        int number1 = 5;
        int number2 = 10;
        //create the lambda expression for subraction.
        //Invoke the method difference.
        System.out.println(String.format
        		( "Difference of %d and %d is %d",
                number1,
                number2,
                result));
    }
}
*/

package com.kpriet.training.java.lambda;

interface Subtractor {
    public int difference(int number1, int number2);
}

public class NumberDifference {

    public static void main(String[] args) {
        int number1 = 5;
        int number2 = 10;
        Subtractor subtract = (num1, num2) -> number1 - number2;
        int result = subtract.difference(number1, number2);
        System.out.println(String.format
        		( "Difference of %d and %d is %d",
                number1,
                number2,
                result));
    }

}