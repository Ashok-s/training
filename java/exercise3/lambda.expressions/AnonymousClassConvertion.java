/*
Requirement:
TO Convert the following anonymous class into lambda expression.

interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}

Entity:
	public class AnonymousClassConvertion
	interface CheckNumber
	
Function Declaration:
	public boolean isEven(int value);
	public static void main(String[] args);

Jobs to be done:
    1.Change the anonymous class definition into a single line statement of lambda
expression.
*/

package com.kpriet.training.java.lambda;

interface CheckNumber {
    public boolean isEven(int value);
}

public class AnonymousClassConvertion {
    public static void main(String[] args) {
        CheckNumber number = value -> value % 2 == 0 ? true : false;
        System.out.println(number.isEven(11));
    }
}