/*
Requirement:
	To write a program to print the volume of a Rectangular box using lambda expression
	
Entity:
	class RectangularBox
	interface Box

Function Declaration:
	public static void main(String[] args);
	public double volume();

Jobs to be done:
	1.Get the length, width, and height for the rectangular box
	2.Multiply these values by invoking the method volume. 
	3.Print the volume of the box.
	
Pseudo code:
interface Box {
    public double volume();
}

class RectangularBox {
    
    
    private double length;
    private double width;
    private double height;
    
    //lambda expression to calculate the volume of the box.
    
    RectangularBox(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
    }
    
    public static void main(String[] args) {
        RectangularBox box = new RectangularBox(50.5, 35.2, 20);
        //Invoke the method volume.
        System.out.println("Volume of the box is " + volume +" cm�");
    }
}

*/
package com.kpriet.training.java.lambda;

interface Box {
    public double volume();
}

class RectangularBox {
	
    private double length;
    private double width;
    private double height;
    
    Box boxVolume = () -> this.length * this.width * this.height;
    
    RectangularBox(double length, double width, double height) {
        this.length = length;
        this.width = width;
        this.height = height;
        System.out.println(String.format("Box created with %.2f cm length, %.2f cm width, %.2f cm height", this.length, this.width, this.height));
    }
    
    public static void main(String[] args) {
        RectangularBox box = new RectangularBox(50.5, 35.2, 20);
        double volume = box.boxVolume.volume();
        System.out.println("Volume of the box is " + volume +" cm�");
    }

}