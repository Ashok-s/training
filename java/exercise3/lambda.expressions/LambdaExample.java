/*
 * Requirement:
 *     Addition,Substraction,Multiplication and Division concepts are achieved using Lambda expression
 *     and functional interface
 *
 * Entity:
 *     LambdaExample
 *     Arithmetic
 *
 * Function declartion:
 *     int operation(int number1, int number2);
 
 * Jobs to done:
 *     1.Create a interface Arithmetic and method operate.
 *     2.Using lambda expression do arithmetic operations and print the values
 *
 *Pseudo code:
 *     interface Arithmetic {
 *         int operate(int number1, int number2);
 *     }
 *
 *     class LambdaExample {
 *
 *         public static void main(String[] args) {
 *             Arithmetic addition = (int number1, int number2) -> (number1 + number2);
 *             System.out.println("Addition = " + addition.operate(5, 6));
 *
 *             Arithmetic subtraction = (int number1, int number2) -> (number1 - number2);
 *             System.out.println("Subtraction = " + subtraction.operate(5, 3));
 *
 *             Arithmetic multiplication = (int number1, int number2) -> (number1 * number2);
 *             System.out.println("Multiplication = " + multiplication.operate(4, 6));
 *
 *             Arithmetic division = (int number1, int number2) -> (number1 / number2);
 *             System.out.println("Division = " + division.operate(12, 6));
 *         }
 *     }
 */

package com.kpriet.training.java.lambda;

interface Arithmetic {
	int operate(int number1, int number2);
}


public class LambdaExample {
    
	public static void main(String[] args) {
		Arithmetic addition = (int number1, int number2) -> (number1 + number2);
		System.out.println("Addition = " + addition.operate(5, 6));

		Arithmetic subtraction = (int number1, int number2) -> (number1 - number2);
		System.out.println("Subtraction = " + subtraction.operate(5, 3));

		Arithmetic multiplication = (int number1, int number2) -> (number1 * number2);
		System.out.println("Multiplication = " + multiplication.operate(4, 6));

		Arithmetic division = (int number1, int number2) -> (number1 / number2);
		System.out.println("Division = " + division.operate(12, 6));
	}
}
