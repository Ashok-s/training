package com.kpriet.training.java.lambda;
/*
Requirement:
To determine the valid lambda expression.
which one of these is a valid lambda expression? and why?:
        (int x, int y) -> x+y; or (x, y) -> x + y;
        
Entity:
	None

Function Declaration:
	None

Jobs to be done:
	1) To explain the correct syntax of lambda expression.
*/

/*
-----------------------------------------------------------------
Answer:

(int x, int y) -> x+y;
(or)
(x, y) -> x + y;

here, both the statements are correct but in the lambda expression there is no
need to specify access specifiers.
-----------------------------------------------------------------
*/