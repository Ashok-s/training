/*
Requirement:
To Code a functional program that can return the sum of elements of varArgs, passed
into the method of functional interface.

Entity:
	class Add
	interface Addable

Function Declaration:
	public int add(int ... varArgs);
	public static void main(String[] args);

Jobs to be done:
	1.Get the values to be added;
	2.Add these values by invoking the method add.
	3.Print the resultant value.
	
Pseudo code:
interface Addable {
	public int add(int ... varArgs);
}

class Add {
	private int sum;
    //Create lambda expression for adding the values.   
        return sum;
    };

    public static void main(String[] args) {
        Add add = new Add();
        System.out.println(add.addable.add(1,2,3,4,5));
    }
}
*/
package com.kpriet.training.java.lambda;

interface Addable {
    public int add(int ... varArgs);
}

class Add {
	
    private int sum;
    Addable addable = (int ... varArgs) -> {
        for (int varArg : varArgs) {
            sum += varArg;
        }
        return sum;
    };

    public static void main(String[] args) {
        Add add = new Add();
        System.out.println(add.addable.add(1,2,3,4,5));
    }
}