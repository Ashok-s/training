/*
 * Requirement:
 *      To write a program to find the average age of all the Person in the person List
 *      
 * Entity:
 *      AverageAge
 *      
 * Function Declaration:
 *      public static void main(String[] args);
 *      
 * Jobs to be done:
 *      1.Assign the value for the list by calling the method createRoster in the Person class.
 *      2.Get the average age of the persons in the list.
 *      3.Print the average age calculated.
 *     
 * Pseudo code:
 *     class AverageAge {
 *     
 *         public static void main(String[] args) {
 *			   List<Person> persons = Person.createRoster();
 *			   double averageAge = persons.stream().mapToInt(Person::getAge)
 *                                        .average().getAsDouble();
 *		   	   System.out.println("Average age : " + averageAge);
 *		   }
 *     }
 */

package com.kpriet.training.java.stream;

import java.util.List;

public class AverageAge {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		double averageAge = persons.stream()
				                   .mapToInt(Person::getAge)
				                   .average()
				                   .getAsDouble();
		System.out.println("Average age : " + averageAge);
	}
}
