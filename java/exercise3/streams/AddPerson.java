/*
 * Requirement:
 *      List<Person> newRoster = new ArrayList<>();
 *          newRoster.add(
 *              new Person(
 *              "John",
 *              IsoChronology.INSTANCE.date(1980, 6, 20),
 *              Person.Sex.MALE,
 *              "john@example.com"));
 *          newRoster.add(
 *              new Person(
 *              "Jade",
 *              IsoChronology.INSTANCE.date(1990, 7, 15),
 *              Person.Sex.FEMALE, "jade@example.com"));
 *          newRoster.add(
 *              new Person(
 *              "Donald",
 *              IsoChronology.INSTANCE.date(1991, 8, 13),
 *              Person.Sex.MALE, "donald@example.com"));
 *          newRoster.add(
 *              new Person(
 *              "Bob",
 *              IsoChronology.INSTANCE.date(2000, 9, 12),
 *              Person.Sex.MALE, "bob@example.com"));
 *      - Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
 *      - Remove the all the person in the roster list
 *
 * Entity:
 *    AddPerson
 *
 * Function declaration:
 *     public static void main(String[] args);
 *
 * Jobs to be done:
 *    1.Assign the value for the list persons by calling the method createRoster in the person class. 
 *    2.Create a another list newPersons and add elements to it. 
 *    3.Add all elements in the newPersons list to persons list.
 *    4.Print the elements present in the list and size of the persons list.
 *    5.Delete all the elements in the persons list by clear method and print it.
 *
 * Pseudo code:
 *    class AddPerson {
 *    
 *        public static void main(String[] args) {
 *			  List<Person> persons = Person.createRoster();
 *			  List<Person> newPersons = new ArrayList<>();
 *			  newPersons.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE,
 *					                    "john@example.com"));
 *			  newPersons.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15),
 *					             Person.Sex.FEMALE, "jade@example.com"));
 *			  newPersons.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13),
 *					             Person.Sex.MALE, "donald@example.com"));
 *			  newPersons.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE,
 *					                    "bob@example.com"));
 *			  persons.addAll(newPersons);
 *			  Stream<Person> stream = persons.stream();
 *	 		  stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
 *					+ person.gender + " " + person.emailAddress));
 *			  System.out.println("Number of person in the persons list : " + roster.size());
 *			  persons.clear();
 *			  System.out.println("After removing all elements : " + persons);
 *            System.out.println("Number of person in the persons list after deleting all the elements : " + roster.size());
 *		  }
 *    }  
 */

package com.kpriet.training.java.stream;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class AddPerson {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		List<Person> newPersons = new ArrayList<>();
		newPersons.add(new Person("John", 
				                  IsoChronology.INSTANCE.date(1980, 6, 20), 
				                  Person.Sex.MALE, 
				                  "john@example.com"));
		newPersons.add(new Person("Jade", 
								  IsoChronology.INSTANCE.date(1990, 7, 15),	
								  Person.Sex.FEMALE, 
				           		  "jade@example.com"));
		newPersons.add(new Person("Donald", 
								  IsoChronology.INSTANCE.date(1991, 8, 13), 
								  Person.Sex.MALE, 
								  "donald@example.com"));
		newPersons.add(new Person("Bob", 
								  IsoChronology.INSTANCE.date(2000, 9, 12), 
								  Person.Sex.MALE,	
								  "bob@example.com"));
		persons.addAll(newPersons);
		Stream<Person> stream = persons.stream();
		stream.forEach(person -> System.out.println(person.name + " " + 
													person.birthday + " " +
			                                    	person.gender + " " + 
													person.emailAddress));
		System.out.println("Number of person in the persons list : " + persons.size());
		persons.clear();
		System.out.println("After removing all elements : " + persons);
		System.out.println("Number of person in the persons list after deleting all the elements : "
				           + persons.size());
	}

}
