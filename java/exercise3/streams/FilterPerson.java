/*
 * Requirement:
 * 		To filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

	Entity:
		FilterPerson
	
	Function Declaration:
		public static void main(String[] args);
		
	Jobs To Be Done:
		1.Assign the value for the list by calling the method createRoster in the Person class.
		2.Create a list and add the name to the list whose gender is male
		3.Get the size of the list.
		4.Print the first and last name in that list.
		5.Print the random name from the list
		
    Pseudo code:
        class FilterPerson {
        
            public static void main(String[] args) {
		    	List<Person> persons = Person.createRoster();
				ArrayList<String> maleName = new ArrayList<>();
				for (Person person : persons) {
					if (person.getGender() == Person.Sex.MALE) {
						maleName.add(person.getName());
					}
				}
				int size = maleName.size();
				System.out.println("The First Filtered Person is " + maleName.get(0));
				System.out.println("The Last Filtered Person is " + maleName.get(size - 1));
				Random random = new Random();
				int index = random.nextInt(size);
				System.out.println("The Random Filtered Person is " + maleName.get(index));
			}
        }		
 */

package com.kpriet.training.java.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FilterPerson {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		ArrayList<String> maleName = new ArrayList<>();
		for (Person person : persons) {
			if (person.getGender() == Person.Sex.MALE) {
				maleName.add(person.getName());
			}
		}
		int size = maleName.size();
		System.out.println("The First Filtered Person is " + maleName.get(0));
		System.out.println("The Last Filtered Person is " + maleName.get(size - 1));
		Random random = new Random();
		int index = random.nextInt(size);
		System.out.println("The Random Filtered Person is " + maleName.get(index));
	}
}
