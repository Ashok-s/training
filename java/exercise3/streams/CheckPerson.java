/*Requirement:
 *     Consider the following Person:
 *          new Person(
 *              "Bob",
 *              IsoChronology.INSTANCE.date(2000, 9, 12),
 *              Person.Sex.MALE, "bob@example.com"));
 *     To check if the above person is in the roster list obtained from Person class.
 * 
 * Entity:
 * 		CheckPerson
 * 
 * Function Declaration:
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Assign the value for the list by calling the method createRoster in the Person class.
 *      2.Get the person who has been searched for in the list.
 *      3.Search the person in the list 
 *          3.1)if the person found in the list set present equal to true and print person is present
 *          3.1)Else print person is not present.
 *      
 * Pseudo code:
 *     class CheckPerson {
 *     
 *         public static void main(String[] args) {
 *			   List<Person> persons = Person.createRoster();
 *			   Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
 *					                   Person.Sex.MALE, "bob@example.com");
 *			   Stream<Person> stream = persons.stream();
 *			   stream.forEach(person -> {
 *				   if ((person.getName().equals(newPerson.getName()))
 *						&& (person.getBirthday().equals(newPerson.getBirthday()))
 *						&& (person.getEmailAddress().equals(newPerson.getEmailAddress()))
 *						&& (person.getGender().equals(newPerson.getGender()))) {
 *					   present = true;
 *				   }
 *			   });
 *			   if (present == true) {
 *				   System.out.println("Person is present");
 *			   } else {
 *				   System.out.println("Person is not present");
 *			   }
 *		   }
 *     }
 *     
 *     
 */

package com.kpriet.training.java.stream;

import java.time.chrono.IsoChronology;
import java.util.List;
import java.util.stream.Stream;

public class CheckPerson {

	public static boolean present = false;

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		Person newPerson = new Person("Bob", 
				                      IsoChronology.INSTANCE.date(2000, 9, 12),
				                      Person.Sex.MALE, 
				                      "bob@example.com");
		Stream<Person> stream = persons.stream();
		stream.forEach(person -> {
			if ((person.getName().equals(newPerson.getName()))
	         && (person.getBirthday().equals(newPerson.getBirthday()))
			 && (person.getEmailAddress().equals(newPerson.getEmailAddress()))
			 && (person.getGender().equals(newPerson.getGender()))) {
				present = true;
			}
		});
		if (present == true) {
			System.out.println("Person is present");
		} else {
			System.out.println("Person is not present");
		}

	}
}
