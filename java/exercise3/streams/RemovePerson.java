/*
 * Requirements:
 *    List<Person> newRoster = new ArrayList<>();
 *           newRoster.add(
 *               new Person(
 *               "John",
 *               IsoChronology.INSTANCE.date(1980, 6, 20),
 *               Person.Sex.MALE,
 *               "john@example.com"));
 *           newRoster.add(
 *               new Person(
 *               "Jade",
 *               IsoChronology.INSTANCE.date(1990, 7, 15),
 *               Person.Sex.FEMALE, "jade@example.com"));
 *           newRoster.add(
 *               new Person(
 *               "Donald",
 *               IsoChronology.INSTANCE.date(1991, 8, 13),
 *               Person.Sex.MALE, "donald@example.com"));
 *           newRoster.add(
 *               new Person(
 *               "Bob",
 *               IsoChronology.INSTANCE.date(2000, 9, 12),
 *               Person.Sex.MALE, "bob@example.com"));
 * Remove the following person from the roster List:
 *           new Person(
 *               "Bob",
 *               IsoChronology.INSTANCE.date(2000, 9, 12),
 *               Person.Sex.MALE, "bob@example.com"));
 * 
 * Entities:
 *    RemovePerson
 *    
 * Function Declaration:
 *    public static void main(String[] args); 
 *    
 * Jobs To Be Done:
 *    1.Assign the value for the list persons by calling the method createRoster in the person class. 
 *    2.Create a new list newPersons and add the elements to it and print them.
 *    3.Remove the person Bob from the list persons and print the remaining persons.
 *
 * Pseudo code:
 *    class RemovePerson {
 *    
 *        public static void main(String[] args) {
 *            List<Person> persons = Person.createRoster();
 *		      List<Person> newPersons = new ArrayList<>();
 *			  newPersons.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE,
 *				    	"john@example.com"));
 *			  newPersons.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15),
 *					Person.Sex.FEMALE, "jade@example.com"));
 *			  newPersons.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13),
 *					Person.Sex.MALE, "donald@example.com"));
 *			  newPersons.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE,
 *				"bob@example.com"));
 *			  Stream<Person> stream = newPersons.stream();
 *			  stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
 *					+ person.gender + " " + person.emailAddress));
 *			  persons = persons.stream().filter(element -> element.getName() != "Bob")
 *					.collect(Collectors.toList());
 *			  Stream<Person> stream1 = persons.stream();
 *			  System.out.println("After removing Bob from roster list");
 *			  stream1.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
 *					+ person.gender + " " + person.emailAddress));  
 *        }
 *    }
 */

package com.kpriet.training.java.stream;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RemovePerson {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		List<Person> newPersons = new ArrayList<>();
		newPersons.add(new Person("John", 
								  IsoChronology.INSTANCE.date(1980, 6, 20), 
				                  Person.Sex.MALE, 
				                  "john@example.com"));				
		newPersons.add(new Person("Jade", 
				                   IsoChronology.INSTANCE.date(1990, 7, 15),
			           	           Person.Sex.FEMALE,  
			           	           "jade@example.com"));
		newPersons.add(new Person("Donald", 
				                   IsoChronology.INSTANCE.date(1991, 8, 13),
						           Person.Sex.MALE,  
						           "donald@example.com"));
		newPersons.add(new Person("Bob", 
				                  IsoChronology.INSTANCE.date(2000, 9, 12), 
						          Person.Sex.MALE, 
						          "bob@example.com"));
		Stream<Person> stream = newPersons.stream();
		stream.forEach(person -> System.out.println(person.name + " " + 
		                                            person.birthday + " " +
				                                    person.gender + " " + 
		                                            person.emailAddress));
		persons = persons.stream()
				         .filter(element -> element.getName() != "Bob")
				         .collect(Collectors.toList());
		Stream<Person> stream1 = persons.stream();
		System.out.println("After removing Bob from persons list");
		stream1.forEach(person -> System.out.println(person.name + " " + 
		                                             person.birthday + " " +
				                                     person.gender + " " + 
		                                             person.emailAddress));
	}
}
