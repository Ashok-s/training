/*
 * Requirement:
 *      TO Sort the roster list based on the person's age in descending order using java.util.Stream
 *      
 * Entity:
 *      SortStream
 *      
 * Function declaration:
 *      public static void main(String[] args);
 * 
 * Jobs to be done:
 *      1.Assign the value for the list by calling the method createRoster in the Person class.
 *      2.Sort the person by age in and store it in descending order
 *      3.Print the list in the ordered way.
 *      
 * Pseudo code:
 *     class SortStream {
 *     
 *         public static void main(String[] args) {
 *		   	  List<Person> persons = Person.createRoster();
 *			  Stream<Person> stream = (persons.stream().sorted(Comparator.comparing(Person::getAge))
 *					                          .collect(Collectors.toList())).stream();
 *			  stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
 *					+ person.gender + " " + person.emailAddress));
 *		  }
 *     }
 */

package com.kpriet.training.java.stream;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortStream {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		Stream<Person> stream = (persons.stream()
				                        .sorted(Comparator
				                        .comparing(Person::getAge))
				                        .collect(Collectors.toList()))
				                        .stream();
		stream.forEach(person -> System.out.println(person.name + " " + 
											        person.birthday + " " +
											        person.gender + " " + 
											        person.emailAddress));
	}
}
