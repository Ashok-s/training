/*Requirement:
 * 		To Write a program to filter the Person, who are male and age greater than 21
 * 
 * Entity:
 * 		GenderAgePerson
 * 
 * Function Declaration:
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Assign the value for the list by calling the method createRoster in the Person class.
 *      2.For each person in the list 
 *          2.1)Check whether the person is male and.
 *          2.2)Check whether the person's age is greater than 21 then.
 *          2.3)Print the name.
 *          
 * Pseudo code:
 *     class GenderAgePerson {
 *     
 *         public static void main(String[] args) {
 *		       List<Person> persons = Person.createRoster();
 *		       for (Person person : persons) {
 *			       if (person.getGender() == Person.Sex.MALE) {
 *				       if (person.getAge() > 21) {
 *					       System.out.println(person.getName());
 *				       }
 *			       }
 *		       }
 *	       }    
 *     }          
 */      

package com.kpriet.training.java.stream;

import java.util.List;

public class GenderAgePerson {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		for (Person person : persons) {
			if (person.getGender() == Person.Sex.MALE) {
				if (person.getAge() > 21) {
					System.out.println(person.getName());
				}
			}

		}
	}
}
