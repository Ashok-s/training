/*
 * Requirement:
 * 		To print minimal person with name and email address from the Person class using java.util.Stream<T>#map API by referring Person.java
 * 
 * Entity:
 * 		MinimalPersonMap
 *
 * Function Declaration:
 *		public static 
 *void main(String[] args);
 *
 * Jobs To Be Done:
 * 		1.Assign the value for the list by calling the method createRoster in the Person class.
 *      2.Create two list which contains person's names and their mailid's separately
 *      3.Find the name and mailid which is minimum in length.
 *      4.Print the name and mailid which was founded already.
 *      
 * Pseudo code:
 *     class MinimalPersonMap {
 *     
 *         public static void main(String[] args) {
 *			   List<Person> persons = Person.createRoster();
 *			   ArrayList<String> names = (ArrayList<String>) persons.stream()
 * 										 .map(string -> string.getName())
 *										 .collect(Collectors.toList());
 *			   ArrayList<String> mailIds = (ArrayList<String>) persons.stream()
 *										   .map(string -> string.getEmailAddress())
 *					                       .collect(Collectors.toList());
 * 			   String minimalName = Collections.min(names);
 *			   String minimalId = Collections.min(mailIds);
 *	
 *			   System.out.println("The Minimal Person Name is " + minimalName + 
 *                                " And EmailId is " + minimalId);
 *		   }
 *     }      
 * 
 */

package com.kpriet.training.java.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MinimalPersonMap {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		ArrayList<String> names = (ArrayList<String>) persons.stream()
				                                             .map(string -> string.getName())
				                                             .collect(Collectors.toList());
		ArrayList<String> mailIds = (ArrayList<String>) persons.stream()
				                                               .map(string -> string.getEmailAddress())
				                                               .collect(Collectors.toList());
		String minimalName = Collections.min(names);
		String minimalId = Collections.min(mailIds);

		System.out.println("The Minimal Person Name is " + minimalName + 
				           " And EmailId is " + minimalId);
	}
}
