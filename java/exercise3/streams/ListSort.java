/*
 * Requirement:
 *      TO sort the roster list based on the person's age in descending order using comparator
 *      
 * Entity:
 *      ListSort
 *
 * Function declaration:
 *      public static void main(String[] args);
 *      
 * Jobs to be done:
 *      1.Create a list by calling the method createRoster in the Person class.
 *      2.Sort the list with the age of person
 *      3.Print the list in the descending order.
 *      
 * Pseudo code:
 *     class ListSort {
 *     
 *         public static void main(String[] args) {
 *			   List<Person> roster = Person.createRoster();
 *			   roster.sort(Comparator.comparing(Person::getAge));
 *			   Stream<Person> stream = roster.stream();
 *			   stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
 *					+ person.gender + " " + person.emailAddress));
 *		   }
 *     }       
 */


package com.kpriet.training.java.stream;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class ListSort {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		roster.sort(Comparator.comparing(Person::getAge));
		Stream<Person> stream = roster.stream();
		stream.forEach(person -> System.out.println(person.name + " " + 
		                                            person.birthday + " " +
			                                        person.gender + " " + 
		                                            person.emailAddress));
	}
}
