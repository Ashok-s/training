/*
 * Requirement:
 * 		To print minimal person with name and email address from the Person class using java.util.Stream<T> API by referring Person.java
 * 
 * Entity:
 * 		MinimalPersonMap
 *
 * Function Declaration:
 *		public static void main(String[] args);
 *
 * Jobs To Be Done:
 * 		1.Assign the value for the list by calling the method createRoster in the Person class.
 *      2.Create two separate list for storing names and mailids and add respective values
 *      3.Find and print the minimum value of name and mail id.
 *      
 * Pseudo code:
 *     class MinimalPersonList {
 *     
 *         List<Person> persons = Person.createRoster();
 *			   ArrayList<String> names = new ArrayList<>();
 *			   ArrayList<String> mailIds = new ArrayList<>();
 *			   for (Person person : persons) {
 *			       names.add(person.getName());
 *				   mailIds.add(person.getEmailAddress());
 *				}
 *				String minimalName = Collections.min(names);
 *				String minimalId = Collections.min(mailIds);
 *				System.out.println("The Minimal Person Name is \" " + minimalName + 
 *                                 " \" And EmailId is \" " + minimalId + " \"");
 *			}
 *     }      
 * 
 */

package com.kpriet.training.java.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimalPersonList {
	
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<String> mailIds = new ArrayList<>();
		for (Person person : persons) {
			names.add(person.getName());
			mailIds.add(person.getEmailAddress());
		}
		String minimalName = Collections.min(names);
		String minimalId = Collections.min(mailIds);
		System.out.println("The Minimal Person Name is \" " + minimalName + 
				           " \" And EmailId is \" " + minimalId + " \"");
	}

}
