/*
 * Requirement:
 *    Consider a following code snippet:
          List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
          - Find the sum of all the numbers in the list using java.util.Stream API
          - Find the maximum of all the numbers in the list using java.util.Stream API
          - Find the minimum of all the numbers in the list using java.util.Stream API 
          
 * Entities:
 *    StreamDemo
 * 
 * Function Declaration:
 *    public static void main(String[] args);
 *    
 * Jobs To Be Done:
 *    1.Create a list having integer generic type
 *    2.Sum the elements present in the list using and print the sum
 *    3.Find the maximum and minimum number in the list and print that numbers
 *    
 * Pseudo code:
 *    class StreamDemo {
 *    
 *        public static void main(String[] args) {
 *			  List<Integer> numbers = Arrays.asList(1, 6, 10, 25, 78);
 *			  System.out.println("List elements are " + numbers);
 *			  int sum = numbers.stream().mapToInt(Integer::valueOf).sum();
 *			  System.out.println("Sum of the list elements " + sum);
 *			  int maxNumber = numbers.stream().max(Integer::compare).get();
 *			  System.out.println("Maximum element in the list " + maxNumber);
 *			  int minNumber = numbers.stream().min(Integer::compare).get();
 *			  System.out.println("Minimum element in the list " + minNumber);
 *        }
 *    }      
 */

package com.kpriet.training.java.stream;

import java.util.Arrays;
import java.util.List;

public class StreamDemo {
	
	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(1, 6, 10, 25, 78);
		System.out.println("List elements are " + numbers);
		int sum = numbers.stream()
				         .mapToInt(Integer::valueOf)
				         .sum();
		System.out.println("Sum of the list elements " + sum);
		int maxNumber = numbers.stream()
				               .max(Integer::compare)
				               .get();
		System.out.println("Maximum element in the list " + maxNumber);
		int minNumber = numbers.stream()
				               .min(Integer::compare)
				               .get();
		System.out.println("Minimum element in the list " + minNumber);
	}
}