/*
 * Requirement:
 *      To Print all the persons in the roster using java.util.Stream<T>#forEach
 *       
 * Entity:
 *      PrintPerson
 *      
 * Function Declaration:
 *      public static void main(String[] args);
 *      
 * Jobs to be done:
 *      1.Assign the value for the list by calling the method createRoster in the Person class. 
 *      2.Create a stream for the list with the method stream().
 *      3.Print the elements in the list using stream.forEach method.
 *      
 * Pseudo code:
 *     class PrintPerson {
 *         
 *	       public static void main(String[] args) {
 *		       List<Person> persons = Person.createRoster();
 *			   Stream<Person> stream = persons.stream();
 *			   stream.forEach(person -> System.out.println(person.name + " " + 
 *                            person.birthday + " " + 
 *                            person.gender + " " + person.emailAddress));
 *		   }
 *     }      
 */

package com.kpriet.training.java.stream;

import java.util.List;
import java.util.stream.Stream;

public class PrintPerson {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		Stream<Person> stream = persons.stream();
		stream.forEach(person -> System.out.println(person.name + " " + 
		                                            person.birthday + " "+ 
				                                    person.gender + " " + 
				                                    person.emailAddress));
	}
}
