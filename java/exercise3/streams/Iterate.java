/*
 * Requirement:
 *      Iterate the roster list in Persons class and and print the person without using forLoop/Stream
 *         
 * Entity:
 *      Iterate
 *      
 * Function Declaration:
 *      public static void main(String[] args);
 *      
 * Jobs to be done:
 *      1.Assign the value for the list by calling the method createRoster in the Person class.
 *      2.For each element in the list print the elements from the list.
 *     
 * Pseudo code:
 *     class Iterate {
 *     
 *         public static void main(String[] args) {
 *			   List<Person> persons = Person.createRoster();
 *			   Iterator<Person> iterator = persons.iterator();
 *			   while (iterator.hasNext()) {
 *				   Person person = iterator.next();
 *				   System.out.println(person.name + " " + person.birthday + " " + person.gender + " " +
 *						              person.emailAddress);
 *			   }
 *	       }
 *     }
 */

package com.kpriet.training.java.stream;

import java.util.Iterator;
import java.util.List;

public class Iterate {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		Iterator<Person> iterator = persons.iterator();
		while (iterator.hasNext()) {
			Person person = iterator.next();
			System.out.println(person.name + " " + 
			                   person.birthday + " " + 
					           person.gender + " " + 
			                   person.emailAddress);
		}
	}
}
