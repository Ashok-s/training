/*
 * Requirement:
 *     Consider a following code snippet:
 *     List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
 *     To Get the non-duplicate values from the above list using java.util.Stream API
 * 
 * Entity:
 *     NonDulicateList
 * 
 * Function Declaration:
 *     public static void main(String[] args);
 * 
 * Jobs to be done:
 *    1.Create a list which contains duplicate elements in it.
 *    2.Make a list from the above list by adding elements which are not repeated in that list.
 *    3.For each element in the list print the element.
 *    
 * Pseudo code:
 *    class NonDuplicateList {
 *    
 *        public static void main(String[] args) {
 *			  List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
 *			  List<Integer> withoutDuplicate = randomNumbers.stream()
 *				  	.filter(element -> Collections.frequency(randomNumbers, element) == 1)
 *					.collect(Collectors.toList());
 *			  Stream<Integer> stream = withoutDuplicate.stream();
 *			  stream.forEach(elements -> System.out.print(elements + " "));
 *		  }
 *    }    
 */

package com.kpriet.training.java.stream;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NonDuplicateList {

	public static void main(String[] args) {
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
		List<Integer> withoutDuplicate = randomNumbers.stream()
				                                      .filter(element -> Collections
				                                      .frequency(randomNumbers, element) == 1)
			            	                          .collect(Collectors.toList());
		Stream<Integer> stream = withoutDuplicate.stream();
		stream.forEach(elements -> System.out.print(elements + " "));
	}
}
