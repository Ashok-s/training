/*
 * Requirements:
 *     To Demonstrate the catching multiple exceptions.
 * 
 * Entities:
 *     ExceptionDemo
 * 
 * Function Declaration:
 *     public static void main(String[] args);
 * 
 * Jobs To Be Done:
 *     1.Get the two numbers from user
 *     2.Divide the numbers and print the value.
 *     3.Assign null value to a variable and print the variable
 *     4.Catch the exceptions occurred.
 * 
 * Pseudo code:
 *     class ExceptionDemo {
 *     
 *         public static void main(String[] args) {
 *			   try {
 *				   Scanner scanner = new Scanner(System.in);
 *				   System.out.println("enter numbers");
 *				   int number1 = scanner.nextInt();
 *				   int number2 = scanner.nextInt();
 *				   scanner.close();
 *				   int number = number1 / number2;
 *				   System.out.println(number);
 *				   String string = null;
 *				   System.out.println(string.length());
 *			   } catch (ArithmeticException exception) {
 *				   System.out.println("Arithmetic Exception occurs");
 *			   } catch (NullPointerException exception) {
 *				   System.out.println("Null Pointer exception occurs");
 *			   }
 *		   }
 *     }
 */

package com.kpriet.training.java.exception;

import java.util.Scanner;

public class ExceptionDemo {
	
	public static void main(String[] args) {
		try {
			Scanner scanner = new Scanner(System.in);
			System.out.println("enter numbers");
			int number1 = scanner.nextInt();
			int number2 = scanner.nextInt();
			scanner.close();
			int number = number1 / number2;
			System.out.println(number);
			String string = null;
			System.out.println(string.length());
		} catch (ArithmeticException exception) {
			System.out.println("Arithmetic Exception occurs");
		} catch (NullPointerException exception) {
			System.out.println("Null Pointer exception occurs");
		}
	}
}
