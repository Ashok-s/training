/* 
Requirement:
    To tell that the code given is valid or not.
     check if user already exists
              validate user
              validate address
              insert user
              insert address
    

Entity:
    There is no Entity used here.

Function Declaration:
    There is no function is declared here.

Jobs to be done:
    To answer the given questions.

Answer:
    The given code snippet is wrong because if the address is wrong or there is no address
    we cannot insert the address so the correct snippet is given below.
    
    check if user already exists
              validate user
              insert user
              validate address
              insert address
*/
        