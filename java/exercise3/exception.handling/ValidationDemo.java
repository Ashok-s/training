/*
Requirement:
    To demonstrate the importance of Validation.

Entity:
    public class ValidationDemo.

Function Declaration:
    public static void main(String[] args);

Jobs to be done:
    1.Set the variable to false 
    2.Get the integer value from the user 
    3.If the entered value is not integer 
        3.1)catch the exception if occurred.
        3.2)otherwise change the variable to true and
        3.3)Print the entered number.
        
Pseudo code:
class ValidationDemo {

	public static void main(String[] args) {
        int number = 0;
        String string;
        boolean valid = false;
   
        Scanner scanner = new Scanner(System.in);
        while (valid == false) {
            System.out.print("Enter an integer value ");
            string = scanner.nextLine();
            //try catch block
        }
        
        System.out.println("the number you entered is " + number);
        scanner.close();
    }
}
*/

package com.kpriet.training.java.exception;

import java.util.Scanner;

public class ValidationDemo {
    
    public static void main(String[] args) {
        int number = 0;
        String string;
        boolean valid = false;
   
        Scanner scanner = new Scanner(System.in);
        while (valid == false) {
            System.out.print("Enter an integer value ");
            string = scanner.nextLine();
            try {
                number = Integer.parseInt(string);
                valid = true;
            } catch (NumberFormatException e) {
                System.out.println("oops!! Enter an integer value");
            }
        }
        
        System.out.println("the number you entered is " + number);
        scanner.close();
    }
}
