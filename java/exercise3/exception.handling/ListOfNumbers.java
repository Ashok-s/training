/*
 * Requirements:
 * 	   To write a program ListOfNumbers using try and catch block.
 * 
 * Entities:
 *     public class ListOfNumbers 
 * 
 * Function Declaration:
 *     public static void main(String[] args);
 * 
 * Jobs To Be Done:
 *     1.Create an array of length 10.
 *     2.Assign a value to the out of index in the array.
 *     3.catch the exception occurred.
 *     
 * Pseudo code:
 *     class ListOfNumbers {
 *     
 *     	   public static void main(String[] args) {
 *
 *			   int[] numbers = new int[10];
 *		  	   try {
 *				   numbers[10] = 16;
 *			   } catch (NumberFormatException exception) {
 *				   System.out.println("Number Format Exception " + exception.getMessage());
 *			   } catch (IndexOutOfBoundsException exception) {
 *				   System.out.println("Index Out Of Bounds Exception " + exception.getMessage());
 *			   } 
 *
 *	       }
 *     }
 */

package com.kpriet.training.java.exception;

public class ListOfNumbers {

	public static void main(String[] args) {

		int[] numbers = new int[10];
		try {
			numbers[10] = 16;
		} catch (NumberFormatException exception) {
			System.out.println("Number Format Exception " + exception.getMessage());
		} catch (IndexOutOfBoundsException exception) {
			System.out.println("Index Out Of Bounds Exception " + exception.getMessage());
		}

	}

}
