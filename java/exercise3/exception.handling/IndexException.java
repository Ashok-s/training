/*
Requirement:
    To handle the given program.
       public class Exception {  
           public static void main(String[] args) {
               int arr[] ={1,2,3,4,5};
               System.out.println(arr[7]);
           }
       }
       
Entity:
    public class IndexException

Function Declaration:
    public static void main(String[] args);

Jobs to be done:
    1.Get the array 
    2.Print the value of an array which is out of index
    3.Catch the exception occurred.
    
Pseudo code:
class IndexException {

	public static void main(String[] args) {
        try {
            int[] array = {1,2,3,4,5,6};
            System.out.println(array[6]);
        } catch (ArrayIndexOutOfBoundsException e1) {
            System.out.println("Not in the index level");
        } catch (Exception  e2) {
            System.out.println("exception occurs");
        }
    }
}
 */

package com.kpriet.training.java.exception;

public class IndexException {
    
    public static void main(String[] args) {
        try {
            int[] array = {1,2,3,4,5,6};
            System.out.println(array[6]);
        } catch (ArrayIndexOutOfBoundsException e1) {
            System.out.println("Not in the index level");
        } catch (Exception  e2) {
            System.out.println("exception occurs");
        }
    }
}
            
        