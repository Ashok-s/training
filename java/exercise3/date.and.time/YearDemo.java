/*
Requirement:
    To write a Java program to get a date before and after 1 year compares to the current date. .
      
Entity:
    YearDemo
    
Function Declaration:
    public static void main(String[] args);
    
Jobs to be done:
    1. Create instance for Calendar.
    2. Print the date 1 year before the current date and after the current date.
    
Pseudo code:
class YearDemo {
    
    public static void main(String[] args) {
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("The day 1 year after the current day is: " + " " + calender.getTime());
    }
}
*/

package com.kpriet.java.training.date_and_time;

import java.util.Calendar;

public class YearDemo {
    
    public static void main(String[] args) {
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, -1);
        System.out.println("Date before 1 year is: " + " " + calender.getTime());
        
        calender.add(Calendar.YEAR, 2);
        System.out.println("The day 1 year after the current day is: " + " " + calender.getTime());
    }
}
