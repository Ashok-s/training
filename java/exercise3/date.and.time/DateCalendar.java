/*
Requirement:
    To write a Java program to get the dates 10 days before and after today.
      1. Using Local date
      2. Using calendar
      
Entity:
    DateCalendar
    
Function Declaration:
    public static void main(String[] args);
    
Jobs to be done:
    1. Get the today's date and store it in today.
    2. Print the today's date.
    3. Print the date 10 days before and after today.
    4. Create instance for Calendar 
    5. Print the date 10 days before and after the current date.
    
Pseudo code:
class DateCalendar {
    
    public static void main(String[] args) {
        // using local date
        LocalDate today = LocalDate.now();
        System.out.println("The current date is: " + " " + today);
        System.out.println("The day 10 days before the current day is: " + " " + today.plusDays(-10));
        System.out.println("The day 10 days after the current day is: " + " " + today.plusDays(10));
        
        // using calendar
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, 10);
        System.out.println("The day 10 days after the current day is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, -20);
        System.out.println("The day 10 days before the current day is: " + " " + calender.getTime());
        
    }

}
*/

package com.kpriet.java.training.date_and_time;

import java.time.LocalDate;
import java.util.Calendar;

public class DateCalendar {
    
    public static void main(String[] args) {
        // using local date
        LocalDate today = LocalDate.now();
        System.out.println("The current date is: " + " " + today);
        System.out.println("The day 10 days before the current day is: " + " " + today.plusDays(-10));
        System.out.println("The day 10 days after the current day is: " + " " + today.plusDays(10));
        
        // using calendar
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, 10);
        System.out.println("The day 10 days after the current day is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, -20);
        System.out.println("The day 10 days before the current day is: " + " " + calender.getTime());
        
    }
}
