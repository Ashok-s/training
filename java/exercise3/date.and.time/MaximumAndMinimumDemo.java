/*
Requirement:
    To find maximum and minimum value of the year, Month, week and day of a default calendar.
    
Entity:
    MaximumAndMinimumDemo
    
Function Declaration:
    public static void main(String[] args); 
    
Jobs to be done:
    1. Create an instance of a Calendar as calendar that gets the calendar using current time zone
    2. Print the maximum and minimum values of the year, month, week and day.
    
 Pseudo code:
 class MaximumAndMinimumDemo {
     
     public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        int maximumYear = calendar.getActualMaximum(Calendar.YEAR);
        System.out.println("The maximum year of default calendar is: " + " " + maximumYear);
        
        int minimumYear = calendar.getActualMinimum(Calendar.YEAR);
        System.out.println("The minimum year of the default calendar is: " + " " + minimumYear);
        
        int maximumMonth = calendar.getActualMaximum(Calendar.MONTH);
        System.out.println("The maximum month of default calendar is: " + " " + maximumMonth);
        
        int minimumMonth = calendar.getActualMinimum(Calendar.MONTH);
        System.out.println("The minimum month of the default calendar is: " + " " + minimumMonth);
        
        int maximumWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        System.out.println("The maximum week of default calendar is: " + " " + maximumWeek);
        
        int minimumWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
        System.out.println("The minimum week of the default calendar is: " + " " + minimumWeek);
        
        int maximumDay = calendar.getActualMaximum(Calendar.DATE);
        System.out.println("The maximum day of default calendar is: " + " " + maximumDay);
        
        int minimumDay = calendar.getActualMinimum(Calendar.DATE);
        System.out.println("The minimum day of the default calendar is: " + " " + minimumDay);
    }
}
*/

package com.kpriet.java.training.date_and_time;

import java.util.Calendar;

public class MaximumAndMinimumDemo {
    
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calendar.getTime());
        
        int maximumYear = calendar.getActualMaximum(Calendar.YEAR);
        System.out.println("The maximum year of default calendar is: " + " " + maximumYear);
        
        int minimumYear = calendar.getActualMinimum(Calendar.YEAR);
        System.out.println("The minimum year of the default calendar is: " + " " + minimumYear);
        
        int maximumMonth = calendar.getActualMaximum(Calendar.MONTH);
        System.out.println("The maximum month of default calendar is: " + " " + maximumMonth);
        
        int minimumMonth = calendar.getActualMinimum(Calendar.MONTH);
        System.out.println("The minimum month of the default calendar is: " + " " + minimumMonth);
        
        int maximumWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
        System.out.println("The maximum week of default calendar is: " + " " + maximumWeek);
        
        int minimumWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
        System.out.println("The minimum week of the default calendar is: " + " " + minimumWeek);
        
        int maximumDay = calendar.getActualMaximum(Calendar.DATE);
        System.out.println("The maximum day of default calendar is: " + " " + maximumDay);
        
        int minimumDay = calendar.getActualMinimum(Calendar.DATE);
        System.out.println("The minimum day of the default calendar is: " + " " + minimumDay);
    }
}
