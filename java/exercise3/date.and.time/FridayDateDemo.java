/*
Requirement:
    To find the date of the previous Friday with the given random date.
    
Entity:
    FridayDateDemo
     
Function Declaration:
    public static void main(String[] args);
     
Jobs to be done:
    1. Create reference for LocalDate and store the date
    2. Print the date of the previous friday by taking current date as input.
    
Pseudo code:
class FridayDateDemo {
    
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println("The previous Friday is: " + localDate.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}
*/

package com.kpriet.java.training.date_and_time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class FridayDateDemo {
    
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println("The previous Friday is: " + localDate.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}
