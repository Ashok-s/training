/*
Requirement:
    To write a Java program to calculate the age 
    
Entity:
    AgeDemo
    
Function declaration:
    public static void man(String[] args);
    
Jobs to be done:
    1. Create two instances of LocalDate as dateOfBirth to store the date of birth and currentDate to store
       current date.
    2. Create an instance for Period and calculate difference between the dateOfBirth and
       currentDate.
    3. Print the years.
    
Pseudo code:
class AgeDemo {
    
    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(2001, 06, 03);
        LocalDate currentDate = LocalDate.now();
        Period period = Period.between(dateOfBirth, currentDate);
        System.out.println("I am " + period.getYears() + " " + "years old");
    }
}
*/

package com.kpriet.java.training.date_and_time;

import java.time.LocalDate;
import java.time.Period;

public class AgeDemo {
    
    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(2001, 06, 03);
        LocalDate currentDate = LocalDate.now();
        Period period = Period.between(dateOfBirth, currentDate);
        System.out.println("I am " + period.getYears() + " " + "years old");
    }
}
