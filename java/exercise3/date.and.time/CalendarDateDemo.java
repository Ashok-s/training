/*
 *  Requirement:
 *   To convert a Calendar to Date and vice-versa with example.
 *  
 * Entity:
 *	 CalendarDateDemo 
 *
 * Function Declaration:
 *	 public static void main(String[] args);
 * 
 * Jobs To Be Done:
 *    1)Create a reference for Calendar class.
 *    2)Convert calendar to date and print the date.
 *    3)Create reference for Date class.
 *    4)Convert date to calendar and print the date.
 *    
 * Pseudo Code:
 * class CalendarToDate {
 *
 *		public static void main(String[] args) {
 *		    Calendar calendar = Calendar.getInstance();
 *			System.out.println(calendar.getTime());
 *			Date date = calendar.getTime();
 *			System.out.println(date);
 *			Date date1 = new Date(45373573);
 *			calendar.setTime(date1);
 *			System.out.println(calendar.getTime());
 *		}
 * }
 */

package com.kpriet.java.training.date_and_time;

import java.util.Date;
import java.util.Calendar;

public class CalendarDateDemo {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.getTime());
		Date date = calendar.getTime();
		System.out.println(date);
		Date date1 = new Date(45373573);
		calendar.setTime(date1);
		System.out.println(calendar.getTime());
    }
}
