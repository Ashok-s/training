/* Requirement:
 *   To write a Java program to convert ZonedDateTime to Date.
 * 
 * Entity:
 *   ZonedDateToDate
 *  
 * Function Declaration:
 *   public static void main(String[] args);
 *  
 * Jobs To Be Done:
 *    1)Create a reference for ZonedDateTime class.
 *    2)Create a reference for Instant class and convert the instant date from zonedDateTime.
 *    3)Create a reference for Date class and convert the date from instant date.
 *    4)Print the date. 
 * 
 * pseudo Code:
 * class ZonedDateToDate {
 *
 *		public static void main(String[] args) {
 *			ZonedDateTime zonedDateTime = ZonedDateTime.now();
 *			System.out.println(zonedDateTime);
 *			
 *			Instant instant = zonedDateTime.toInstant();
 *			System.out.println(instant);
 *			
 *			Date date = Date.from(instant);
 *			System.out.println(date);
 *		} 
 *
 *	} 
 *
 * 
 */

package com.kpriet.java.training.date_and_time;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;

public class ZonedDateToDate {

	public static void main(String[] args) {
		ZonedDateTime zonedDateTime = ZonedDateTime.now();
		System.out.println(zonedDateTime);
		
		Instant instant = zonedDateTime.toInstant();
		System.out.println(instant);
		
		Date date = Date.from(instant);
		System.out.println(date);
	}

}
