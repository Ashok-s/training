/* 
Requirement:
    To code for the schedule() method in Timer class.

Entity:
    ScheduleMethodDemo

Function Declaration:
    public static void main(String[] args);
    public void run();
    
Jobs to be done:
    1. Create reference for Timer and TimerTask
    2. For each time that is from 0 to 10 
           2.1) Print that number and cancel that timer.
           2.2) remove the cancelled timer and print the removed value.
    3. schedule the task whose execution at specified time(1000 ms) for repeated delay for 1 ms.
    
Pseudo code:
public class ScheduleMethodDemo {
    
    public static void main(String[] args) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            
            public void run() {
                
                for (int number = 0; number <= 10; number++ ) {
                    System.out.println( "number : " + number);
                    timer.cancel();
                    System.out.println("stop" + " purge value of Task : " + timer.purge());
                }   
            }
        };
        
        timer.schedule(task, 1000, 1);
    }
}

*/

package com.kpriet.java.training.date_and_time;

import java.util.Timer;
import java.util.TimerTask;

public class ScheduleMethodDemo {
    
    public static void main(String[] args) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            
            public void run() {
                
                for (int number = 0; number <= 10; number++ ) {
                    System.out.println( "number : " + number);
                    timer.cancel();
                    System.out.println("stop" + " purge value of Task : " + timer.purge());
                }   
            }
        };
        
        timer.schedule(task, 1000, 1);
    }
}
