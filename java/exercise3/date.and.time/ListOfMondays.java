/*
Requirement:
    To Write a Java program for a given month of the current year, lists all of the Mondays in that month.
    
Entity:
    ListOfMondays
    
Function Declaration:
    public static void main(String[] args); 
    
Jobs to be done:
    1. Get the month from the user for which list of Mondays to be printed and store it in String
       variable givenMonth.
    2. Create a reference for Month as month that stores the upper case of givenMonth. 
    3. Create reference for LocalDatae as localDate that stores for the current year and for
       given month the date of first Monday in that month.
    4. Create an another instance for Month as newMonth which stores the name of the given month.
    5. Check whether the newMonth and month are equal,
        5.1) get the next Mondays date and print it
        5.1) continue this process until the condition fails. 
           

 Pseudo code:
 class ListOfSaturdays {
     
     public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the name of the month to which list of Mondays to find: ");
        String givenMonth = scanner.nextLine();
        scanner.close();
        Month month = Month.valueOf(givenMonth.toUpperCase());
        LocalDate localDate = Year.now().atMonth(month).atDay(1)
                                  .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        
        Month newMonth = localDate.getMonth();
        while (newMonth == month) {
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            newMonth = localDate.getMonth();
        }
    }
}
*/

package com.kpriet.java.training.date_and_time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class ListOfMondays {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the name of the month to which list of mondays to find: ");
        String givenMonth = scanner.nextLine();
        scanner.close();
        Month month = Month.valueOf(givenMonth.toUpperCase());
        System.out.printf("For the month of %s%n", month);
        LocalDate localDate = Year.now().atMonth(month).atDay(1)
                                  .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        
        Month newMonth = localDate.getMonth();
        while (newMonth == month) {
            System.out.println(localDate);
            localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            newMonth = localDate.getMonth();
        }
    }
}
