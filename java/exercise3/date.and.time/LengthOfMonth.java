/*
Requirement:
    To write an example that, for a given year, reports the length of each month within that particular year.
    
Entity:
    LengthOfMonth 

Function Declaration:
    public static void main(String[] args);
    
Jobs to be done:
    1. Get the year as input and store it in integer variable givenYear.
    2. For the given year print the days foe each month.

    
Pseudo code:
class LengthOfMonth {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year: ");
        int givenYear = scanner.nextInt();
        scanner.close();
        for (int month = 1; month <= 12; month++) {
        	Month nextMonth = Month.of(month);
        	System.out.println("Month " + month + " : " + nextMonth.maxLength() + " Days");
        }
    }
}
*/

package com.kpriet.java.training.date_and_time;

import java.util.Scanner;
import java.time.Month;

public class LengthOfMonth {
    
    @SuppressWarnings("unused")
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year: ");
        int givenYear = scanner.nextInt();
        scanner.close();
        for (int month = 1; month <= 12; month++) {
        	Month nextMonth = Month.of(month);
        	System.out.println("Month " + month + " : " + nextMonth.maxLength() + " Days");
        }
    }
}
