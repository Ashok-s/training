/*
requirement:
	To print the following Date Formats in Date using locale.
    DEFAULT, MEDIUM, LONG, SHORT, FULL. 
	
Entity:
	DateFormatDemo
	
Function Declaration:
	public static void main(String[] args); 
	
Jobs to be Done:
	1)Get the locale file to access the date 
	2)Print the date format in Default, medium, long, short and full for particular locale.
		
Pseudo Code:
class DateFormatDemo {

	public static void main(String[] args) {
	    Date date = new Date();
	    Locale locale = new Locale("en", "UK");
		        
        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.DEFAULT,locale); 
        String date1 = dateFormat1.format(date);
        System.out.println("DEFAULT Date is:" + date1);                                  
		        
        DateFormat dateFormat2 = DateFormat.getDateInstance(DateFormat.SHORT,locale);
        String date2 = dateFormat2.format(date);
        System.out.println("SHORTED Date is:" + date2);                                 
		        
        DateFormat dateFormat3 = DateFormat.getDateInstance(DateFormat.MEDIUM,locale);
        String date3 = dateFormat3.format(date);
		System.out.println("MEDIUM Date is:" + date3);                                  
		        
        DateFormat dateFormat4 = DateFormat.getDateInstance(DateFormat.LONG,locale);
        String date4 = dateFormat4.format(date);
        System.out.println("LONG Date is:" + date4);                                    
		        
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL,locale);
        String date5 = dateFormat.format(date);
        System.out.println("FULL Date is:" + date5);  
    } 
}  
 */

package com.kpriet.training.java.internationalization;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatDemo {

    public static void main(String[] args) {
        Date date = new Date();
        Locale locale = new Locale("en", "UK"); 
        System.out.println("Today Date is :" + date);

        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.DEFAULT, locale); 
        String date1 = dateFormat1.format(date);
        System.out.println("DEFAULT Date is:" + date1);                                 
        
        DateFormat dateFormat2 = DateFormat.getDateInstance(DateFormat.SHORT, locale);
        String date2 = dateFormat2.format(date);
        System.out.println("SHORTED Date is:" + date2);                                 
        
        DateFormat dateFormat3 = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
        String date3 = dateFormat3.format(date);
        System.out.println("MEDIUM Date is:" + date3);                                  
        
        DateFormat dateFormat4 = DateFormat.getDateInstance(DateFormat.LONG, locale);
        String date4 = dateFormat4.format(date);
        System.out.println("LONG Date is:" + date4);                                   
        
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL, locale);
        String date5 = dateFormat.format(date);
        System.out.println("FULL Date is:" + date5);                                    
    
    }
}