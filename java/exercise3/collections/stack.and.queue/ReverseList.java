/*
 * Requirements : 
 * 		To Reverse List Using Stack with minimum 7 elements in list.
 * 
 * Entities :
 * 		ReverseList.
 * 
 * Function Declaration :
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Create reference for the list and add elements to the list and print them.
 * 		2.Create a stack 
 * 			2.1)For each element in the list add that element to the stack.
 * 		3.Clear the existing list 
 * 		4.For the size of the stack pop the element from the stack and add 
 * 		  that element to the list.
 * 		5.Print the reversed list. 
 * 
 * Pseudo code:
 * class ReverseList {
 * 
 *     public static void main(String[] args) {
 *		   ArrayList<Integer> list = new ArrayList<>();
 *		   list.add(7);
 *		   list.add(6);
 *		   list.add(5);
 *		   list.add(4);
 *		   list.add(3);
 *		   list.add(2);
 *		   list.add(1);
 *		   System.out.println("Before Reversing : " + list); 
 *
 *		   Stack<Integer> stack = new Stack<>();
 *		   for (Integer value : list) {
 *			   stack.push(value);
 *		   }
 *		   list.clear();
 *		   int size = stack.size();
 *		   for (int iteration = 0; iteration < size; iteration++) {
 *			   list.add(stack.pop());
 *		   }
 *		   System.out.println("After Reversing : " + list);
 *     }   
 * }
 */

package com.kpriet.training.java.stackqueue;

import java.util.ArrayList;
import java.util.Stack;

public class ReverseList {
	
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(7);
		list.add(6);
		list.add(5);
		list.add(4);
		list.add(3);
		list.add(2);
		list.add(1);
		System.out.println("Before Reversing : " + list);

		Stack<Integer> stack = new Stack<>();
		for (Integer value : list) {
			stack.push(value);
		}
		list.clear();
		int size = stack.size();
		for (int iteration = 0; iteration < size; iteration++) {
			list.add(stack.pop());
		}
		System.out.println("After Reversing : " + list);

	}
}
