/*
 * Requirement:
 *   Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
 * -> add at least 5 elements
 * -> remove the front element
 * -> search a element in stack using contains key word and print boolean value value
 * -> print the size of stack
 * -> print the elements using Stream 
 *
 * Entity:
 *     QueueGeneric
 *     
 * Function Declaration:
 *     public static void main(String[] args);
 *      
 * Jobs to be done:
 *     1.Create a reference for queue and add elements to the queue and print them.
 *     2.Remove the first element from the queue.
 *     3.Check whether the element is present in the queue or not.
 *     4.Print the size of the queue.
 *     5.For each element in the queue print the elements.
 *
 * Pseudo code:
 *	   class QueueGeneric {
 *
 *         public static void main(String[] args) {
 *	    	   System.out.println("Using LinkedList");
 *	           Queue<Integer> queue = new LinkedList<>();
 *	           queue.add(1);
 *             queue.add(2);
 *             queue.add(3);
 *	           queue.add(4);
 *	           queue.add(5);
 *	           System.out.println("The queue elements are ");
 *	           System.out.println(queue);
 *	           System.out.println("Removing the front element " + queue.remove());
 *	           System.out.println("After removing the first element " + queue);
 *	           boolean element = queue.contains(3);
 *	           System.out.println(element);
 *	           System.out.println("The size of the queue is " + queue.size());
 *	           Stream<Integer> stream = queue.stream();
 *	           System.out.println("The queue element are printed by using stream");
 *	           stream.forEach(elements -> System.out.print(elements + " "));
 *	        
 *	           System.out.println("Using Priority Queue");
 *	           Queue<Integer> queue1 = new PriorityQueue<>();
 *	           queue1.add(1);
 *	           queue1.add(2);
 *	           queue1.add(3);
 *	           queue1.add(4);
 *	           queue1.add(5);
 *	           System.out.println("The queue elements are ");
 *	           System.out.println(queue1);
 *	           System.out.println("Removing the front element " + queue1.remove());
 *	           System.out.println("After removing the first element " + queue1);
 *	           boolean element1 = queue1.contains(3);
 *	           System.out.println(element1);
 *	           System.out.println("The size of the queue is " + queue1.size());
 *	           Stream<Integer> stream1 = queue1.stream();
 *	           System.out.println("The queue element are printed by using stream");
 *	           stream1.forEach(elements -> System.out.print(elements + " "));  
 *	       }
 *	   }     
 */

package com.kpriet.training.java.stackqueue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueGeneric {

    public static void main(String[] args) {
    	System.out.println("Using LinkedList");
        Queue<Integer> queue = new LinkedList<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        System.out.println("The queue elements are ");
        System.out.println(queue);
        // Removing the front element
        System.out.println("Removing the front element " + queue.remove());
        System.out.println("After removing the first element " + queue);
        // Check if Queue Contains Element
        boolean element = queue.contains(3);
        System.out.println(element);
        System.out.println("The size of the queue is " + queue.size());
        Stream<Integer> stream = queue.stream();
        System.out.println("The queue element are printed by using stream");
        stream.forEach(elements -> System.out.print(elements + " "));
        
        System.out.println("Using Priority Queue");
        Queue<Integer> queue1 = new PriorityQueue<>();
        queue1.add(1);
        queue1.add(2);
        queue1.add(3);
        queue1.add(4);
        queue1.add(5);
        System.out.println("The queue elements are ");
        System.out.println(queue1);
        // Removing the front element
        System.out.println("Removing the front element " + queue1.remove());
        System.out.println("After removing the first element " + queue1);
        // Check if Queue Contains Element
        boolean element1 = queue1.contains(3);
        System.out.println(element1);
        System.out.println("The size of the queue is " + queue1.size());
        Stream<Integer> stream1 = queue1.stream();
        System.out.println("The queue element are printed by using stream");
        stream1.forEach(elements -> System.out.print(elements + " "));
        
    }

}