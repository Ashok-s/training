/*
 * Requirement:
 * To find the output of the code and complete the code.
 *     Queue bike = new PriorityQueue();    
 *     bike.poll();
 *     System.out.println(bike.peek());    
 *      
 *      
 *Entity:
 *   QueueDemo
 *   
 *Function Declaration:
 *   public static void main(String[] args);
 *   
 * Jobs to be done:
 *    1.Print the output of the Given code.
 */

package com.kpriet.training.java.stackqueue;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueDemo {

    public static void main(String[] args) {
        Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());  //Prints null
        
    }

}
//output of the code: null