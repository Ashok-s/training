/*
 * Requirement:
 *     Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),
 *     pollFirst(),poolLast() methods to store and retrieve elements in ArrayDeque .
 *
 * Entity:
 *     ArrayDequeMethod
 *
 * Function Declaration:
 *     public static void main(String[] args) 
 *
 * Jobs to be Done;
 *     1.Create a reference for ArrayDeque with a generic type.
 *     2.Add the elements to the deque.
 *     3.Print the elements in the deque.
 *     4.Add the elements to the first and last to the deque.
 *     5.Remove the first and last elements in the deque and print the deque
 *     6.print the first and last elements in the deque with methods peekFirst(), pollFirst(), peekLast()
 *       and pollLast().
 *       
 * Pseudo code:
 *     class ArrayDequeMethod {
 *     
 *         public static void main(String[] args) {
 *			   ArrayDeque<Integer> deque = new ArrayDeque<>();
 *			   deque.add(1);
 *			   deque.add(2);
 *			   deque.add(3);
 *			   deque.add(4);
 *			   deque.add(5);
 *			   deque.add(6);
 *			   deque.add(7);
 *			   deque.add(8);
 *			   System.out.println("The ArrayDeque " + deque);
 *			   deque.addFirst(0);
 *			   deque.addLast(9);
 *			   System.out.println("After adding the first and last element" + deque);
 *			   deque.removeFirst();
 *			   deque.removeLast();
 *			   System.out.println("After removing the first and last element" + deque);
 *			   System.out.println("The first peek element " + deque.peekFirst());
 *			   System.out.println("The last  peek element " + deque.peekLast());
 *			   System.out.println("The first poll element " + deque.pollFirst());
 *			   System.out.println("The last poll element " + deque.pollLast());
 *         }
 *     } 
 */

package com.kpriet.training.java.deque;

import java.util.ArrayDeque;

public class ArrayDequeMethod {

	public static void main(String[] args) {
		ArrayDeque<Integer> deque = new ArrayDeque<>();
		deque.add(1);
		deque.add(2);
		deque.add(3);
		deque.add(4);
		deque.add(5);
		deque.add(6);
		deque.add(7);
		deque.add(8);
		System.out.println("The ArrayDeque " + deque);
		deque.addFirst(0);
		deque.addLast(9);
		System.out.println("After adding the first and last element" + deque);
		deque.removeFirst();
		deque.removeLast();
		System.out.println("After removing the first and last element" + deque);
		System.out.println("The first peek element " + deque.peekFirst());
		System.out.println("The last  peek element " + deque.peekLast());
		System.out.println("The first poll element " + deque.pollFirst());
		System.out.println("The last poll element " + deque.pollLast());
	}
}

