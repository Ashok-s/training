/*
 *Requirement:
   To create a stack using generic type and implement
      1)Push at least 5 elements.
      2)Pop the peek element.
      3)Search a element in stack and print the index value.
      4)print the size of stack.
      5)print the elements using stream.      

Entity:
    public class StackDemo

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1.Create a reference for stack and add the elements to them.
    2.Print the peek element and pop the peek element.
    3.Print the stack after poping the element.
    4.Print the index of the given element.
    5)For each element in the stack print the element.
    
Pseudo code:
class StackDemo {

	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<>();
		stack.push(1);
		stack.push(5);
		stack.push(10);
		stack.push(20);
		stack.push(35);
		System.out.println("The peek element is " + stack.peek());
		System.out.println("The poped element is " + stack.pop());
		System.out.println("The Stack after poped is " + stack);
		System.out.println("The Index of the element '20' is " + stack.indexOf(20));
		System.out.println("The Size of the Stack is " + stack.size());
		Stream<Integer> stream = stack.stream();
		System.out.print("The Stack elements printed using Stream ");
		stream.forEach(i -> System.out.print(i + " "));
	}
}
*/

package com.kpriet.training.java.stackqueue;

import java.util.Stack;
import java.util.stream.Stream;

public class StackDemo {
	
	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<>();
		stack.push(1);
		stack.push(5);
		stack.push(10);
		stack.push(20);
		stack.push(35);
		System.out.println("The peek element is " + stack.peek());
		System.out.println("The poped element is " + stack.pop());
		System.out.println("The Stack after poped is " + stack);
		System.out.println("The Index of the element '20' is " + stack.indexOf(20));
		System.out.println("The Size of the Stack is " + stack.size());
		Stream<Integer> stream = stack.stream();
		System.out.print("The Stack elements printed using Stream ");
		stream.forEach(i -> System.out.print(i + " "));
	}
}
