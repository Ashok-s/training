/*
Requirement:
    To Add and remove the elements in stack.

Entity:
    StackExample

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1.Create a reference for stack with a generic type.
    2.Add the elements to the stack.
    3.Print the stack.
    4.remove the elements in the stack.
    5.print the stack after removal of some elements.
    
Pseudo code:
    class StackExample {

        public static void main(String[] args) {
            Stack<Integer> stack = new Stack<>();
            stack.push(10);
            stack.push(20);
            stack.push(30);
            stack.push(40);
            stack.push(50);
            System.out.println(stack);
            stack.remove(2);
            stack.remove(1);
            System.out.println("After removing the some of element :" + stack);
        }
    }
*/

package com.kpriet.training.java.stackqueue;

import java.util.Stack;

public class StackExample {
    
	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<>();
		stack.push(10);
		stack.push(20);
		stack.push(30);
		stack.push(40);
		stack.push(50);
		System.out.println(stack);
		stack.remove(2);
		stack.remove(1);
		System.out.println("After removing the some of element :" + stack);
	}
}
