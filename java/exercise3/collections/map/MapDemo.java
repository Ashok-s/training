/*
 * Requirements:
 *    1) To write a Java program to copy all of the mappings from the specified map to another map?
 *    2) To write a Java program to test if a map contains a mapping for the specified key?
 *    3) Count the size of mappings in a map?
 *    4) To Write a Java program to get the portion of a map whose keys range from a given key to another key?
 
 * Entities:
 *    MapDemo
 *
 * Function Declaration:
 *    public static void main(String[] args); 
 *
 * Jobs To Be Done:
 *    1.Create reference for map of type TreemMap.
 *    2.Add key value pair and print them.
 *    3.Create reference for another map and copy all the key value pair from the previous map.
 *    4.Print the size, particular value, and subMap of the map. 
 *
 * Pseudo code:
 *     class MapDemo {
 *     
 *         public static void main(String[] args) {
 *			   TreeMap<Integer, String> map = new TreeMap<>();
 *			   map.put(1, "Ashok");
 *			   map.put(2, "Aravind");
 *			   map.put(3, "Arun");
 *			   map.put(4, "Ajay");
 *			   map.put(5, "Anand");
 *			   System.out.println(map);
 *	
 *			   TreeMap<Integer, String> map1 = new TreeMap<>();
 *			   map1.putAll(map);
 *			   System.out.println(map1);
 *			   System.out.println(map1.size());
 *			   System.out.println(map1.get(3));
 *			   System.out.println(map1.subMap(2, 5));
 *	       }
 *     }
 */

package com.kpriet.training.java.map;

import java.util.TreeMap;

public class MapDemo {

	public static void main(String[] args) {
		TreeMap<Integer, String> map = new TreeMap<>();
		map.put(1, "Ashok");
		map.put(2, "Aravind");
		map.put(3, "Arun");
		map.put(4, "Ajay");
		map.put(5, "Anand");
		System.out.println(map);

		TreeMap<Integer, String> map1 = new TreeMap<>();
		map1.putAll(map);
		System.out.println(map1);
		System.out.println(map1.size());
		System.out.println(map1.get(3));
		System.out.println(map1.subMap(2, 5));

	}
}
