/*
 * Requirement:
 *      To demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() )
 * 
 * Entity:
 *      MapMethods
 * 
 * Function declaration:
 *      public static void main(String[] args);
 * 
 * Jobs to be done:
 *      1.Create reference for map with key and value of types Integer and String respectively.
 *      2.Add elements to map and print the elements in the map.
 *      3.Remove an element from the map and print the map.
 *      4.Check whether the value is present in the map or not.
 *      5.Replace the value of one's key with other value.
 *      
 * Pseudo code:
 * 		class MapMethods {
 * 
 * 			public static void main(String[] args) {
 *		    	HashMap<Integer, String> map = new HashMap<>();
 *				map.put(1, "Tamil");
 *				map.put(2, "English");
 *				map.put(3, "Maths");
 *				map.put(4, "Science");
 *				map.put(5, "Social");
 *				System.out.println("The map elements are " + map);
 *				map.remove(5);
 *				System.out.println("After removing element the map is " + map);
 *				System.out.println(map.containsValue("Tamil"));
 *				System.out.println(map.containsValue("Social"));
 *				map.replace(4, "Social");
 *				System.out.println("After replacing the key and value of map " + map);
 *			}
 * 		}     
 */

package com.kpriet.training.java.map;

import java.util.HashMap;

public class MapMethods {
	
	public static void main(String[] args) {
		HashMap<Integer, String> map = new HashMap<>();
		map.put(1, "Tamil");
		map.put(2, "English");
		map.put(3, "Maths");
		map.put(4, "Science");
		map.put(5, "Social");
		System.out.println("The map elements are " + map);
		map.remove(5);
		System.out.println("After removing element the map is " + map);
		System.out.println(map.containsValue("Tamil"));
		System.out.println(map.containsValue("Social"));
		map.replace(4, "Social");
		System.out.println("After replacing the key and value of map " + map);
	}
}
