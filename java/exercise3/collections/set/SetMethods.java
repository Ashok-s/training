/*
 * Requirements : 
 * 		To explain the working of contains(), isEmpty() and give example.
 * 
 * Entities :
 * 		SetMethods.
 * 
 * Function Declaration :
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Create reference for the set and add elements to them.
 * 	    2.Check whether the given element is present in the set or not.
 *      3.Check whether the set is empty or not.
 *      
 * Pseudo code:
 *     class SetMethods {
 *     
 *         public static void main(String[] args) {
 *			   HashSet<Integer> set = new HashSet<>();
 *			   set.add(10);
 *			   set.add(20);
 *			   set.add(30);
 *			   System.out.println(set.contains(100)); // returns true
 *			   System.out.println(set.isEmpty()); // returns false
 *		   }
 *     }
 */

/*
 * Explanation:
 * 	contains(<T>):
 * 		returns true if the element is present in the list else returns false.
 * 
 * 	isEmpty(<T>):
 * 		returns true if the set is empty else returns false.
 */

package com.kpriet.training.java.set;

import java.util.HashSet;

public class SetMethods {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();
		set.add(10);
		set.add(20);
		set.add(30);
		System.out.println(set.contains(100)); // returns true
		System.out.println(set.isEmpty()); // returns false
	}

}
