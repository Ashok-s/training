/*
 * Requirements:
 *    To demonstrate linked hash set to array() method in java.
 * 
 * Entities:
 *    SetToArray.
 *
 * Function Declaration:
 *    public static void main(String[] args);
 *    
 * Jobs To Be Done:
 *    1.Create reference for linked set and add elements to it.
 *    2.Print the set.
 *    3.Convert the set to array.
 *    4.For each element in the  array print the elements.
 *    
 * Pseudo code:
 *    class LinkedHashSetDemo {
 *    
 *        public static void main(String[] args) {
 *            LinkedHashSet<Integer> set = new LinkedHashSet<>();
 *            set.add("element0");
 *            set.add("element1");
 *            set.add("element2");
 *            set.add("element3");
 *            set.add("element4");
 *            System.out.println("The Linked Hash Set " + set);
 *            Object[] array = set.toArray();
 *            System.out.println("The array is");
 *            for (Object element : array) {
 *   		      System.out.println(element + " ");
 *		      }
 *        }
 *    } 		      
 */

package com.kpriet.training.java.set;

import java.util.LinkedHashSet;

public class SetToArray {
	
	public static void main(String[] args) {
		LinkedHashSet<String> set = new LinkedHashSet<>();
		set.add("element0");
		set.add("element1");
		set.add("element2");
		set.add("element3");
		set.add("element4");
		System.out.println("The Linked Hash Set " + set);
		Object[] array = set.toArray();
		System.out.println("The array is");
		for (Object element : array) {
			System.out.println(element + " ");
		}
	}
}
