/*
Requirements:
    To write a java program to demonstrate adding elements, displaying, removing, and iterating in 
    hash set

Entity:
    HashSetDemo

Function Declaration:
    public void operate();
    public static void main(String[] args);

Jobs To Be Done:
    1.Get the set of inputs from the user
    2.Invoke the method operate
    3.Under the method operate()
        3.1)Add the elements in the set and print them.
        3.2)Remove some elements from the set.
        3.3)Iterate the set and print the elements in the set.

Pseudo code:
class HashSetDemo {

    public void operate(set) {
    	set.add();
    	System.out.println(set);
    	set.remove();
    	for (T element : set) {
        	System.out.println(element);
    	}
    }

    public static void main(String[] args) {
        HashSet<?> set = new HashSet<>();
        operate(set);
    }
}
 */

package com.kpriet.training.java.set;

import java.util.HashSet;

public class HashSetDemo {

	public static void operate(HashSet<String> set) {
		set.add("arg0");
		set.add("arg1");
		set.add("arg2");
		System.out.println(set);
		set.remove("arg1");
		for (String element : set) {
			System.out.println(element);
		}
	}

	public static void main(String[] args) {
		HashSet<String> set = new HashSet<>();
		operate(set);
	}
}
