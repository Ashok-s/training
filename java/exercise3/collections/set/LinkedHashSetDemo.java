/*
 * Requirements:
 *    To demonstrate the basic add and traversal operation of linked hash set.
 * 
 * Entities:
 *    LinkedHashSetDemo.
 *
 * Function Declaration:
 *    public static void main(String[] args);
 *    
 * Jobs To Be Done:
 *    1.Create reference for a linked set and add elements to it.
 *    2.For each of the element in the set print the element.
 *    
 * Pseudo code:
 *    class LinkedHashSetDemo {
 *   
 *        public static void main(String[] args) {
 *            LinkedHashSet<Integer> set = new LinkedHashSet<>();
 *            set.add(10);
 *            set.add(20);
 *            set.add(30);
 *            set.add(40);
 *            set.add(50);
 *            Iterator<Integer> iterator = set.iterator();
 *            while (iterator.hasNext()) {
 *   		      System.out.println(iterator.next() + " ");
 *		      }
 *        }
 *    } 		      
 */

package com.kpriet.training.java.set;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

	public static void main(String[] args) {
		LinkedHashSet<Integer> set = new LinkedHashSet<>();
		set.add(10);
		set.add(20);
		set.add(30);
		set.add(40);
		set.add(50);
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next() + " ");
		}
	}
}
