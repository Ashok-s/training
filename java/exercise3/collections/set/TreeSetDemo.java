/*
 * Requirements:
 *    To demonstrate insertions and string buffer in tree set.
 * 
 * Entities:
 *    TreeSetDemo.
 *
 * Function Declaration:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create reference for set and add elements to it.
 *    2.Print the elements in the set.
 *    
 * Pseudo code:
 *    class TreeSetDemo {
 *   
 *        public static void main(String[] args) {
 *            TreeSet<StringBuffer> treeSet = new TreeSet<>();
 *            TreeSet<StringBuffer> treeSet = new TreeSet<>();
 *			  treeSet.add(new StringBuffer("A"));
 *	          treeSet.add(new StringBuffer("B"));
 *			  treeSet.add(new StringBuffer("C"));
 *			  treeSet.add(new StringBuffer("D"));
 *			  treeSet.add(new StringBuffer("E"));
 *			  System.out.println(treeSet); 
 *		  }
 *    }		      
 */

package com.kpriet.training.java.set;

import java.util.TreeSet;

public class TreeSetDemo {

		public static void main(String[] args) {
			TreeSet<StringBuffer> treeSet = new TreeSet<>();
			treeSet.add(new StringBuffer("A"));
			treeSet.add(new StringBuffer("B"));
			treeSet.add(new StringBuffer("C"));
			treeSet.add(new StringBuffer("D"));
			treeSet.add(new StringBuffer("E"));
			System.out.println(treeSet);
		}
}

