/*
 * Requirements : 
 * 		To do the following:
 * Create a set
 *  => Add 10 values
 *  => Perform addAll() and removeAll() to the set.
 *  => Iterate the set using 
 *      - Iterator
 *      - for-each
 *
 * Entities :
 * 		SetDemo.
 * 
 * Function Declaration :
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Create reference for the Set set and add elements to them
 *		2.Create reference for another Set newSet and add all the elements which is present in the Set set   
 *		3.Add some other elements to the newSet and print all the elements in the newSet.
 *      4.Remove all the elements in the newSet.
 *      5.Print the elements in the set using various loops.
 *      
 * Pseudo code:
 *     class SetDemo {
 *     
 *         public static void main(String[] args) {
 *			   HashSet<Integer> set = new HashSet<>();
 *			   set.add(10);
 *			   set.add(20);
 *			   set.add(30);
 *			   set.add(40);
 *			   set.add(50);
 *			   set.add(60);
 *			   set.add(70);
 *			   set.add(80);
 *			   set.add(90);
 *			   set.add(100);
 *	
 *			   HashSet<Integer> newSet = new HashSet<>();
 *			   newSet.addAll(set);
 *			   System.out.println("After using addAll method : " + newSet);
 *			   newSet.add(200);
 *			   newSet.add(300);
 *			   newSet.removeAll(set);
 *			   System.out.println("After using removeAll method : " + newSet);
 *	
 *			   System.out.println("Using Iterator");
 *			   Iterator<Integer> iterator = set.iterator();
 *			   while (iterator.hasNext()) {
 *				   System.out.print(iterator.next() + " ");
 *		   	   }
 *			   System.out.println();
 *	
 *			   System.out.println("Using for-each");
 *			   for (int values : set) {
 *				   System.out.print(values + " ");
 *			   }
 *		   }
 *     }      
 */

package com.kpriet.training.java.set;

import java.util.HashSet;
import java.util.Iterator;

public class SetDemo {
	
	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();
		set.add(10);
		set.add(20);
		set.add(30);
		set.add(40);
		set.add(50);
		set.add(60);
		set.add(70);
		set.add(80);
		set.add(90);
		set.add(100);

		HashSet<Integer> newSet = new HashSet<>();
		newSet.addAll(set);
		System.out.println("After using addAll method : " + newSet);
		newSet.add(200);
		newSet.add(300);
		newSet.removeAll(set);
		System.out.println("After using removeAll method : " + newSet);

		// using Iterator
		System.out.println("Using Iterator");
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();

		// using for each
		System.out.println("Using for-each");
		for (int values : set) {
			System.out.print(values + " ");
		}
	}

}
