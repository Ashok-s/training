/*
 * Requirements : 
 * 		To explain about contains(), subList(), retainAll() and give example
 * 
 * Entities :
 * 		ListMethods.
 * 
 * Function Declaration :
 * 		public static void main(String[] args);
 *
 * Jobs To Be Done:
 *      1.Create reference for the array list and add elements to that.
 * 		2.Invoke the contains() method to check the element whether it is present in the list or not.  
 * 		3.Create a sublist from a list.
 * 		4.Create reference for the another list and use the retainAll() method. 
 * 		
 * Pseudo code:
 *     class ListMethods {
 *     
 *         public static void main(String[] args) {
 *			   ArrayList<Integer> list = new ArrayList<>();
 *			   list.add(100);
 *			   list.add(200);
 *			   list.add(300);
 *			   list.add(400);
 *			   list.add(500);
 *			   list.add(600);
 *			   System.out.println(list.contains(500));
 *			   System.out.println(list.subList(1, 4));
 *			   ArrayList<Integer> newList = new ArrayList<>();
 *			   newList.add(400);
 *		   	   newList.add(300);
 *			   newList.add(1000);
 *			   System.out.println("Before using retainall method : " + newList);
 *			   newList.retainAll(list);
 *			   System.out.println("After using retainall method : " + newList);
 *		   }   
 *     }
 */

/*
 * Explanation:
 * 	contains():
 * 		returns true if the element is present in the list else returns false.
 * 	subList(starting index, ending index):
 * 		returns the list elements from starting index to ending index.
 * 	retainAll():
 * 		It is used to remove all the array list's elements that are not contained in the specified list.
 */

package com.kpriet.training.java.list;

import java.util.ArrayList;

public class ListMethods {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(100);
		list.add(200);
		list.add(300);
		list.add(400);
		list.add(500);
		list.add(600);
		System.out.println(list.contains(500)); // it prints true
		System.out.println(list.subList(1, 4)); //prints elements from index 1 to 3
		
		ArrayList<Integer> newList = new ArrayList<>();
		newList.add(400);
		newList.add(300);
		newList.add(1000);
		System.out.println("Before using retainall method : " + newList);
		newList.retainAll(list);
		System.out.println("After using retainall method : " + newList);
	}

}
