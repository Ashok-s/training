/*
 * Requirements : 
 * 		To Code for sorting vector list in descending order
 *
 * Entities :
 * 		ReverseList.
 *
 * Function Declaration :
 * 		public static void main(String[] args);
 *
 * Jobs To Be Done:
 * 		1.Create reference for the list with generic type
 *      2.Add the elements to that list
 *      3.Print the list in the same order
 *      4.Sort the list in ascending order and print the list in reversed order.
 * 	
 * Pseudo code:
 *     class ReverseList {
 *     
 *     	   public static void main(String[] args) {
 *             ArrayList<Integer> list = new ArrayList<>();
 *			   list.add(10);
 *			   list.add(30);
 *			   list.add(20);
 *			   list.add(40);
 *			   list.add(60);
 *			   list.add(70);
 *			   list.add(50);
 *		       System.out.println("Before Sorting : " + list);
 *			   Collections.sort(list, Collections.reverseOrder());
 *			   System.out.println("After Reversing :" + list);      
 *         }
 *     }
 */

package com.kpriet.training.java.list;

import java.util.ArrayList;
import java.util.Collections;

public class ReverseList {
	
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(30);
		list.add(20);
		list.add(40);
		list.add(60);
		list.add(70);
		list.add(50);
		System.out.println("Before Sorting : " + list);
		Collections.sort(list, Collections.reverseOrder());
		System.out.println("After Reversing :" + list);
	}
}
