/*
 * Requirements:
 *    LIST CONTAINS 10 STUDENT NAMES
 *    krishnan, abishek, arun, vignesh, kiruthiga, murugan, adhithya,balaji,vicky, priya and 
 *    display only names starting with 'A'.
 *
 * Entities:
 *    Student
 *
 * Function Declaration:
 *    public static void main(String[] args);
 *
 * Jobs To Be Done:
 *    1.Create reference for the list that contains student name and print the list
 *    2.Change the lower case names to upper case.
 *    3.Print the names that starts with 'A'.
 *    
 * Pseudo code:
 *    class Student {
 *    
 *    	  public static void main(String[] args) {
 *            List<String> names = Arrays.asList("krishnan", "abishek", "arun", "vignesh", "kiruthiga",
 *                                               "murugan", "adhithya", "balaji", "vicky", "priya");
 *            System.out.println("list" + names);
 *	          names.replaceAll(String::toUpperCase);
 *		      System.out.println("Uppercase of the list is" + names);
 *		      for (String name : names) {
 *			      if (name.startsWith("A")) {
 *				      System.out.println(name);
 *			      }
 *		      }
 *    	  }
 *    }
 */

package com.kpriet.training.java.list;

import java.util.List;
import java.util.Arrays;

public class Student {

	public static void main(String[] args) {
		List<String> names = Arrays.asList("krishnan", "abishek", "arun", "vignesh", "kiruthiga",
                                           "murugan", "adhithya", "balaji", "vicky", "priya");
		System.out.println("list" + names);
		names.replaceAll(String::toUpperCase);
		System.out.println("Uppercase of the list is" + names);
		for (String name : names) {
			if (name.startsWith("A")) {
				System.out.println(name);
			}
		}
	}
}
