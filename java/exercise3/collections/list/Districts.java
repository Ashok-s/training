/*
 * Requirement:
 *      8 districts are shown below
 *  Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.
 *
 * Entity:
 *     Districts
 *
 * Function Declaration:
 *     public static void main(String[] args)
 *
 * Jobs to be done:
 *     1.Create reference for the list containing district names.
 *     2.Change the district names to UpperCase.
 *     3.Print the case changed district names.
 *
 * Pseudo code:
 *     class Districts {
 *
 *         public static void main(String[] args) {
 *              List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur",
 *				                                  "Salem", "Erode", "Trichy");
 *		       list.replaceAll(String::toUpperCase);
 *              System.out.println(list);
 *	       }
 *      }
 */

package com.kpriet.training.java.list;

import java.util.Arrays;
import java.util.List;

public class Districts {

	public static void main(String[] args) {
		List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur",
				                          "Salem", "Erode", "Trichy");
		list.replaceAll(String::toUpperCase);
		System.out.println(list);
	}

}
