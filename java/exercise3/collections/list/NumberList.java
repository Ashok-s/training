/*
 * Requirements : 
 * 		To do the following:
 * Create a list
 *   => Add 10 values in the list
 *   => Create another list and perform addAll() method with it
 *   => Find the index of some value with indexOf() and lastIndexOf()
 *   => Print the values in the list using 
 *       - For loop
 *      - For Each
 *       - Iterator
 *       - Stream API
 *   => Convert the list to a set
 *   => Convert the list to a array
 *
 * Entities :
 * 		NumberList.
 * 
 * Function Declaration :
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Create reference for the list and add elements to it and print the list.
 *   	2.Create another reference for the new list and add all the elements in the old lis.
 * 		3.Get the index of the particular elements from the list.
 * 	    4.Print the elements in the list using various loops.
 * 		5.Convert the list into set and array.
 *
 * Pseudo code:
 *      class NumberList {
 *      
 *          public static void main(String[] args) {
 *				ArrayList<Integer> list = new ArrayList<>();
 *				list.add(1);
 *				list.add(2);
 *				list.add(3);
 *				list.add(4);
 *				list.add(5);
 *				list.add(6);
 *				list.add(4);
 *				list.add(8);
 *				list.add(9);
 *				list.add(6);
 *				System.out.println(list);
 *		
 *				ArrayList<Integer> newList = new ArrayList<>();
 *				newList.addAll(list);
 *				System.out.println("After adding all values to newlist : " + newList);
 *				System.out.println("The index of value 6 is : " + newList.indexOf(6));
 *				System.out.println("The index of value 4 is : " + newList.indexOf(4));
 *				System.out.println("Last index value of 4 is : " + newList.lastIndexOf(6));
 *		
 *				System.out.println("Using for loop :");
 *				for (int index = 0; index < newList.size(); index++) {
 *					System.out.print(newList.get(index) + " ");
 *				}
 *				
 *				System.out.println("Using for each :");
 *				for (int value : newList) {
 *					System.out.print(value + " ");
 *				}
 *		
 *				System.out.println("using iterator :");
 *				Iterator<Integer> iterator = newList.iterator();
 *				while (iterator.hasNext()) {
 *					System.out.print(iterator.next() + " ");
 *				}
 *		
 *				System.out.println("Using Stream :");
 *				Stream<Integer> stream = newList.stream();
 *				stream.forEach(value -> System.out.print(value + " "));
 *		
 *				Set<Integer> set = new HashSet<>(newList);
 *				System.out.println("Converted to Set : " + set);
 *	  	
 *				Integer[] array = new Integer[newList.size()];
 *				array = newList.toArray(array);
 *				System.out.println("converted to array : " + Arrays.toString(array));
 *			}
 *      }
 */

package com.kpriet.training.java.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

public class NumberList {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(4);
		list.add(8);
		list.add(9);
		list.add(6);
		System.out.println(list);

		ArrayList<Integer> newList = new ArrayList<>();
		newList.addAll(list);
		System.out.println("After adding all values to newlist : " + newList);
		System.out.println("The index of value 6 is : " + newList.indexOf(6));
		System.out.println("The index of value 4 is : " + newList.indexOf(4));
		System.out.println("Last index value of 4 is : " + newList.lastIndexOf(6));

		// using for loop
		System.out.println("Using for loop :");
		for (int index = 0; index < newList.size(); index++) {
			System.out.print(newList.get(index) + " ");
		}

		// using For Each
		System.out.println("Using for each :");
		for (int value : newList) {
			System.out.print(value + " ");
		}

		// Using iterator
		System.out.println("using iterator :");
		Iterator<Integer> iterator = newList.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}

		// Using stream api
		System.out.println("Using Stream :");
		Stream<Integer> stream = newList.stream();
		stream.forEach(value -> System.out.print(value + " "));

		// converting list to a set
		Set<Integer> set = new HashSet<>(newList);
		System.out.println("Converted to Set : " + set);

		// converting to a array
		Integer[] array = new Integer[newList.size()];
		array = newList.toArray(array);
		System.out.println("converted to array : " + Arrays.toString(array));
	}
	
}
