/*Requirement:
 *    To read the file contents  line by line in streams with example
 *  
 *Entity:
 *    ReadFile
 *  
 *Function Declaration:
 *    public static void main(String[] args);
 *  
 *Jobs to be Done:
 *    1)Create an object for FileInputStream.
 *    2)Read the file and store the value in a integer variable.
 *    3)Until the value comes -1 read the file.
 *    4)Print the readed content.
 *    5)Close the input stream
 *  
 *Pseudo code:
  class ReadFile {
  
      public static void main(String[] args) throws IOException {
          FileInputStream reader = new FileInputStream("nioexercise.txt");

          int data = reader.read();
          while (data != -1) {
              data = reader.read();
              System.out.println(data);
          }
          reader.close();
      }
  }
 * 
 */

package com.kpriet.training.java.io;

import java.io.FileInputStream;
import java.io.IOException;

public class ReadFile {
    
    public static void main(String[] args) throws IOException {
        FileInputStream reader = new FileInputStream("nioexercise.txt");
        int data = reader.read();
        while (data != -1) {
            data = reader.read();
            System.out.print((char) data);
        }
        reader.close();
        System.out.println("The file was read successfully");
    }
}
