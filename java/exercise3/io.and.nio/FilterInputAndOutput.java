/*
 * Requirement:
 * 	   To write and read a file using filter and buffer input and output streams.
 * 
 * Entity:
 * 	   FilterinputAndOutput
 * 
 * Function Declaration:
 * 	   public static void main(String[] args);
 * 
 * Jobs to be Done:
 * 	   1)Create a file to write a file using fileOutputStream
 * 	   2)Write a message in that file.
 * 	   3)Access the file using FilterInputStream
 * 
 * Pseudo Code:
 * class FilterInputAndOutput {
 * 	   
 *     public static void main(String[] args) throws IOException {
		   File data = new File("WriteFile.txt");
		   FileOutputStream file = new FileOutputStream(data);
		   FilterOutputStream filter = new FilterOutputStream(file);

		   String string = "Welcome to java. Here are some example for the Input and Output stream for filter and buffer";

		   byte[] byteArray = string.getBytes();
		   filter.write(byteArray);
		   filter.flush();
		   filter.close();
		   file.close();

		   File data1 = new File("WriteFile.txt");
		   FileInputStream file1 = new FileInputStream(data1);
		   FilterInputStream filter1 = new BufferedInputStream(file1);

		   int data2 = 0;
		   while ((data2 = filter1.read()) != -1) {
	   		   System.out.print((char) data2);
		   }
		   file1.close();
		   filter1.close();
	   }
   }
 */

package com.kpriet.training.java.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

public class FilterInputAndOutput {

	public static void main(String[] args) throws IOException {
		File data = new File("WriteFile.txt");
		FileOutputStream file = new FileOutputStream(data);
		FilterOutputStream filter = new FilterOutputStream(file);

		String string = "Welcome to java. Here are some example for the Input and Output stream for filter and buffer";

		byte[] byteArray = string.getBytes();
		filter.write(byteArray);
		filter.flush();
		filter.close();
		file.close();

		File data1 = new File("WriteFile.txt");
		FileInputStream file1 = new FileInputStream(data1);
		FilterInputStream filter1 = new BufferedInputStream(file1);

		int data2 = 0;
		while ((data2 = filter1.read()) != -1) {
			System.out.print((char) data2);
		}
		file1.close();
		filter1.close();
	}
    }
