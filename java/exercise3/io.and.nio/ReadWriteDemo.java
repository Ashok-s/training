/*
 * Requirement :
 *  To write a Java program reads data from a particular file using FileReader and writes it to another, using FileWriter .
 *  
 *Entity:
 *  ReadWriteDemo 
 *  
 *Function Declaration:
 *  public static void main(String[] args);
 *  
 *Jobs to be Done:
 *  1)Create a reference for FileInputStream with file as argument .
 *  2)Create a reference for FileWriter.
 *  2)Read the file and store the value in a integer variable.
 *  3)Until the value comes -1 read the file.
 *      3.1)Write the file. 
 *  4)Print the statement .
 *  5)Close the file reader and writer .
 *  
 *Pseudo code:
 class ReadWriteDemo {
 
    public static void main(String[] args) throws IOException {
        //Declare the file path in FileInputStream 
        FileInputStream reader = new FileInputStream("Source.txt");
        FileWriter writer= new FileWriter("outputFile.txt");
        int data = reader.read();
        while (data != -1) {
            char dataChar = (char) data;
            writer.write(dataChar);
            data = reader.read();
        }
        System.out.println("Read and write operation was done successfully ");
        reader.close();
            
        writer.close();
    }
}
 */

package com.kpriet.training.java.io;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

public class ReadWriteDemo {
	
    public static void main(String[] args) throws IOException {
        FileInputStream reader = new FileInputStream("Source.txt");
        FileWriter writer= new FileWriter("outputFile.txt");
        int data = reader.read();
        while (data != -1) {
            writer.write(data);
            data = reader.read();
        }
        System.out.println("Read and write operation was done successfully ");
        reader.close();
            
        writer.close();
    }
}
