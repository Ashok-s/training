/*
 * Requirement:
 *     Delete a file using visitFile() method.
 *   
 * Entity:
 *     VisitFileDelete
 *   
 * Function Declaration:
 *     public static void main(String[] args);
 *     public FileVisitResult visitFile(Path file, BasicFileAttributes attribute);
 *   
 * Jobs to be done:
 *     1) Create a path instance for a path.
 *     2) By using walkfileTree method implement simpleFileVisitor.
 *     3) In a visitFile method traverse the path ,find and delete the files.
 *     4) Print the deleted file path.
 *     5) The operation continue until when the path dose not have any files.
 *   
 * Pseudo code:
 * class VisitFileDelete {
 *
 *     public static void main(String[] args) throws IOException {
 *         Path path = Paths.get(Path name);
 *         Files.walkFileTree(Path, new SimpleFileVisitor<Path>() {
 *
 *         public FileVisitResult visitFile(Path file, BasicFileAttributes attribute)
 *                  throws IOException {
 *              System.out.println("delete file: " + file.toString());
 *              //delete file
 *              return FileVisitResult.CONTINUE;
 *          }
 *     }
 */

package com.kpriet.training.java.io;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class VisitFileDelete {

    public static void main(String[] args) throws IOException {

        Path path = Paths.get("E:\\delete1.txt");

        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            public FileVisitResult visitFile(Path file, BasicFileAttributes attribute)
                   throws IOException {
                System.out.println("deleted file: " + file.toString());
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        });
    }
}
