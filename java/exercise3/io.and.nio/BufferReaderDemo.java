/*
 * Requirements : 
 * 		Read a any text file using BufferedReader and print the content of the file
 *
 * Entities :
 * 		BufferedReaderDemo.
 * 
 * Function Declaration:
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Create a reference for BufferedReader wrapped with FileReader having file as constructor argument.
 *     	2.Till the end of the file
 *          2.1)Read the content of the file.
 *          2.2)Print the content of the file.
 *     	3.Close the created BufferReader.
 *
 * PseudoCode:
 * 		class BufferedReaderDemo {
 * 
 *			public static void main(String args[]) throws Exception {
 *				BufferedReader reader = new BufferedReader(new FileReader("ReaderEx.txt"));
 *				//Read and Print the content of the file.
 *				reader.close();
 *			}
 *		}
 */

package com.kpriet.training.java.io;

import java.io.BufferedReader;
import java.io.FileReader;

public class BufferReaderDemo {
	
	public static void main(String args[]) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader("outputFile.txt"));
		int data;
		while ((data = reader.read()) != -1) {
			System.out.print((char) data);
		}
		reader.close();
	}
}