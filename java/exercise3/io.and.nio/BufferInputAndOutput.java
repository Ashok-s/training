/*
 * Requirement:
 * 		To write and read a file using filter and buffer input and output streams.
 * 
 * Entity:
 * 		BufferInputAndOutput
 * 
 * Function Declaration:
 * 		public static void main(String[] args);
 * 
 * Jobs to be Done:
 * 		1)Create a file to write a file using BufferOutputStream
 * 		2)Write a message in that file.
 * 		3)Access the file using BufferrInputStream
 * 
 * Pseudo Code:
 * 		class FilterInputAndOutput {
 * 
 * 		    public static void main(String[] args) {
 * 				File file = new File("FileOutput.txt");
 *		        FileOutputStream fileOutputStream = new FileOutputStream(file);
 *		
 *				BufferedOutputStream outputStream = new BufferedOutputStream(fileOutputStream);
 *				String string =
 *						"Welcome to java. Here are some example for the Input and Output stream for filter and buffer";
 *				byte[] byteArray = string.getBytes();
 *				outputStream.write(byteArray);
 *				outputStream.flush();
 *				outputStream.close();
 *		
 *				File newFile = new File("FileOutput.txt");
 *				FileInputStream data = new FileInputStream(newFile);
 *		
 *				//Read and print the content using bufferdInputStream
 * 			}
 * 		}
 */
package com.kpriet.training.java.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class BufferInputAndOutput {
	
	public static void main(String[] args) throws IOException {
		File file = new File("FileOutput.txt");
		FileOutputStream fileOutputStream = new FileOutputStream(file);

		BufferedOutputStream outputStream = new BufferedOutputStream(fileOutputStream);
		String string =
				"Welcome to java. Here are some example for the Input and Output stream for filter and buffer";
		byte[] byteArray = string.getBytes();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();

		File newFile = new File("FileOutput.txt");
		FileInputStream data = new FileInputStream(newFile);

		try {

			BufferedInputStream inputStream = new BufferedInputStream(data);
			int input;
			while ((input = inputStream.read()) != -1) {
				System.out.print((char) input);
			}
			inputStream.close();

		} catch (Exception e) {
			System.out.println(e);
		}

	}
}


