/*
 * Requirements : 
 *     To write some String content using Writer.
 *
 * Entities :
 * 	   StringWritter
 
 * Function Declaration:
 * 	   public static void main(String[] args);
 
 * Jobs To Be Done:
 * 		1.Create a reference for FileWriter with a file as constructor argument.
 *     	2.Write the content given in that file.        
 *     	3.Close the created FileWriter.
 *
 * PseudoCode:
 *     class StringWritter {
 *     
 *	       public static void main(String args[]) throws Exception {
 *			   Writer write = new FileWriter("StringWriter.txt");
 *			   String content = "The Java OutputStream class, java.io.OutputStream, is the " +
 *		                        "base class of all output streams in the Java IO API. Subclasses " + 
 *		                        "of OutputStream include the Java BufferedOutputStream and the " +
 *		                        "Java FileOutputStream among others. To see a full list of output " +
 *		                        "streams, go to the bottom table of the Java IO Overview page." ;
 *		       write.append(content);
 *			   write.close();
 *		   }
 *	   }
 */

package com.kpriet.training.java.io;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class StringWritter {

	public static void main(String[] args) throws IOException {
		Writer write = new FileWriter("StringWriter.txt");
		String content = "The Java OutputStream class, java.io.OutputStream, is the " +
		                 "base class of all output streams in the Java IO API. Subclasses " + 
		                 "of OutputStream include the Java BufferedOutputStream and the " +
		                 "Java FileOutputStream among others. To see a full list of output " +
		                 "streams, go to the bottom table of the Java IO Overview page." ;
		write.append(content);
		write.close();
		System.out.println("success");
		
	}

}
