/*
 * Requirements : 
 * 		Number of files in a directory and number of directories in a directory.
 *
 * Entities :
 * 		NoOfFilesAndDir
 * 
 * Function Declaration:
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1)Create a reference for File with file as constructor argument.
 * 		2)Print the number of files in the directory
 * 		3)Add all the files and directories to the file array.
 * 		4)initialize a integer variable with 0.
 * 		5)for each element in the array
 * 			5.1)check whether the element is directory
 * 				5.2)if yes, add 1 to the integer variable
 * 		6)Print the value of integer variable.
 *
 * Pseudo code:
 * 		class NoOfFilesAndDir {
 *
 *			public static void main(String[] args) {
 *				File file = new File("C:\\Users\\pabal.BALAJI\\eclipse-workspace\\javaee-demo");
 *				//Print the number of files
 *				
 *				File[] files = file.listFiles();
 *				int noOfDir = 0;
 *				for(File element : files) {
 *					noOfDir += element.isDirectory()? 1 : 0;
 *				}
 *				System.out.println("Number of directories in this directory : " + noOfDir);
 *			}
 *		
 *		}
 */

package com.kpriet.training.java.io;

import java.io.File;

public class NoOfFilesAndDir {

	public static void main(String[] args) {
		File file = new File("C:\\Users\\ashok\\eclipse-workspace\\javaee-demo\\lambda expression");
		System.out.println("Number of files in this directory : " + file.list().length);
		
		File[] files = file.listFiles();
		int noOfDir = 0;
		for(File element : files) {
			noOfDir += element.isDirectory()? 1 : 0;
		}
		
		System.out.println("Number of directories in this directory : " + noOfDir);
	}

}
