/*
 * Requirements:
 *     To get the file names of all file with specific extension in a directory.
 *    
 * Entities:
 *     FileName
 *     
 * Function Declaration:
 *     public static void main(String[] args);
 *     
 * Jobs To Done:
 *     1.Get the directory name.
 *     2.Store the files into an array.
 *     3.For each file in the array
 *     	   3.1)check whether the file is ends with particular file extension.
 *     	   3.2)if so print the file name.
 *
 * Pseudo code:
 *     class FileName {
 *
 *         public static void main(String[] args) {
 *             File file = new File("C:\Users\ashok\Downloads");
 *			   File[] fileArray = file.listFiles(); 
 *			   for (File files : fileArray ) {
 *                 if(files.getName().endsWith(".txt")) {
 *				       System.out.println(files.getName());
 *                 }
 *             }    
 *         }
 *     }   
 */
    	
package com.kpriet.training.java.io;

import java.io.File;

public class FileName {

	public static void main(String[] args) {
		File file = new File("C:\\Users\\ashok\\Downloads");
		File[] fileArray = file.listFiles();
		for (File files : fileArray ) {
			if(files.getName().endsWith(".txt")) {
			    System.out.println(files.getName());
		    }
		}	
	}
}
