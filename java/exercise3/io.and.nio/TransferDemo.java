/*
Requirement:
    To create two files named source and destination . Transfer the data from source file to destination file using NIO file channel.
    
Entity:
    TransferDemo
    
Function Declaration:
     public static void main(String[] args);
     
Jobs to be done:
    1. Create two files named source and destination.
    2. Get a channel for both the files.
    3. Transfer the data from source to destination using file channel.
    4. Check destination file for the result.
    
Pseudo code:
class TransferDemo {
    
    public static void main(String[] args) throws Exception {
        RandomAccessFile sourceFile = new RandomAccesssFile("file path","mode");
        FileChannel sourceChannel = sourceFile.getChannel();//create channel for source file
        
        RandomAccessFile destinationFile = new RandomAccesssFile("file path","mode");
        FileChannel destinationChannel = destinationFile.getChannel();//create channel for destination file
        
        detinationChannel.transferFrom(sourceChannel, position, count);
        //transfer data from source to destination
    }
}  

*/
package com.kpriet.training.java.io;

import java.nio.channels.FileChannel;
import java.io.RandomAccessFile;

public class TransferDemo {
    public static void main(String[] args) throws Exception {
        RandomAccessFile sourceFile = new RandomAccessFile(
                "C:\\Users\\ashok\\eclipse-workspace\\javaee-demo\\io and nio\\com\\kpriet\\training\\java\\io\\Source.txt",
                "rw");
        FileChannel sourceChannel = sourceFile.getChannel();

        RandomAccessFile destinationFile = new RandomAccessFile(
                "C:\\Users\\ashok\\eclipse-workspace\\javaee-demo\\io and nio\\com\\kpriet\\training\\java\\io\\destination.txt",
                "rw");
        FileChannel destinationChannel = destinationFile.getChannel();

        long position = 0;
        long count = sourceChannel.size();

        destinationChannel.transferFrom(sourceChannel, position, count);
        sourceFile.close();
        destinationFile.close();
        System.out.println("Transfered");
    }
}
