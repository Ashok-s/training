/*
 * Requirement:
 *     To write primitive datatypes to file using by dataoutputstream.
 * 
 * Entity:
 * 	   DataOuputStreamDemo
 * 
 * Function Declaration:
 * 	   public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 	   1) Open a file to write the primitive data type values in FileOutputStream.
 * 	   2) Declare that particular file as DataOutputStream type.
 * 	   3) Write some values which are primitive type.
 * 
 * Pseudo Code:
 * 	   class DataOutputStreamDemo {
 * 
 * 		   public static void main(String[] args) {
 * 		   	   FileOutputStream outputStream = new FileOutputStream("File.name");
 * 
 * 			   DataOutputStream dataOutputStream = new DataOutputStream(file);
 * 			   //Add some primitive data values to the file.
 * 
 * 			   //To print the file, Access the file again in datainputstream.
 * 		   }
 * 	   }
 */

package com.kpriet.training.java.io;

import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class DataOutputStreamDemo {
	
	public static void main(String[] args) throws IOException {

		FileOutputStream outputStream = new FileOutputStream("data.txt");
		DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
		dataOutputStream.writeInt(50000);
		dataOutputStream.writeShort(2500);
		dataOutputStream.writeByte(120);
		dataOutputStream.close();

		FileInputStream inputStream = new FileInputStream("data.txt");
		DataInputStream dataInputStream = new DataInputStream(inputStream);
		System.out.println("Int   : " + dataInputStream.readInt());
		System.out.println("Short : " + dataInputStream.readShort());
		System.out.println("Byte  : " + dataInputStream.readByte());
		dataInputStream.close();
	}

}
