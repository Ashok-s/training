/*
 * Requirements: 
 * 	   To Read a file using Reader.
 *
 * Entities :
 * 	   ReaderDemo.
 * 
 * Function Declaration:
 * 		public static void main(String[] args);
 * 
 * Jobs To Be Done:
 * 		1.Create a reference for FileReader with file as constructor argument.
 *     	2.Till the end of the file
 *          2.1)Read the content of the file.
 *          2.2)Print the content of the file.
 *     	3.Close the created reader.
 *
 * PseudoCode:
 * 		class ReaderDemo {
 * 
 *			public static void main(String args[]) throws Exception {
 *				Reader reader = new FileReader("ReaderEx.txt");
 *				//Read and Print the content of the file.
 *				reader.close();
 *			}
 *		}
 */

package com.kpriet.training.java.io;

import java.io.FileReader;
import java.io.Reader;

public class ReaderDemo {
	
	public static void main(String args[]) throws Exception {
		Reader reader = new FileReader("StringWriter.txt");
		int data;
		while ((data = reader.read()) != -1) {
			System.out.print((char) data);
		}
		reader.close();
	}
}
