/*
 * Requirement:
 * 		To write a program for ByteArrayInputStream class to read byte array as input stream.
 * 
 * Entity:
 * 		ByteArrayInput
 * 
 * Function Declaration:
 * 		public static void main(String[] args); 
 * 
 * Jobs To be Done:
 * 		1)Create a numbers to be added to the ByteArray system array.
 * 		2)Add those numbers to the byte array input stream system.
 * 		3)It access the array as byte read and process the required operation for it.
 * 
 * Pseudo Code:
 * 	   class ByteArrayInput {
 * 
 * 		   public static void main(String[] args) {
 * 			   byte[] buteArray = {"Some numbers"};
 * 
 * 			   //Assign the values to the byte input stream
 * 			   ByteArrayInputStream inputStream = new ByteArrayInputStream(byteArray);  
 *  
 *  		   //print the each value of input value into the special characters by reference of ascii code.
 * 		   }
 * 	   }
 */

package com.kpriet.training.java.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ByteArrayInput {
	
	 public static void main(String[] args) throws IOException {  
		    byte[] buteArray = { 35, 36, 37, 38 };  
		    ByteArrayInputStream inputStream = new ByteArrayInputStream(buteArray);  
		    int data = 0;  
		    while ((data = inputStream.read()) != -1) {   
		      char character = (char) data;  
		      System.out.println("ASCII value of Character is:" + data + "; Special character is: " + character);  
		    }  
		  }  
}
