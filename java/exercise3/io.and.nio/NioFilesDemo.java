/*
 * Requirements : 
 * 		Read a file using java.nio.Files using Paths.
 *
 * Entities :
 * 		NioFilesDemo
 
 * Function Declaration:
 * 		public static void main(String[] args);
 *
 * Jobs To Be Done:
 * 		1)Create a reference for Path.
 * 		2)Add all Lines in the file to the list.
 * 		3)for each line in the list.
 * 			3.1)Print all lines.
 *
 * PseudoCode:
 * 		class NioFilesDemo {
 * 
 *			public static void main(String[] args) throws IOException {
 *				Path path = Paths.get("outputFile.txt");
 *				List<String> lines = Files.readAllLines(path);
 *				for (String line : lines) {
 *					System.out.println(line);
 *				}
 *			}
 *		}
 */

package com.kpriet.training.java.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class NioFilesDemo {

	public static void main(String[] args) throws IOException {
		Path path = Paths.get("outputFile.txt");
		List<String> lines = Files.readAllLines(path);
		for (String line : lines) {
			System.out.println(line);
		}
	}
}
