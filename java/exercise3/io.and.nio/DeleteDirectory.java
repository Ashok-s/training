/*
 * Requirement:
 *     Delete the directory along with the files recursively.
 *    
 * Entity:
 *     DeleteDirectory
 *    
 * function declaration:
 *     public static void main(String[] args);
 *    
 * Jobs to be done:
 *    1) Create a path instance for a path.
 *    2) By using walkfileTree method implement simpleFileVisitor.
 *    3) In a visitFile method traverse the root  path ,find and delete the files.
 *    4) Print the deleted file path.
 *    5) The operation continue until when the path dose not have any files.
 *    6) In a postVisitDirectory method traverse the path ,find and delete the directory.
 *    7) Print the deleted directory path.
 *    
 * Pseudo code:
 * class DeleteDirectory {
 *
 *     public static void main(String[] args) throws IOException {
 *
 *         Path path = Paths.get("E:\\deletefolder");
 *
 *         Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
 *
 *         public FileVisitResult visitFile(Path file, BasicFileAttributes attribute)
 *                  throws IOException {
 *
 *             System.out.println("delete file: " + file.toString());
 *             //delete file
 *             return FileVisitResult.CONTINUE;
 *         }
 *
 *         public FileVisitResult postVisitDirectory(Path dir, IOException ex)
 *                   throws IOException {
 *             //delete directory
 *             System.out.println("delete directory: " + dir.toString());
 *             return FileVisitResult.CONTINUE;
 *         }
 *     });
 * }
 */

package com.kpriet.training.java.io;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class DeleteDirectory {

	public static void main(String[] args) throws IOException {

		Path path = Paths.get("E:\\deletefolder");

		Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

			public FileVisitResult visitFile(Path file, BasicFileAttributes attribute)
					throws IOException {

				System.out.println("deleted file: " + file.toString());
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			public FileVisitResult postVisitDirectory(Path dir, IOException ex) throws IOException {
				Files.delete(dir);
				System.out.println("deleted directory: " + dir.toString());
				return FileVisitResult.CONTINUE;
			}
		});
	}
}
