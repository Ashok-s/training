/*
 * What is Serialization?
 * 
 * 		Serialization is a mechanism of converting the state of an object into a byte stream.
 * Serialization in Java is the process of converting the Java code Object into a Byte Stream, to
 * transfer the Object Code from one Java Virtual machine to another and recreate it using the
 * process of Deserialization.
 * 
 */
