/*

 * Requirement:
 *      To write a program for Java String Regex Methods.
 *
 * Entity:
 *      StringRegexMethods
 *
 * Function declaration:
 *      public static void main(String[] args);
 *
 * Jobs to be done:
 *      1.Assign a string input for a variable.
 *      2.Find whether the regex pattern is present in the string.
 *      3.Spilt the string using regex pattern.
 *      4.Replace the first word by another word and replace all the same word by another word
 *      5.Print the results.
 *
 * Pseudo code:
 *      class StringRegexMethods {
 *
 *          public static void main(String[] args) {
 *              String text = "one two three two one";
 *              boolean matches = text.matches(".*two.*");
 *              System.out.println(matches);
 *              String[] regexArray = text.split("two");
 *              System.out.println(regexArray);
 *              String replaceFirst = text.replaceFirst("two", "five");
 *              System.out.println(replaceFirst);
 *              String replaceAll = text.replaceAll("two", "five");
 *              System.out.println(replaceAll);
 *          }
 *      }
 */
 
package com.kpriet.training.java.regex;

import java.util.Arrays;

public class StringRegexMethods {

    public static void main(String[] args) {
        String text = "one two three two one";
        boolean matches = text.matches(".*two.*");
        System.out.println(matches);
        String[] regexArray = text.split("two");
        System.out.println(Arrays.toString(regexArray));
        String replaceFirst = text.replaceFirst("two", "five");
        System.out.println(replaceFirst);
        String replaceAll = text.replaceAll("two", "five");
        System.out.println(replaceAll);
    }
}