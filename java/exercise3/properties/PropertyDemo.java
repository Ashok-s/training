/* Requirement:
 *   Write a program to perform the following operations in Properties.
 *       i) add some elements to the properties file.
 *      ii) print all the elements in the properties file using iterator.
 *     iii) print all the elements in the properties file using list method.
 *     
 * Entity:
 *     PropertyDemo
 *   
 * Function Declaration:
 *     public static void main(String[] args);
 *   
 * Jobs to be done:
 *   1) Create a object for properties.
 *   2) Set some property as (key - value) pair in properties.
 *   3) Create a object for FileOutputStream and assign a name for properties file.
 *   4) Store the properties in FileOutputStream as properties file.
 *   5) Print the properties stored successful.
 *   6) Invoke properties file and assign it to the iterator with type reference.
 *   7) For each element in the property file 
 *       7.1) get key and values and print the element.
 *   8) Print the element by using list method.
 *   
 * Pseudo code:
 * class PropertyDemo {
 *
 *     public static void main(String[] args) throws IOException{ 
 *         Properties properties = new Properties();
 *         //Add the element to the properties.
 *         //store the property element to the Outputstream.
 *         System.out.println("Properties stored successful");
 *
 *         System.out.println("Printing all the elements in the properties file using iterator");
 *         while (iterator.hasNext()) {
 *             String key = (String) iterator.next();
 *             String value = properties.getProperty(key);
 *             System.out.println(key + " = " + value);
 *         }
 *         properties.list(System.out);
 *      }
 * }
 */

package com.kpr.training.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Properties;

public class PropertyDemo {

	public static void main(String[] args) throws IOException {
		Properties properties = new Properties();
		properties.setProperty("India", "New Delhi");
		properties.setProperty("Pakistan", "Islamabad");
		properties.setProperty("Sri Lanka", "Colombo");
		properties.setProperty("Nepal", "Kathmandu");

		OutputStream output = new FileOutputStream("Capitals.properties");
		properties.store(output, "Countries and capitals.");
		System.out.println("Properties stored successful");

		System.out.println("Printing all the elements in the properties file using iterator");
		Iterator<Object> iterator = properties.keySet().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			String value = properties.getProperty(key);
			System.out.println(key + " = " + value);
		}
		properties.list(System.out);
	}
}
