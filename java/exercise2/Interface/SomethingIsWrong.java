package com.kpriet.training.java.core;

/*
Requirement:
    To identify what is wrong in the given interface program.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Entity:
    public interface SomethingIsWrong.

Function Declaration:
    public static void aMethod(int aValue).

Jobs to be done:
    1. Identifying the error in the given program and correcting in it.
*/

public interface SomethingIsWrong {

    public static void aMethod(int aValue) {     // Here default or static can be used.
        System.out.println("Hi Mom");
    }
}    