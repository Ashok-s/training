package com.kpriet.training.java.core;

/*
Requirement:
    To analyze the following interface valid or not:
    public interface Marker {}

Entity:
    public interface Marker.

Function Declaration:
    Here there is no function declaration.

Jobs To Be Done:
    1.Looking the given expression and answering the question.
*/

/*
Solution:
    Yes, This interface is valid.
 */