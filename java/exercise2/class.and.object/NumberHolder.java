package com.kpriet.training.java.core;

/*
Requirements:
    To write code that creates an instance of the class and initializes its two member variables 
    with provided values,and then displays the value of each member variable.
    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }


Entities:
    public class NumberHolder

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1. Create a instance for the class.
    2. Get the integer and float value.
    3. Print the integer and float value.
*/

public class NumberHolder {

	public int anInt;
	public float aFloat;

	public static void main(String[] args) {
		NumberHolder numberHolder = new NumberHolder();
		numberHolder.anInt = 100;
		numberHolder.aFloat = 22.67f;
		System.out.println(numberHolder.anInt);
		System.out.println(numberHolder.aFloat);
	}
}