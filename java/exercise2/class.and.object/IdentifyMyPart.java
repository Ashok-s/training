/*
Requirements:
To find the class and instance Variables in the given program.
public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }

Entities:
    public class IdentifyMyParts

Function Declaration:
    There is no function is declared.

Jobs To Be Done:
    1. Find the class variable and instance variable by static key word.
*/

/*
Class Variable : x     //because the variable is declared with static keyword.
Instance Variable : y
 */