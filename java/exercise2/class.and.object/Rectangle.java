package com.kpriet.training.java.core;

/*
Requirement:
To correct the error for the given program
public class SomethingIsWrong {
    public static void main(String[] args) {
        Rectangle myRect;
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
    }
}

Entities:
    public class SomethingIsWrong

Function Declaration:
    public int area();
    public static void main(String[] args);

Jobs To Be Done:
    1. Correct the given program.
*/

//THE CORRECTED PROGRAM:
public class Rectangle {

	public int width;        //Declare the variables
	public int height;

	public int area() {       //Define the method
		return (width * height);
	}

	public static void main(String[] args) {
		Rectangle myRect = new Rectangle();
		myRect.width = 40;
		myRect.height = 50;
		System.out.println("myRect's area is " + myRect.area());
	}
}