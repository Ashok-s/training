/*
Requirement:
    To demonstrate the abstract class using the class shape that has two methods to calculate the
    area and perimeter of two classes named the Square and the Circle extended from the class Shape.

Entity:
    Circle

Function Declaration:
    public void printArea();
    public void print Perimeter();

Jobs To Be Done:
    1)Create the class Circle.
    2)Declare the variable radius and initiate them.
    3)Declare and define the methods printArea() and printPerimeter().
*/

package com.kpriet.training.java.core;

public class Circle extends Shape {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public void printArea() {
        System.out.println(3.14 * radius * radius);
    }

    public void printPerimeter() {
        System.out.println(2 * 3.14 * radius);
    }
}
