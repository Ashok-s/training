/*
Requirement:
    To demonstrate the abstract class using the class shape that has two methods to calculate the
    area and perimeter of two classes named the Square and the Circle extended from the class Shape.

Entity:
    abstract class Shape.

Function Declaration:
    public void printArea();
    public void print Perimeter();

Jobs To Be Done:
    1)Create the abstract class Shape.
    2)Declare the abstract method printArea and printPerimeter.
*/

package com.kpriet.training.java.core;

public abstract class Shape {

    public abstract void printArea();
    public abstract void printPerimeter();
}
