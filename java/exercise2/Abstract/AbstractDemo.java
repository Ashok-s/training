/*
Requirement:
    To demonstrate the abstract class using the class shape that has two methods to calculate the
    area and perimeter of two classes named the Square and the Circle extended from the class Shape.

Entity:
    AbstractDemo

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1. Get the side length of the square and radius of the circle.
    2. Print the area and perimeter for both square and radius
    	2.1) By invoking printArea and printPerimeter method using square and radius references.
 */

package com.kpriet.training.java.core;

import java.util.Scanner;

public class AbstractDemo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double sideLength = scanner.nextDouble();
        double radius = scanner.nextDouble();
        scanner.close();
        Square square = new Square(sideLength);
        square.printArea();
        square.printPerimeter();
        Circle circle = new Circle(radius);
        circle.printArea();
        circle.printPerimeter();
    }
}
