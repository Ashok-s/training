/*
Requirement:
    To demonstrate the abstract class using the class shape that has two methods to calculate the
    area and perimeter of two classes named the Square and the Circle extended from the class Shape.

Entity:
    Square

Function Declaration:
    public void printArea();
    public void print Perimeter();

Jobs To Be Done:
    1)Create the class Square.
    2)Declare the variable sideLength and initiate them.
    3)Declare and define the methods printArea() and printPerimeter.
*/

package com.kpriet.training.java.core;

public class Square extends Shape {

    private double sideLength;

    public Square(double sideLength) {
        this.sideLength = sideLength;
    }

    public void printArea() {
        System.out.println(sideLength * sideLength);
    }

    public void printPerimeter() {
        System.out.println(4 * sideLength);
    }
}
