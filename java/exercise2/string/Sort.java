/*
Requirement:
    To sort and print following String[] alphabetically ignoring case. Also convert and print even indexed 
Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Entity:
    public class Sort.

Function declaration:
    public static void main(String[] args);
    
Jobs to be done:
    1)Sort the place in ascending order.
    2)Using loop change the even indexed places to uppercase.
 */

package com.kpriet.training.java.core;

import java.util.Arrays;

public class Sort {

	public static void main(String[] args) {
		String[] place = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem"};
		Arrays.sort(place);
		System.out.println(Arrays.toString(place));
		for (int i = 0; i < place.length; i++) {
			if (i % 2 == 0) {
				place[i] = place[i].toUpperCase();
			}
		}
		System.out.print(Arrays.toString(place));
	}
}
