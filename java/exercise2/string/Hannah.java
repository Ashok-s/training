/*
Requirement:
    Considering the following string:
String hannah = "Did Hannah see bees? Hannah did.";
1. What is the value displayed by the expression hannah.length()?.
2. What is the value returned by the method call hannah.charAt(12)?
3. Write an expression that refers to the letter b in the string referred to by hannah.

Entities:
	Hannah  

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1)Find the length of the given string.
    2)Find the charater value at the 12th positon.
    3)Find the index for the letter b. */

//Program:

package com.kpriet.training.java.core;

public class Hannah {

    public static void main(String[] args) {
        String hannah = "Did Hannah see bees? Hannah did.";
        System.out.println(hannah.length());
        System.out.println(hannah.charAt(12));
        System.out.println(hannah.charAt(15));
    }
}

/*Solution:
    1)32
    2)e
    3)b */