/*Requirement:
    To analyze the following code and to check the number of reference to those objects exist after
    the code executes.And also checking whether the object is eligible or garbage collection.
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;

Entity:
    ObjectEligible

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1. Create an students array of length 10.
    2. Assign the studentName as Peter Parker.
    3. Assign the 1st value of  students array as Peter Parker.
    4. Make the studentName as null
    5. print the students array
*/

package com.kpriet.training.java.core;

public class ObjectEligible {

    public static void main(String[] args) {
        String[] students = new String[10];
        String studentName = "Peter Parker";
        students[0] = studentName;
        System.out.println(students[0]);
        studentName = null;
        System.out.println(students.toString());
    }
}

/*ANSWER:
    1)Here the array has 1 reference and it is "Peter Parker".
    2)It is neither eligible nor garbage collection. Because the studentName is assigned as null, so
      there is no value is initialized after this.
    3)But the array size is 10 and its has only one reference value.*/