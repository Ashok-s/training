/*
Requirement:
    To write the program that computes the initial from the full name and displays the output.

Entity:
    Name
    
Function Declaration:
    public static void main(String[] args);
    
Jobs To Be Done:
    1. Get the name from the user.
    2. Print the initial of the name.
 */

//Program:

package com.kpriet.training.java.core;

public class Name {

	public static void main(String[] args) {
		String name = "Ashok S";
		System.out.print("initial = " + name.charAt(6));
	}
}
