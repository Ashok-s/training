/*
Requirement:
To find the output of the following program after each numbered line.
    public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

    /1/     result.setCharAt(0, original.charAt(0));
    /2/     result.setCharAt(1, original.charAt(original.length()-1));
    /3/     result.insert(1, original.charAt(4));
    /4/     result.append(original.substring(1,4));
    /5/     result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }

Entity:
    StringDemo

Function declaration:
    public static void main(String[] args),

Jobs To Be Done:

    1)Create the class StringDemo
    2)Declare the variable string of type String and assign the value as software.
    3)Create the object as newString and give the parameter as "hi".
    4)declare the variable index of type int and assign the index value of the character 'a' in the
      software using the method indexOf().
    5)DO set, insert ,append in the string "hi" and print the appropriate results.
 */

package com.kpriet.training.java.core;

public class StringDemo {

    public static void main(String[] args) {
        String string = "software";
        StringBuilder newString = new StringBuilder("hi");
        int index = string.indexOf('a');
/*1*/   newString.setCharAt(0, string.charAt(0));
        System.out.println(newString);
/*2*/   newString.setCharAt(1, string.charAt(string.length()-1));
        System.out.println(newString);
/*3*/   newString.insert(1, string.charAt(4));
        System.out.println(newString);
/*4*/   newString.append(string.substring(1,4));
        System.out.println(newString);
/*5*/   newString.insert(3, (string.substring(index, index+2) + " "));
        System.out.println(newString);
    }
}


/* OUTPUT
1)si
2)se
3)swe 
4)sweoft
5)swear oft
 */