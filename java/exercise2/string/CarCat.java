/*
Requirement:
    To find how long is the string returned by the following expression? What is the string?
    "Was it a car or a cat I saw?".substring(9, 12)

Entity:
    public class CarCat

Function declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1. Find the length of the string to be  returned.
    2. Find what the string is to be returned
*/

//Solution:

package com.kpriet.training.java.core;

public class CarCat {

    public static void main(String[] args) {
        System.out.println("Was it a car or a cat I saw?".substring(9, 12));
        System.out.println("Was it a car or a cat I saw?".substring(9, 12).length());
    }
}


/*Output:
    It returns the string of length 3 and it returns the string  car.*/