/*
Requirement:
   To show two ways to concatenate the following two strings together to get the string "Hi, mom.":
String hi = "Hi, ";
String mom = "mom.";

Entities:
    public class Concat

Function:
    public static void main(String[] args)

Jobs To Be Done:
    1. join two strings using plus operator and by using concate key word.
*/

//SOLUTION:

package com.kpriet.training.java.core;

public class Concat {

    public static void main(String[] args) {
        String hi = "Hi, ";
        String mom = "mom.";
        System.out.println(hi + mom);
        System.out.println(hi.concat(mom));
    }
}
