/*
Requirement:
    To demonstrate object equality using Object.equals() vs ==, using String objects.

Entity:
    ObjectEquality

Function Declaration:
    public static void main(String[] args);

Jobs To be Done:
    1.Create the variable for storing the same string value
    2.Use == and equals() method and print the result.
*/

package com.kpriet.training.java.core;

public class ObjectEquality { 

    public static void main(String[] args) {
    	
        String string1 = new String("HELLO");
        String string2 = new String("HELLO");
        String string3 = "HELLO";
        String string4 = "HELLO";
        
        System.out.println(string1 == string2);     //false    == operator checks whether the address is equal or not.
        System.out.println(string1 == string3);
        System.out.println(string3 == string4);       //true    in string pool constant the address is same for same string. 
        
        System.out.println(string1.equals(string2));   //true   equals() method checks for the value.
        System.out.println(string1.equals(string3));
    } 
}