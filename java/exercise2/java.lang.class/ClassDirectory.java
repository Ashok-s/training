/*
Requirement:
    To print the absolute path of the .class file of the current class.

Entity:
    CurrentDirectory.

Function Declaration:
    public static void main(String[] args);

Jobs to be Done:
    1.Get the directory where the .class file is loacated for current class
    2.Print the directory.
*/

package com.kpriet.training.java.core;

public class ClassDirectory {

    public static void main(String[] args) {
        String path = System.getProperty("user.dir");
        System.out.println("File Directory = " + path);
    }
}

