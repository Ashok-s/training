/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entities:
    Snake.

Function Declaration:
    public void move().

Jobs To Be Done:
    1)Declare the class Snake.
    2)Declare and define the method move().

*/
package com.kpriet.training.java.core;

public class Snake extends Animal {

    public Snake(String name, int legs) {
		super(name, legs);
	}

	public void move() {
        System.out.println("The snake moves");
    }
}
