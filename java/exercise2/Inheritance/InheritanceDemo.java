/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entity:
    InheritanceDemo

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)Create the class InheritanceDemo
    2)Create the objects for several classes Snake, Dog, Cat.
    3)Call the methods in that classes.
*/
package com.kpriet.training.java.core;

public class InheritanceDemo {

    public static void main(String[] args) {
        Animal snake = new Snake(null, 0);
        Animal dog = new Dog(null, 0);
        Animal cat = new Cat(null, 0);
        snake.move();           //overriding
        snake.move(5);          //overloading
        dog.sound();
        dog.move();             //overloading
        cat.sound();
    }
}
