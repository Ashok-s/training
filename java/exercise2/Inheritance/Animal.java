/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entity:
    Animal

Function Declaration:
    public void sound();
    public void move();
    public void move(int kilometer);

Jobs To Be Done:
    1. Get the name and number of legs for the animal from the user.
    2. Create the reference for Animal class.
    3. Invoke the methods move() and move(Kilometer) using the reference created.

*/

package com.kpriet.training.java.core;

import java.util.Scanner;

public class Animal {

    public String name;
    public int legs;

    public Animal(String name, int legs) {
        this.name = name;
        this.legs = legs;
    }

    public void sound() {
        System.out.println("Animal is giving sound");
    }

    public void move() {
        System.out.println("the animal is moving");
    }

    public void move(int kilometer) {
        System.out.println("the animal moves for " + kilometer + " kilometer");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int legs = scanner.nextInt();
        Animal animal = new Animal(name, legs);
        animal.move();
        animal.move(5);
        scanner.close();
        System.out.println(name);
    }
}
