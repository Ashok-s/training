/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entities:
    Dog.

Function Declaration:
    public void sound().

Jobs To Be Done:
    1)Declare the class Dog.
    2)Declare and define the method sound().

*/
package com.kpriet.training.java.core;

public class Dog extends Animal {
 
	public Dog(String name, int legs) {
		super(name, legs);
	}

	public void sound() {
        System.out.println("the dog barks");
    }
}