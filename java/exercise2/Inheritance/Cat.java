/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entities:
    Cat.

Function Declaration:
    public void sound().

Jobs To Be Done:
    1)Declare the class Cat and inherit the base class.
    2)Declare and define the method sound().
*/

package com.kpriet.training.java.core;

import com.kpriet.training.java.core.Animal;

public class Cat extends Animal {

    public Cat(String name, int legs) {
		super(name, legs);
	}

	public void sound() {
        System.out.println("The cat meows");
    }
}