/*
Requirement:
To find fibonacci series using while loop.

Entity:
    public class FibonacciWhile

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1.Get the input range for the series to be printed.
    2.Assign the values 0 and 1 for two different variables.
    3.For the end of the range starting from zero
    	3.1)Add the two numbers and store it in the variable sum.
    	3.2)Swap the number2 value to number1 and sum to number2.
    	3.3)Print the number1 value.
 */
package com.kpriet.training.java.core;

import java.util.Scanner;

public class FibonacciWhile {

    public static void main(String[] args) {
        int rangeValue; 
        int number1;
        int number2;
        int initialValue;
        int sum;
        number1 = 0;
        number2 = 1;
        initialValue = 1;
        Scanner scanner = new Scanner(System.in);
        rangeValue = scanner.nextInt();
        scanner.close();
        while(initialValue <= rangeValue) {
            sum = number1 + number2;
            number1 = number2;
            number2 = sum;
            initialValue++;
            System.out.print(number1 + " ");
        }
    }
}
