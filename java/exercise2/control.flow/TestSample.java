/*
Requirements:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
    It doesn't have any class name

Function Declaration:
    There is no function is declared in this program.

Jobs To Be Done:
    1)Considering the given program and determining the output
    2)Checking the aNumber value greater than or equal to zero.
    3)If it is true,it moves to the next statement.
    4)Checking the aNumber value equal to zero 
    5)If it is true,it prints the print statement if not it moves to the else part of this if clause
    6)There is no else part in this if clause. So it moves to the else of previous if clause.
    7)It prints the else print statement and the next print statement.
 */

/*
OUTPUT:
second string
third string
 */