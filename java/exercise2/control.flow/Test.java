/*
Requirements:
    To write the test program consisting the following codes snippet and make the value of aNumber
    to 3 and find the output of the code by using proper line space and breakers and also by using
    proper curly braces. The following snippet is given as
     if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
     else System.out.println("second string");
     System.out.println("third string");

Entities:
    public class Test

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1)Considering the given program
    2)creating the class Test.
    3)Creating the variable aNumber of type int.
    4)Assigning the value for aNumber as 3.
    5)Using proper line space and proper curly braces in the given snippet.
    6)Checking the output.
*/

package com.kpriet.training.java.core;

public class Test {

	public static void main(String[] args) {
		int aNumber = 3;
		if (aNumber >= 0) {

			if (aNumber == 0) {
				System.out.println("First String");
			}
		}

		else {
			System.out.println("Second String");
		}
		System.out.println("Third String");
	}
}