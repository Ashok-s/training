/*
Requirement:
To find fibonacci series using Recursion.

Entity:
    public class FibonacciRecursion

Function Declaration:
    public static void printSeries();
    public static void main(String[] args);
    
Jobs to be Done:
    1.Assign the values 0 and 1 for two different variables and also declare the variable sum.
    2.Get the range for fibonacci series to be printed.
    3.Invoke the method for printing the series.
    4.In method add the two numbers and store the value in the variable sum
        4.1)Swap the values of numbe2 to number1 and sum to number2.
        4.2)Print the value of number1.
        4.3)Invoke the same method.
 */

package com.kpriet.training.java.core;

import java.util.Scanner;

public class FibonacciRecursion {

        public static int number1 ;
        public static int number2 ;
        public static int sum ;
        public static void printSeries(int finalValue) {
            if(finalValue > 0) {
                sum = number1 + number2;
                number1 = number2;
                number2 = sum;
                System.out.print(number1 + " ");
                printSeries(finalValue - 1);
            }
        }
        
    public static void main(String[] args) {
        int rangeValue;
        number1 = 0;
        number2 = 1;
        Scanner scanner = new Scanner(System.in);
        rangeValue = scanner.nextInt();
        scanner.close();
        printSeries(rangeValue);
    }
}
