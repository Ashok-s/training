/*
Requirements:
    To find fibonacci series using for loop.

Entity:
    public class FibonacciFor

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1.Get the input range for the series to be printed.
    2.Assign the values 0 and 1 for two different variables.
    3.For the end of the range starting from zero
    	3.1)Add the two numbers and store it in the variable sum.
    	3.2)Swap the number2 value to number1 and sum to number2.
    	3.3)Print the number1 value.
 */

package com.kpriet.training.java.core;

import java.util.Scanner;

public class FibonacciFor {

    public static void main(String[] args) {
        int rangeValue ;
        int number1 = 0;
        int number2 = 1;
        int sum;
        Scanner scanner = new Scanner(System.in);
        rangeValue = scanner.nextInt();
        scanner.close();
        for(int initialValue = 1; initialValue <= rangeValue; initialValue++) {
            sum = number1 + number2;
            number1 = number2;
            number2 = sum;
            System.out.print(number1 + " ");
         }
    }
}
