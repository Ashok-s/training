/*
Requirement:
    To compare the enum values using equals method and == operator.

Entity:
    public class EnumDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:

    1. Create the enum classes.
    2. There are two types of comparision of enum values using equals method and == operator.
    3. Use the equals and = operator and print the results.
*/

//Answer:
package com.kpriet.training.java.core;

public class EnumDemo {

    public enum State {
        TamilNadu,
        Andrapradesh,
        Kerala,
        Karnataka,
        Telungana
    }

    public enum Capital {
        Chennai,
        Visakhapatnam,
        Thiruvanandapuram,
        Bengaluru,
        Hyderabad
    }

    public static void main(String[] args) {
        State state = null;
        System.out.println(state == State.Kerala);                // prints false
        System.out.println(Capital.Bengaluru.equals(State.Karnataka)); // prints false
        Capital capital = Capital.Chennai;
        System.out.println(capital == Capital.Chennai);             // prints true
        System.out.println(capital.equals(Capital.Chennai));        // prints true
    }
}
