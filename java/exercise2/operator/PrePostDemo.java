package com.kpriet.training.java.core;

/*
Requirement:
   To explain why the value "6" is printed twice in a row:
class PrePostDemo {

    public static void main(String[] args) {
        int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;
        System.out.println(i);    // "5"
        System.out.println(++i);  // "6"
        System.out.println(i++);  // "6"
        System.out.println(i);    // "7"
    }
} 

Entities:
    PrePostDemo

Function declaration:

Jobs To Be Done:
    1)understanding the concepts of pre and post increment
    2)Explaining  the result.


EXPLANATION:
    1) The class PrePostDemo is created.
    2) The variable i is declared and initialize the value as 3.
    3) The value of i is post incremented in the next line so that the value of i is printed as 4 in
       the next line.
    4) The value of i is pre incremented in the next line so that the value of i is printed as 5 in
       the next line.
    5) In the next line the value of i is preincremented and printed so it prints the value 6.
    6) In the next line the value of i is post incremented and printed in the same line so that
       first it prints the value of i and icreases the value of i so that it again prints the value
       of i as 6.

 */