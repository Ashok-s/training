/*
Requirements:
  To Change the following program to use compound assignments:
class ArithmeticDemo {

    public static void main (String[] args){

        int result = 1 + 2; // result is now 3
        System.out.println(result);

        result = result - 1; // result is now 2
        System.out.println(result);

        result = result * 2; // result is now 4
        System.out.println(result);

        result = result / 2; // result is now 2
        System.out.println(result);

        result = result + 8; // result is now 10
        result = result % 7; // result is now 3
        System.out.println(result);
    }
}

Entities:
    ArithmeticDemo

Function Declaration:
    public static void main (String[] args);

Jobs to be done:
    1)In the given program the operators are found and it is converted to 
      compound assignment.
*/

package com.kpriet.training.java.core;

class ArithmeticDemo {

    public static void main (String[] args){
        int result = 1+2; // result is now 3
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
        System.out.println(result);
        result %= 7; // result is now 3
        System.out.println(result);

    }
}