/*
Requirement:
    To find Integer method can you use to convert an int into a string that expresses the number in
    hexadecimal.

Entity:
    IntToString.

Function Declaration:
    public static void main(String[] args);

Jobs To Be Done:
    1.Get the number from the user.
    2.Convert the number to Hexa decimal. 
    3.Print the Hexa decimal number.
*/

package com.kpriet.training.java.core;

import java.util.Scanner;

public class IntToString {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        scanner.close();
        String string = Integer.toHexString(number).toUpperCase();
        System.out.println("HexaDecimal representation of the number " + number + " is = " + string);
    }
}
