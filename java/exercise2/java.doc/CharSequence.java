/*
Requirement:
    To find what methods would a class that implements the java.lang.CharSequence
    interface have to implement.
    
Entity:
    CharSequence

Function declaration:
    No function

Jobs to be done:
    1)Find the methods that a CharSequence interface have to implement.
*/

/*
Answer:
    The methods are:
        charAt(),
        length(),
        subSequence() and 
        toString()
 */