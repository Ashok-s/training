/*
Requirement:
    To find the Integer method would be used to convert a string expressed in base 5 into the
    equivalent int

Entity:
    StringToInt.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1.Get the string in base 5 from the user.
    2.Convert the it to integer value. 
    3.Print the value.
*/
package com.kpriet.training.java.core;

import java.util.Scanner;

public class StringToInt {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        scanner.close();
        int equivalentInteger = Integer.valueOf(string, 5);
        System.out.println("equivalent integer = " + equivalentInteger);
    }
}
