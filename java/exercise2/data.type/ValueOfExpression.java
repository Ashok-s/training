/*
Requirement:
    To find the value of the following expression, and to say why
     Integer.valueOf(1).equals(Long.valueOf(1)).

Entity:
    ValueOfExpression.

Function Declaration:
    public static void main (String[] args).

Jobs To Be Done:
    1)Create the class ValueOfExpression.
    2)Under the main statement print the value of the given expression.
*/

// Program

package com.kpriet.training.java.core;

public class ValueOfExpression {

    public static void main(String[] args) {
        System.out.println(Integer.valueOf(1).equals(Long.valueOf(1)));
    }
}

/*
The value of the given expression is false because the type of the method valueOf() is different.
*/