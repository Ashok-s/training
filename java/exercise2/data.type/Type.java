/*Requirement:
To print the type of the result value of following expressions
    100 / 24
    100.10 / 10
    'Z' / 2
    10.5 / 0.5
    12.4 % 5.5
    100 % 56

Entity:
    Type

Function declartion:
    public static void main(String[] args).

Jobs To Be Done:
    1)Create the class Type.
    2)Declare the variable expression1,expression2 and so on and assign the various expressions for 
      the variables.
    3)print the type of the expressions using the methods getClass() and getName() with the variable
      name.


*/
package com.kpriet.training.java.core;

public class Type {

    public static void main(String[] args) {
        Object expression1 = 100 / 24;
        System.out.println(expression1.getClass().getName());
        Object expression2 = 100.10 / 10;
        System.out.println(expression2.getClass().getName());
        Object expression3 = 'Z' /2;
        System.out.println(expression3.getClass().getName());
        Object expression4 =10.5 /0.5 ;
        System.out.println(expression4.getClass().getName());
        Object expression5 = 12.4 % 5.5;
        System.out.println(expression5.getClass().getName());
        Object expression6 = 100/56;
        System.out.println(expression6.getClass().getName());
    }
}
