/*
Requirement:
    To print the ClassNames of the Primitive Datatype.

Entity:
    1.PrimitiveClassName

Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1.Create the class literals for the primitive types and get the class name for the same.
*/
package com.kpriet.training.java.core;

public class PrimitiveClassName {

    public static void main(String[] args) {

        String intClassName = int.class.getName();
        System.out.println("ClassName of int : " + intClassName);
        String charClassName = char.class.getName();
        System.out.println("ClassName of Char : " + charClassName);
        String doubleClassName = double.class.getName();
        System.out.println("ClassName of double : " + doubleClassName);
        String floatClassName = float.class.getName();
        System.out.println("ClassName of float : " + floatClassName);
    }
}