/*
Requirement:
    To create a program that is similar to the previous one but instead of reading integer arguments
    it reads floating-point arguments.It displays the sum of the arguments, using exactly two digits
    to the right of the decimal point.For example, suppose that you enter the following:
    java FPAdder 1 1e2 3.0 4.754 The program would display 108.75. Depending on your locale, the 
    decimal point might be a comma (,) instead of a period (.).

Entity:
    Adder

Function declaration:
    public void add(Float... numbers);
    public static void main(String[] args);

Jobs to be done:
    1.Get the float values to be added
    2.Invoke the method add to add the given float values.
    3.If the array has length equal to  one
        3.1)print add more numbers
        3.2)otherwise add the float values and print it.
*/
package com.kpriet.training.java.core;

public class FloatAdder {

    public void add(float... numbers) {
        if(numbers.length == 1) {
            System.out.println("Add more numbers");
        } else {
            float sum = 0;
            for(float number : numbers) {
                sum += number;
            }
            System.out.format("%.2f", sum);
        }
    }
    public static void main(String[] args) {
        FloatAdder adder = new FloatAdder();
        adder.add(1, 1e2f, 3.0f, 4.754f);
        adder.add(4.78f);
    }
}
