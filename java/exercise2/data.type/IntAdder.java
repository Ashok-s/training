/*
Requirement:
    Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message if the user enters only one argument.

Entity:
    Adder

Function declaration:
    public void add(int... numbers);
    public static void main(String[] args);

Jobs to be done:
    1.1.Get the integer values to be added
    2.Invoke the method add to add the given integer values.
    3.If the array has length equal to  one
        3.1)print add more numbers
        3.2)otherwise add the integer values and print it.
*/
package com.kpriet.training.java.core;

public class IntAdder {

    public void add(int... numbers) {
        if(numbers.length == 1) {
            System.out.println("Add more numbers");
        } else {
            int sum = 0;
            for(int number : numbers) {
                sum += number;
            }
            System.out.println(sum);
        }
    }
    public static void main(String[] args) {
        IntAdder adder = new IntAdder();
        adder.add(2,3,4);
        adder.add(4);
    }
}


