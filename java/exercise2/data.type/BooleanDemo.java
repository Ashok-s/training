/*
Requirement:
    To invert the value of a boolean,using an operator.

Entity:
    BooleanDemo

Function declaration:
    public static void main(String[] args);

Jobs to be done:
    1.Create a class BooleanDemo.
    2.Declare the variable booleanValue as true .
    3.Print the inverted boolean value.
*/
package com.kpriet.training.java.core;

public class BooleanDemo {

    public static void main(String[] args) {
        boolean booleanValue = !true;
        System.out.println("Inverted boolean : " + booleanValue);
    }
}
