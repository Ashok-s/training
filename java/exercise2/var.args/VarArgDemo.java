/*
Requirement:
    To demonstrate the method overloading using varArgs.

Entity:
    VarArgDemo.

Function Declaration:
    public static void main(String[] args);
    public void test(int... numbers);
    public void test(String message, int... numbers);

Jobs to be done:
    1. Get the array of elements with some arguments.
    2. Invoke the methods printArray with different types of arguments.
    3. if the arguments has only numbers
    	3.1)print the length of the array and the numbers.
    	3.2)otherwise print the string and print numbers.

*/

package com.kpriet.training.java.core;

public class VarArgDemo {

    public void printArray(int... numbers) {
        System.out.println("Total Numbers = " + numbers.length);
        System.out.println("The numbers are:");
        for (int number : numbers) {
            System.out.println(number);
        }
    }

    public void printArray(String name, int ... marks) {
        System.out.println(" Student name = " + name);
        System.out.println("The marks are:");
        for (int mark : marks) {
            System.out.println(mark);
        }
    }

    public static void main(String[] args) {
        VarArgDemo array = new VarArgDemo();
        array.printArray(1,2,3);
        array.printArray("ASHOK", 99, 98, 96);
    }
}
