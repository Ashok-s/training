SELECT employee.first_name
      ,employee.sur_name
      ,department.department_name
  FROM employee
      ,department
 WHERE employee.department_id = department.department_id       