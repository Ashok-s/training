SELECT area
      ,department.department_id
      ,department.department_name
      ,employee.first_name
  FROM employee
  JOIN department ON (employee.department_id = department.department_id)
 ORDER BY department_name
         ,area