SELECT department.department_name
      ,MIN(employee.annual_salary) AS least_salary
      ,MAX(employee.annual_salary) AS highest_salary
  FROM exercise_2.department  
      ,exercise_2.employee
 WHERE department.department_id = employee.department_id
 GROUP BY department.department_id     