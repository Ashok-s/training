SELECT employee.employee_id
      ,employee.first_name
      ,employee.sur_name
      ,employee.department_id
  FROM employee 
 WHERE employee.department_id IS NULL ;