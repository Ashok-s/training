CREATE TABLE person (
    person_id      INT 
    ,first_name    VARCHAR(20)  
    ,sur_name      VARCHAR(20)
    ,date_of_birth DATE
    ,blood_group   VARCHAR(10)
    ,phone_number  INT
    ,email_id      VARCHAR(50)
    ,address       VARCHAR(255));

SHOW COLUMNS
FROM person