SELECT * FROM person 
 WHERE age = 15 AND blood_group = 'o+' ;
 
SELECT * FROM person
 WHERE age = 15 AND sur_name = 'kumar' ;
 
 
SELECT * FROM person
 WHERE age =  15 OR sur_name = 'kumar' ; 
 
 
SELECT * FROM person
 WHERE NOT blood_group = 'o+' ;
 
 
SELECT * FROM person 
 WHERE sur_name LIKE '%r' ;
 
 
SELECT * FROM person
 WHERE email_id LIKE 'a%' ;
 
 
SELECT * FROM person
 WHERE first_name NOT LIKE 'b%' ;   
 
 
SELECT first_name
  FROM Person
 WHERE detail_id = ANY (SELECT detail_id FROM detail WHERE school_name = 'Green park') ;
 
 
SELECT * FROM person
 WHERE blood_group IN ('o+') ;
 
 
SELECT * FROM person
 WHERE detail_id IN (1 ,2) ;
 
 
SELECT person.first_name, person.sur_name
  FROM person
 WHERE first_name LIKE 'al%' ;


SELECT person.first_name, person.sur_name
  FROM person
 WHERE first_name LIKE '%al%' ; 