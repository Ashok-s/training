SELECT person.person_id 
      ,person.first_name 
      ,person.sur_name 
      ,detail.school_name
  FROM person
 INNER JOIN detail ON person.detail_id = detail.detail_id ;
 
 
SELECT person.first_name 
      ,person.sur_name 
      ,detail.school_name
  FROM detail
  LEFT JOIN  person ON person.detail_id = detail.detail_id ;
  
  
SELECT person.first_name 
      ,person.sur_name 
      ,detail.school_name
  FROM person
 RIGHT JOIN  detail ON person.detail_id = detail.detail_id ;  
 
 
SELECT person.first_name 
      ,person.sur_name 
      ,detail.school_name
  FROM person
 CROSS JOIN  detail ;