CREATE TABLE person (
    person_id      INT(11)      NOT NULL PRIMARY KEY 
	,first_name    VARCHAR(20)  NOT NULL 
	,sur_name      VARCHAR(20)  NULL 
	,date_of_birth DATE         NOT NULL
    ,age           INT          CHECK (age>10) 
	,blood_group   VARCHAR(10)  NULL 
	,phone_number  INT(11)      NOT NULL 
    ,email_id      VARCHAR(50)  NOT NULL UNIQUE
    ,address       VARCHAR(255) NULL DEFAULT 'TamilNadu,India'
    ,detail_id     INT          NOT NULL FOREIGN KEY (detail_id) 
    REFERENCES detail (detail_id)
);