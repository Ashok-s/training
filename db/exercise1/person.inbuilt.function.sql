SELECT person.date_of_birth,NOW()
    AS today_date
  FROM person ;
  
  
SELECT MAX(person.age)
    AS maxium_age
  FROM person ;    
  
  
SELECT MIN(person.age)
    AS minimum_age
  FROM person ;
  

SELECT AVG(person.age)
    AS average_age
  FROM person ;
  

SELECT COUNT(person.person_id)
    AS total_person
  FROM person ;
  
  
SELECT person.first_name
    AS first_person
  FROM person  
 LIMIT 1 ;
  
  
SELECT person.first_name
    AS last_person
  FROM person   
 ORDER BY person.first_name DESC
 LIMIT 1 ;
    
    
SELECT SUM(person.age)
    AS total_age
  FROM person                       