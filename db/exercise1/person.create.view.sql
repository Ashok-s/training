CREATE VIEW full_detail AS
SELECT person.first_name
      ,detail.school_name
      ,customer.product
      ,customer.price
  FROM person 
 INNER JOIN detail ON person.detail_id = detail.detail_id
 INNER JOIN customer ON person.customer_id = customer.customer_id ; 
 
 
 SELECT * FROM full_detail ;