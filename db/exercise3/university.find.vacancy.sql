select designation.name AS designation_name 
      ,designation.rank  
      ,university.university_code
      ,university.university_name
      ,college.name AS college_name
      ,department.name AS dept_name
      ,college.city
      ,college.state
      ,college.year_opened
  FROM university
      ,college
      ,department
      ,designation
      ,college_department
      ,employee
 WHERE college.university_code = university.university_code
   AND university.university_code = department.university_code
   AND college_department.college_id = college.college_id 
   AND college_department.udept_code = department.code
   AND employee.college_id = college.college_id 
   AND employee.cdept_id = college_department.id 
   AND employee.desig_id = designation.id 
 ORDER BY designation.rank