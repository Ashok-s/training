SELECT student.id AS student_id
      ,student.roll_number AS student_roll_no
      ,student.name AS student_name
      ,student.gender
      ,student.phone
      ,college.name AS College_Name
      ,semester_result.semester
      ,semester_result.credits
      ,semester_result.grade
      ,semester_result.gpa
  FROM student
      ,semester_result
      ,college
      ,university
 WHERE university.university_code = college.university_code
   AND college.college_id = student.college_id
   AND student.id = semester_result.student_id
 ORDER BY college.name
         ,semester_result.semester ;
         
#using joins

SELECT student.id AS student_id
      ,student.roll_number AS student_roll_no
      ,student.name AS student_name
      ,student.gender
      ,student.phone
      ,college.name AS College_Name
      ,semester_result.semester
      ,semester_result.credits
      ,semester_result.grade
      ,semester_result.gpa
  FROM student
 INNER JOIN semester_result
    ON student.id = semester_result.student_id
 INNER JOIN college
    ON college.college_id = student.college_id
 INNER JOIN university
    ON university.university_code = college.university_code
 ORDER BY college.name
         ,semester_result.semester ;         