SELECT student.roll_number
      ,student.name AS student_name
      ,student.gender
      ,student.date_of_birth
      ,student.email
      ,student.phone
      ,student.address
      ,college.name AS college_name
      ,department.name AS dept_name
      ,employee.name AS HOD_name
  FROM department
      ,student
      ,employee
      ,college_department
      ,college
      ,university
 WHERE university.university_code = college.university_code
   AND student.college_id = college.college_id
   AND employee.college_id = college.college_id
   AND employee.desig_id = 3
   AND student.cdept_id = college_department.id
   AND college_department.udept_code = department.code
   AND university.university_name = 'Anna University'
   AND college.city = 'Coimbatore'
   LIMIT 0,20;
   
# using joins
  
SELECT student.roll_number
      ,student.name AS student_name
      ,student.gender
      ,student.date_of_birth
      ,student.email
      ,student.phone
      ,student.address
      ,college.name AS college_name
      ,department.name AS dept_name
      ,employee.name AS HOD_name
  FROM student 
 INNER JOIN college_department
    ON student.cdept_id = college_department.id
 INNER JOIN department
    ON department.code = college_department.udept_code
 INNER JOIN college
    ON college.college_id = student.college_id
 INNER JOIN university
    ON college.university_code = university.university_code
 INNER JOIN employee
    ON employee.college_id = college.college_id
   AND employee.desig_id = 3
 WHERE university.university_name IN ('Anna university')
   AND college.city = 'coimbatore'
 LIMIT 20;   