SELECT employee.id AS employee_id
      ,employee.name AS employee_name
      ,employee.date_of_birth
      ,employee.desig_id
      ,designation.name AS desig_name
      ,designation.rank
      ,college.name AS college_name
      ,department.name AS dept_name
      ,university.university_code
      ,university.university_name
  FROM employee
      ,college
      ,university
      ,designation
      ,department
      ,college_department
 WHERE college.college_id = employee.college_id
       AND college.university_code = university.university_code
       AND designation.id = employee.desig_id
       AND employee.cdept_id = college_department.id
       AND department.code = college_department.udept_code
       AND university.university_name = 'Anna University'
       ORDER BY designation.rank 
               ,college.name ;
               
#using joins

SELECT employee.id AS employee_id
      ,employee.name AS employee_name
      ,employee.date_of_birth
      ,employee.desig_id
      ,designation.name AS desig_name
      ,designation.rank
      ,college.name AS college_name
      ,department.name AS dept_name
      ,university.university_code
      ,university.university_name
  FROM employee  
 INNER JOIN designation
    ON employee.desig_id = designation.id
 INNER JOIN college_department
    ON employee.cdept_id = college_department.id
 INNER JOIN college
    ON college_department.college_id = college.college_id
 INNER JOIN university
    ON college.university_code = university.university_code
 INNER JOIN department
    ON college_department.udept_code = department.code
 WHERE college.university_code = ( SELECT university.university_code
                                     FROM university
                                    WHERE university.university_name = 'anna university')
 ORDER BY college.name
         ,designation.rank;               