#CREATE TABLE IF DOES NOT EXIST clt_amt 
SELECT university.university_name
      ,college.name AS college_name
      ,SUM(semester_fee.amount) AS collected_amount      
  FROM university
      ,college 
      ,semester_fee
      ,college_department
 WHERE university.university_code = college.university_code
   AND semester_fee.cdept_id = college_department.id
   AND college_department.college_id = college.college_id 
   AND semester_fee.paid_status = 'paid'
 GROUP BY university_name;


#CREATE TABLE unclt_amt
SELECT university.university_name
      ,college.name AS college_name
      ,SUM(semester_fee.amount) AS uncollected_amount      
  FROM university
      ,college 
      ,semester_fee
      ,college_department    
 WHERE university.university_code = college.university_code
   AND semester_fee.cdept_id = college_department.id
   AND college_department.college_id = college.college_id 
   AND semester_fee.paid_status = 'unpaid'
 GROUP BY university_name;
 
 
SELECT DISTINCT university.university_name
      ,college.name AS college_name
      ,clt_amt.collected_amount
      ,unclt_amt.uncollected_amount
  FROM university
      ,college 
      ,semester_fee
      ,college_department
      ,clt_amt
      ,unclt_amt
 WHERE university.university_code = college.university_code
   AND semester_fee.cdept_id = college_department.id
   AND college_department.college_id = college.college_id    
   AND clt_amt.college_name = unclt_amt.college_name 
 GROUP BY collected_amount;          
       