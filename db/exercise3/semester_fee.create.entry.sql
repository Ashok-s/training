SELECT university.university_name
      ,student.roll_number AS student_roll_number
      ,student.name AS student_name
      ,student.gender
      ,student.date_of_birth
      ,student.address
      ,college.name AS college_name
      ,department.name AS dept_name
      ,semester_fee.amount
      ,semester_fee.paid_year
      ,semester_fee.paid_status
  FROM university
      ,college
      ,department
      ,college_department
      ,student
      ,semester_fee
 WHERE university.university_code = college.university_code
   AND student.college_id = college.college_id
   AND department.code = college_department.udept_code
   AND college_department.college_id = student.college_id
   AND semester_fee.student_id = student.id
   AND semester_fee.cdept_id = college_department.id
 ORDER BY student.roll_number