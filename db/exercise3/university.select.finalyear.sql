SELECT student.roll_number
      ,student.name AS student_name
      ,student.gender
      ,student.date_of_birth
      ,student.email
      ,student.phone
      ,student.address
      ,college.name AS college_name
      ,department.name AS dept_name
  FROM department
      ,student
      ,college_department
      ,college
      ,university
WHERE university.university_code = college.university_code
  AND student.college_id = college.college_id
  AND student.cdept_id = college_department.id
  AND college_department.udept_code = department.code
  AND university.university_name = 'Anna university' 
  AND college.city = 'Coimbatore'
  AND student.academic_year = '2020';
  
/* Using INNER JOIN*/  

SELECT student.roll_number
      ,student.name AS student_name
      ,student.gender
      ,student.date_of_birth
      ,student.email
      ,student.phone
      ,student.address
      ,college.name AS college_name
      ,department.name AS dept_name
  FROM student
 INNER JOIN college_department 
    ON student.cdept_id = college_department.id
 INNER JOIN department
    ON college_department.udept_code = department.code
 INNER JOIN college
    ON student.college_id = college.college_id 
 INNER JOIN university
    ON university.university_code = college.university_code
 WHERE university.university_name = ('Anna university')
   AND college.city = 'Coimbatore'
   AND student.academic_year = '2020'
