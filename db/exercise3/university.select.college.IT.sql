SELECT DISTINCT college.code AS college_code
      ,college.name AS college_name
      ,university.university_name 
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.name AS dept_name
      ,employee.name AS hod_name
  FROM college
      ,university
      ,department
      ,college_department
      ,employee
 WHERE (university.university_code = '0001'
    OR university.university_code = '0004')
   AND university.university_code = college.university_code
   AND department.name = 'IT'
   AND college_department.college_id = college.college_id
   AND department.code = college_department.udept_code
   AND employee.desig_id = 3
   AND college.college_id = employee.college_id ;
   
#using joins

SELECT college.code AS college_code
      ,college.name AS college_name
      ,university.university_name 
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.name AS dept_name
      ,employee.name AS hod_name
  FROM employee 
 INNER JOIN college 
	ON employee.college_id = college.college_id
 INNER JOIN designation 
	ON employee.desig_id = designation.id
   AND designation.name = "head_of_department"
 INNER JOIN university 
	ON college.university_code = university.university_code
 INNER JOIN college_department 
	ON college_department.college_id = college.college_id
 INNER JOIN department
	ON college_department.udept_code = department.code
HAVING department.name IN ('cse','it');   