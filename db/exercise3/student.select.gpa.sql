SELECT student.roll_number
      ,student.name AS student_name
      ,student.gender
      ,student.date_of_birth
      ,student.email
      ,student.phone
      ,student.address
      ,college.name AS college_name
      ,department.name AS dept_name
      ,semester_result.gpa
  FROM department
      ,student
      ,college_department
      ,college
      ,university
      ,semester_result
 WHERE university.university_code = college.university_code
   AND student.college_id = college.college_id
   AND student.cdept_id = college_department.id
   AND college_department.udept_code = department.code
   AND semester_result.student_id = student.id
   AND semester_result.gpa >= 8;
  
  
SELECT student.roll_number
      ,student.name AS student_name
      ,student.gender
      ,student.date_of_birth
      ,student.email
      ,student.phone
      ,student.address
      ,college.name AS college_name
      ,department.name AS dept_name
      ,semester_result.gpa
  FROM department
      ,student
      ,college_department
      ,college
      ,university
      ,semester_result
 WHERE university.university_code = college.university_code
   AND student.college_id = college.college_id
   AND student.cdept_id = college_department.id
   AND college_department.udept_code = department.code
   AND semester_result.student_id = student.id
   AND semester_result.gpa >= 5;  
  
  
